# Notas sobre LaTeX

# Instalación
* Windows: MikTeX/Overleaf
* Linux: TexMaker
* MacOS: MacTeX

Los tres elementos necesarios para crear documentos PDF escritos en LaTeX son:
1. Editor de texto: Programa en el que escribiremos el documento en LaTeX.
2. Compilador: Traductor de las instrucciones escritas en LaTeX a documento PDF.
3. Visualizador PDF: Programa capaz de interpretar y mostrar un documento PDF.

La ventaja con Overleaf ante otros editores es que al ser un editor online 
podemos guardar nuestros documentos en la nube, en vez de tener que subir una
actualización del documento en nuestra nube personal o por medio de algún 
controlador de versiones como Github o GitLab.
Además de que podremos colaborar en un mismo documento de manera simultanea con 
otros integrantes.

# Crear un documento
Debemos indicar el tipo de documento que vamos a crear, lo cuál se puede indicar
con la siguiente línea de código:
    
    \documentclass{article}

En este caso creamos un documento corto con la clase _article_. Existen otras 
opciones como _report, book, etc_ para documentos más largos.

The more you know: 
<https://texblog.org/2007/07/09/documentclassbook-report-article-or-letter/>

Después de declarar el tipo de documento, debemos indicar el comienzo del 
contenido mediante las etiquetas:

    \begin{document}
        Mi primer documento en LaTeX!
    \end{document}

Estas dos etiquetas delimitan el contenido de nuestro documento. Es importante
escribir siempre todo el contenido entre estás dos etiquetas.

Pero pues queremos escribir ecuaciones, ¿no? Así que veamos cómo podemos hacerlo
en LaTeX.
Una opción es escribir las ecuaciones junto con el texto, sin cambiar 
necesariamente la línea. Esto se hace escribiendo las ecuaciones entre los 
símbolos: `$`.

    La expresión más importante de la teoría de la relatividad es: $E=mc^2$.

Pero si queremos que nuestra ecuación este centrada debemos hacerlo así:

    La expresión más importante de la teoría de la relatividad es: $$E=mc^2$$.
    
Esto lo veremos en la sección _Escribir ecuaciones en LaTeX._

## Declaración de entornos
Dentro de un documento es habitual definir distintos entornos para crear bloques
de contenido que LaTeX debe interpretar de forma distinta, la estructura de un
entorno es:

    \begin{Entorno}
    \end{Entorno}
    
Por ejemplo, el entorno principal de un documento pues es `document`. Dentro de
ese entorno es posible definir subentornos según las funciones deseadas. Podemos
abrir otro entorno dentro del entorno `document`.

    \begin{document}
        \begin{center}
            Este texto aparecerá centrado.
        \end{center}
    \end{document}

Otro ejemplo es con el entorno `itemize` para crear una lista:

    \begin{document}
        Aquí empieza una lista:
        \begin{itemize}
            \item Primer elemento.
            \item Segundo elemento.
        \end{itemize}
    \begin{document}

Existen grandes variadades de entornos que nos permitirán insertar todo tipo de
contenidos en el documento.

## Preámbulo de un documento
La sección entre la declaración del documento y el comienzo del contenido se 
conoce como preámbulo.
En esta sección podemos incluir instrucciones adicionales para trabajar con 
nuestro documento. 
Por ejemplo, el título, el autor y la fecha.

    \title{Mi primer documento}
    \date{27-04-2021}
    \author{El Juls}

La información que estamos agregando no aparecerá en el documento al compilarlo,
pero queda guardada y puede ser utilizada para crear una portada.
El preámbulo también es utilizado para cargar paquetes adicionales que añaden
funcionalidades a nuestro documento. La primera vez que utilicemos un paquete
nuevo el propio programa de LaTeX que utilicemos se encargará de instalarlo en
el ordenador para que esté disponible.
Uno de los paquetes de uso habitual en documentos con contenido matemático es
el `amsmath` creado por la _American Mathematical Society_. Podemos cargarlo de
la siguiente manera:

    \usepackage{amsmath}
    
Incluyendo esta línea en el preámbulo, LaTeX tendrá acceso a más comandos y 
entornos que nos permitirán escribir fácilmente ecuaciones de mayor complejidad.

También en el preámbulo es recomendable indicar el idioma de escritura del 
documento mediante el paquete `babel`, lo cual se indica así:

    \usepackage[spanish]{babel}

Aunque yo prefiero hacerlo con el comando:

    \usepackage[utf8]{inputenc}
    
De está forma podemos escribir las tildes y la ñ sin ningún problema.

De manera alterna podemos usar la siguiente codificación:

    \usepackage[latin1]{inputenc}

## Comentarios
Una de las posibilidades que nos ofrece LaTeX es la de añadir comentarios en 
medio del documento. Estos comentarios pueden servir para clarificar el uso de 
algunos comando y no aparecerán en el documento compilado.

La forma más rápida de añadir un comentario es escribir el símbolo `%` seguido
del comentario:

    Esta es una línea de texto % Esto es un comentario

En caso de querer escribir comentarios de varías líneas podemos utlizar el 
entorno `comment`, que está disponible mediante el paquete `comment`.

# Crear una portada
Para crear una portada en un documento podemos utilizar dos comandos distintos.

La opción más simple consiste en utilizar el comando `\maketitle.` Con esto lo
único que debemos de ahcer es escribir este comando justo después de empezar el 
entorno `document` para que LaTeX pueda generar automáticamente un título para 
el documento. La opción más avanzada y que nos da más libertad para personalizar
una portada es el entorno `titlepage`.

## Portada con el comando \maketitle
Hay que tener en cuenta que con este comando es necesario definir los parámetros
necesarios para crear el título en el preámbulo del documento. Esto incluye 
definir el título, el autor y la fecha del documento.

Con todo esto podemos crear una portada así:

    \documentlass{report}
    \title {La vuelta al mundo en 80 días}
    \date {Noviembre de 1872}
    \author {Jules Verne}
    \begin{document}
    \maketitle
    Contenido
    \end{document}
    
En caso de que no tengamos o queramos mostrar la fecha es necesario escribir el
comando `date` en blanco `\date{}`.

También hay que tener en cuenta que la clase de documento que seleccionemos 
define el formato de éste título. Utilizando la clase de documento `book` o 
`report`, se creará automáticamente una página para la portada al principio del 
documento. La clase `article` coloca el título, autores y fecha en el espacio
superior de la primer página.

Es posible forzar la creación de la portada en una página a parte indicándolo en
el comando que define la clase del documento. Podemos hacerlo con el parámetro 
`titlepage`:

    \documentclass[titlepage]{article}

## Portada con el entorno titlepage
Para crear una portada más personalizada es recomendable utilizar el entorno
`titlepage` dentro del documento.

Este entorno nos permite definir los elementos que queremos mostrar en la 
portada, incluyendo su posición, formato, espaciado, etc.

Un ejemplo de esto sería:

    \documentclass{report}
    \begin{document}
        \begin{titlepage}
            \centering
            {\bfseries\LARGE Universidad Nacional Autónoma de México \par}
            \vspace{1cm}
            {\scshape\Large Facultad de Ciencias \par}
            \vspace{3cm}
            {\scshape\Huge Título del proyecto \par}
            \vspace{3cm}
            {\itshape\Large Proyecto pa demostrar que no me ama \par}
            \vfill
            {\Large Autor: \par}
            {\Large Yo mero petatero \par}
            \vfill
            {\Large Abril 2021 \par}
        \end{titlepage}
    \end{document}

Aquí ponemos aún cositas que no he enseñado como `\vfill` o `\Large`.
Aún así, son comandos muy simples y fáciles de comprender.

Dentro del entorno `titlepage`, incluimos el comando `\centering` para centrar
el texto, podríamos alinearlo a la izquierda con `\raggedright` o a la derecha
con `\raggedleft`. 

También existen cosas entre llaves. Estás son las distintas unidades de texto
que aparecen en la portada. Se escriben entre llaves para indicar que deben
mostrarse con el mismo estilo. En este caso el estilo incluye definir el tipo
de letra y su tamaño.

Los tipos de letra utilizados en esta portada son mayúsculas pequeñas con 
`\scshape` (small caps), negritas (boldface) con `\bfseries` y cursiva (italic)
con `\itshape`. Aparte de estas tres posibilidades existen otros tipos de letra.
https://manualdelatex.com/tutoriales/tipo-de-letra

El tamaño de la letra se ha especificado con los comandos `\large, \Large, 
\LARGE y \Huge`. A parte de estás cuatro opciones existen también otros niveles.
https://manualdelatex.com/tutoriales/tamano-de-letra

Cada línea de texto termina con el comando `\par`. Esto indica que debe crearse
un nuevo párrafo. De lo contrario, LaTeX generaría todos los elementos de texto 
en una sola línea.

Por último, existen también una serie de comandos para indicar el espacio 
vertical entre los distintos elementos. Uno de ellos es `\vspace{longitud}` que
crea un espacio vertical con una longitud especificada. Y existe también la 
opción de usar el comando `\vfill`. Este comando simplemente rellana el espacio
para ocupar la página entera.

## Insertar una imagen o logo en la portada
Bueno, pero ¿qué pasa si queremos agregar ahora un logo, sea de nuestra empresa 
o de la Universidad a la que asistimos?
Para esto primero debemos usar el paquete `graphicx` que nos permite pegar 
imágenes en nuestro archivo. Este paquete otorga muchos beneficios que veremos 
después. 

Entonces podemos hacer lo siguiente:

    \documentclass{report}
    \usepackage{graphicx}
    \begin{document}
        \begin{titlepage}
            \centering
            {\includegraphics[width=0.2\textwidth]{logo}\par}
            \vspace{1cm}
            {\bfseries\LARGE Universidad Nacional Autónoma de México \par}
            \vspace{1cm}
            {\scshape\Large Facultad de Ciencias \par}
            \vspace{3cm}
            {\scshape\Huge Título del proyecto \par}
            \vspace{3cm}
            {\itshape\Large Proyecto pa demostrar que no me ama \par}
            \vfill
            {\Large Autor: \par}
            {\Large Yo mero petatero \par}
            \vfill
            {\Large Abril 2021 \par}
        \end{titlepage}
    \end{document}

El comando que inserta la imagen pues es:

    \includegraphics[width=0.2\textwidth]{logo}

Este comando inserta la imagen con el nombre logo. En este caso, es necesario 
que la imagen esté guardada en la misma carpeta que el archivo LaTeX. También 
es posible indicar la ruta hacia la carpeta donde hemos guardado la imagen.
Por ejemplos, si esta dentro de la carpeta `images` sería:

    \includegraphics[width=0.2\textwidth]{images/logo}
    
El comando anterior también incluye un parámetro para indicar la anchura de la
imagen. Existen distintas unidades para indicar la anchura de la imagen. En este
caso se ha indicado que sea un 20% de la anchura total del texto con 
`0.2\textwidth`.

# Estructura de un documento en LaTeX
La estructura de un documento es esencial para poder prsentar el contenido de 
forma ordenada. En LaTeX, diponemos de algunos comandos para dividir el 
contenido en distintos niveles de importancia.

En los documentos de clase `article`, una de las divisiones más importantes es
la división en secciones. Las secciones se definen mediante e comando `\section`

    \section{Título de la sección}

Para crear niveles inferiores disponemos de los comandos `\subsection{}` y
`\subsubsection{}`. Para crear una estructura con distintas secciones y 
subsecciones podemos utilizar:

    \documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
    \section {Animales}
        \subsection{Vertebrados}
            \subsubsection{Mamíferos}
            \subsubsection{Anfibios}
            \subsubsection{Reptiles}
            \subsubsection{...}
        \subsection{Invertebrados}
            \subsubsection{Gusanos}
            \subsubsection{Moluscos}
            \subsubsection{Medusas}
            \subsubsection{...}
    \end{document}

LaTeX asigna automáticamente a cada sección o subsección la numeración que 
corresponda.

En caso de querer crear una sección sin número, puede añadirse un asterico al 
declarar la sección. En este caso, la sección no aparecerá al crear un índice
de contenido.

    \section*{Sección sin número}

Crear un índice de contenidos: https://manualdelatex.com/tutoriales/indice-de-contenidos

Por debajo del nivel `\subsubsection` también es posible declarar párrafos y 
subparráfos. Estás unidades aparecen sin número y se declaran mediante los 
comandos `\paragraph` y `\subparagraph`. En estos dos casos, la jerarquía entre
estos niveles inferiores se indica mediante el sangrada o identación:

    \documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        \section {Animales}
            \subsection{Vertebrados}
                \subsubsection{Mamíferos}
                    \paragraph{Anatomía}
                        La anatomía de los mamíferos se caracteriza por...
                        \subparagraph{Aparato locomotor}
                        El aparato locomotor consiste en...
                        \subparagraph{Aparato digestivo}
                        El aparato digestivo es el conjunto...
                    \paragraph{Evolución}
                        La evolución de los mamíferos ...
                        \subparagraph{Orígenes}
                        Durante el período Jurásico ...
                        \subparagraph{Antecedentes}
                        Antes de la aparición de los primeros mamíferos ...
                    \subsubsection{Anfibios}
                    \subsubsection{Reptiles}
                    \subsubsection{...}
                    \subsection{Invertebrados}
                    \subsubsection{Gusanos}
                    \subsubsection{Moluscos}
                    \subsubsection{Medusas}
                    \subsubsection{...}
    \end{document}

## División en partes y capítulos
Uno de los comandos principales al crear un documento en LaTeX es la declaración
de la clase de documento `\documentclass{}`. En función de la clase de documento
que indiquemos tendremos acceso a distintos comandos para dividir el documento.

Por ejemplo, la clase `article` permite la división en secciones, subsecciones,
subsubsecciones, etc. Pero no permite la división a un nivel superior entre 
partes o capítulos.

En caso de querer dividir un documento en partes o capítulos es neesario 
utilizar las clases `book` o `report`. Estás divisiones pueden introducirse 
mediante los comandos `\part` y `\chapter`.

Por ejemplo podemos crear la siguiente división de un libro en dos partes, cada
una con distintos capítulos.

    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        \part {Curso de matemáticas}
            \chapter{Teoría}
                \section{Los números enteros}
                    \subsection{Definición}
                    \subsection{Ejemplos}
                \section{Fracciones}
                    \subsection{Definición}
                    \subsection{Ejemplos}
            \chapter{Ejercicios}
                \section{Números enteros}
                \section{Fracciones}
        \part{Curso de Geometría}
                \chapter{Geometría del plano}
                    \section{Definiciones}
                    \section{Ejemplod}
                \chapter{Polígonos}
                    \section{Definiciones}
                    \section{Ejemplos}
    \end{document}
    
Al declarar secciones entonces se asigna automáticamente la numeración que 
corresponde a cada parte o capítulo.

Para insertar una parte o capíutlo sin número simplemente debe incluirse un
asterisco junto con el comando correspondiente. Es importante recordar que en 
este caso, el capítulo o parte tampoco aparecerá en el índice de contenidos.

    \chapter*{Capítulo sin número}

## Mostrar la tabla de contenidos
En todos los casos mostrados anteriormente podemos crear un índice de contenidos
que muestre todas las divisiones con el comando `\tableofcontents`. En el 
ejemplo anterior, insertando este comando justo después del `\begin{document}` 
generá un bonito índice.

# Paquetes importantes en LaTeX
Un paquete de LaTeX es un conjunto de archivos que puede cargarse al principio 
de un documento para añadir funcionalidades a través de nuevos comandos.

La instlación de laTeX viene con una serie de paquetes preinstalados. Aún así, a
menudo es necesario cargar otros paquetes para aumentar el rango de 
posibilidades de lo que podemos hacer en nuestro documento.

Los paquetes pueden cargarse en el preámbulo del documento mediante el comando

    \usepackage[opciones]{nombre del paquete}
    
La primera vez que cargues un paquete que no has utilizado nunca es posible que
necesites descargar primero los archivos correspondientes al paquete.
A continuación te presentamos una lista de los paquetes más utilizados y que 
probablemente pueden serte de ayuda en alguna ocasión:

## amsmath y amssymb
Estos dos paquetes fueron creados por la _American Mathematical Society_ y 
amplían el catálogo de símbolos y entornos disponibles para escribir fórmulas
matemáticas. Probablemente los necesitarás para escribir fórmulas matemáticas
relativamente complejas.

## babel
El paquete _babel_ es útil para gestionar de forma óptima las particularidades 
del idioma en que se escribe el documento. Este paquete corrige pequeños 
detalles tipográficos asociados a un idioma en concreto. Por ejemplo, en el 
caso del español traduce el nombre de etiquetas como Capítulo, Bibliografía, 
etc. Que por defecto aparecen en inglés. Además se encarga de dividir 
correctamente las palabras a final de línea colocando el guión donde 
corresponda. Para utilizar el paquete _babel_ en un documento escrito en 
español puedes incluir en el preámbulo.

    \usepackage[spanish]{babel}
    
## biblatex
El paquete _biblatex_ permite gestionar la bibliografía de un documento. Este 
paquete permite crear automáticamente la bibliografía con el estilo deseado a
partir de las referencias guardadas en una base de datos tipo .bib.

Crear una bibliografía: https://manualdelatex.com/tutoriales/bibliografia

## blindtext
Este paquete permite insertar texto de relleno (lorem ipsum...) para maquetar
secciones de un documento. Con este paquete podemos simular distintas secciones
(párrafos, listas, enumeraciones, etc.) para probar distintos formatos. Puedes
cargar el paquete con el comando `\usepackage{blindtext}` y crear un texto
simulado con el comando `\blindtext`.

## graphicx
El paquete _graphicx_ permite insertar y manipular imágenes en un documento. Una
vez cargado el paquete podemos utilizar el comando

    \includegraphics[opciones]{imagen}
    
para insertar una imagen. Algunas de las opciones más utilizadas de este paquete
permiten cambiar el tamaño y orientación de la imagen.

## hyperref
El paquete _hyperref_ permite crear links entre distintas partes del documento o
incluso links a páginas web externas. Es muy útil para crear links entre 
elementos del texto y el punto del documento donde se encuentra el elemento en 
cuestión. Con este paquete puede crearse una tabla de contenidos de modo que 
cada título puede ser clicado para transladarse a la página correspondiente.

## inputenc
El paquete _inputenc_ facilita la escritura de un documento en un idioma que no
sea el inglés. Este paquete permite determinar el tipo de carácteres que deben 
ser considerados carácteres de entrada. Se especifica como codificación de 
entrada el formato utf8. Esto se indica mendiante el comando 
`\usepackage[utf8]{inputenc}`.
En el caso del espaól permite escribir directamente letras con tilde, diéresis o
la letra ñ sin tener que indicarlo con la barra invertida.

## fancyhdr
Cargar el paquete _fancyhdr_ introduce una serie de comandos para modificar el 
formato del encabezado y pie de página del documento. Esto permite escoger qué
elemntos mostrar en el encabezado o pie de página (nombre del capítulo, nombre 
de la sección, número de página, etc.) y su formato (izquierda, derecha, etc.)

## longtable
_Longtable_ es un paquete que puede ser de gran utilidad para crear tablas que
ocupan más de una página.

## multicol
La función principal del paquete _multicol_ es permitir la distribución de texto
en columnas. Por defecto LaTeX incorpora el comando `\twocolumn` para crear un
textoen dos columnas, la ventaja de este paquete es que permite dividir el texto
hasta en 9 columnas y asegura que la altura de cada columna sea aproximadamente
la misma.
Puede ser util si existen distintas secciones con distinto número de columnas.

## subcaption
Con este paquete podemos garantizar distintas leyendas o pies de foco dentro de 
otro elemento con su propia leyenda a un nivel superior. Un caso clásico es el
de una imaen que contiene a su vez otras imágenes. En este caso existen una 
leyenda para cada imagen y otra para englobar a todas las imagenes.

## siunitx
Puede ser de utilidad para gestionar y expresar correctamente cualquier medida 
utilizando el sistema internacional de unidades. Así nos aseguramos de que las
unidades se expresan según los estándares establecidos y evitamos errores de 
notación.

## verbatim
Este paquete introduce dos entornos de gran utilidad.

El primero es el entorno _verbatim_. Este entorno permite crear un espacio en el
que escribir de modo que su contenido aparezca de forma literal en el documento 
final. En otras palabras, dentro de este entorno los comandos no son ejecutados 
y aparecen en el mismo formato en el documento compilado.

El otro entorno introducido por este paquete es el entorno _comment_. Lo cual 
nos permite escribir comentarios multilínea sin necesidad de escribir el símbolo
`%` al principio de cada línea.

## xcolor
Permite cambiar el color de texto, resaltarlo de un color determinado o crear 
bordes a su alrededor.

# Escribir ecuaciones
Para escribir ecuaciones en LaTeX debemos crear un documento con el formato y 
clase que se requiera, cargar los paquetes necesarios para poder interpretar 
los comandos matemáticos y así llevarlo de una forma sencilla:

    \documentclass{article} %Definicióndel tipo de documento, artículo en este caso.
    \usepackage{amssymb, amsmath} %Paquetes matemáticos de la AMS.
    \begin{document}
        %Aquí escribiremos el contenido del documento. Ecuaciones en este caso.
    \end{document}

A continuación hay que definir el entorno dentro del cual se incluirá la 
ecuación. Si la ecuación debe aparecer de fomra continua en una línea de texto 
(modo texto) debe indicarse con los comando `$ ... $` o `\ (...) \`

Por ejemplo:

    Dada la función $f(x)=y$, el valor de la variable...

Si, en cambio, queremos que la ecuación aparezca centrada en una línea separada
(modo display) debemos utilizar `\begin{equation} ... \end{equation}` o de 
manera alterna `\[...]\`

En este caso:

    Dada la función
    \begin{equation}
        f(x)=y
    \end{equation}
    el valor de la variable...

En este caso la ecuación aparece automáticamente numerada. Si no queremos 
numerar las ecuaciones debemos indicarlo utilizando en su lugar los comandos
`\begin{equation*} ... \end{equation*}`

Una vez definidos los delimitadores del entorno `equation` sólo debemos escribir
en su interior los comando referentes a la ecuación en cuestión. Esta puede 
incluir números, letras, símbolos, operadores, fracciones, matrices, integrales,
etc.

## Símbolos matemáticos de utilidad
Los símbolos matemáticos más habituales (suma, resta, igualdad, paréntesis) 
pueden escribirse directamente en una ecuación sin necesidad de usar los 
comandos especiales.

    (2+3) - 6 = -1

Igual para las desigualdades

    5 < 7
    
Símbolos que podemos escribir directamente son: `! / () [] | : *`

Para las multiplicaciones podemos utilizar el comando `\cdot` o una cruz con el
comando `\times`:

    2 \cdot 3 = 2 \times 3 = 6

Otros símbolos utilizados habitualmente son los siguientes:

    \pm
    \approx
    \approxeq
    \geq
    \geqslant
    \leq
    \leqslant
    \neq
    \ll
    \gg
    \equiv

## Fracciones
Podemos introducir fracciones con el comando `\frac{numerados}{denominador}`.
Es necesario incluir entre llaves los dos argumentos que requiere este comando,
es decir, las expresiones para el númerador y el denominador.

    \frac{120}{527}

Además del comando `\frac` también podemos utilizar los comandos `\tfrac` y
`\dfrac`. `tfrac` es el comando de fracciones en estilo texto y `dfrac` en 
estilo display. Si sólo indicamos la fraccion con `\frac` LaTeX se encarga de 
escoger la versión más adecuada al contexto.

El estilo de texto, `\tfrac`, es de menor tamaño y encaja mejor en ecuaciones
escritas a lo largo del teto. Para ecuaciones escritas en una línea separada 
LaTeX escogerá automáticamente el formato `\dfrac`, pero sólo cuando tenga 
espacio suficiente para escribir la ecuación entera. En caso contrario, puede
escoger utilizar el modo texto. Para sobrescribir estás instrucciones es 
necesario utilizar `\dfrac` o `\tfrac`.

Si en modo texto no indicamos nada, LaTeX mostrará la ecuación en modo texto.

    Dada una fracción $\frac{79}{88}$ que indica...

Podemos reescribir esta instrucción indicando que queremos mostrar la ecuación
en modo display mediante el comando `\dfrac`

    Dada la fracción $\dfrac{79}{88}$ que indica...
    
## Raíces
La raíz cuadrada puede indicarse con el comando `sqrt`

    \sqrt{9} = 3

Para introducir otros exponentes simplemente tenemos que indicarlo entre 
corchetes:

    \sqrt[4]{16} = 2

De esta forma ya podemos combinar distintos comandos para crear expresiones
más complejas:

    \sqrt[3]{\frac{250}{2}} = \sqrt[3]{125} = 5

## Subíndices y superíndices
Para introducir subíndices sólo hay que indicarlo mediante el símbolo `_`

    K_n

Los superíndices o exponentes se introducen mediante el símbolo `^`. 

    2^3 = 8

También es posible introducir estos dos elementos simultáneamente

    K_n^2

LaTeX asume que el subíndice o superíndice es solamente el carácter situado
inmediatamente después del símbolo `_` o `^`. Así que hay que encerrar entre
corchetes si queremos tener superíndices o subíndices más largos.

    2^{2342}

## Paréntesis, llaves y corchetes
Los paréntesis y corchetes en LaTeX pueden escribirse diretamente, sin necesidad
de comandos especiales

    (2), [3]

Las llaves también se escriben directamente pero en este caso hay que escribir 
primero una barra invertida para que LaTeX interprete esta instrucción 
correctamente

    \{8\}

Otros tipos de delimitadores de uso habitual son los sigueintes:

    |3|, \[5\] y \langle 7 \rangle

Los paréntesis, llaves y corchetes escritos mediante estos comandos son siempre
de tamaño fijo. A veces es necesario insertar estos elementos con un tamaño 
variable que se adapte a la ecuación que contienen. 
Por ejemplo aquí deberíamos tener un paréntesis del tamaño de la fracción:

    (\frac{5}{15})

Para idnicar que los paréntesis deben tener una altura variable tenemos que 
insertar los comandos `\left` y `\right` antes del paréntesis de la izquierda y
derecha, respectivamente:

    \left(\frac{5}{15}\right)

Esto mismo se aplica con llaves, corchetes y el resto de delimitadores.

## Funciones trigonométricas
Aunque es posible escribir directamente funciones trigonométricas dentro de una
ecuación (sin, cos, etc) LaTeX incorpora una serie de comandos para definir 
estás funciones correctamente. Los comandos consisten en una barra invertida con
la abreviación de la función trigonométrica.

    \sin
    \cos
    \tan
    \arcsin
    \arccos
    \arctan
    \csc
    \sec
    \cot
    \sinh
    \cosh
    \tanh

## Logaritmos
Como el caso de las trigonométricas, los logaritmos se indicando con los 
siguientes comandos:
    
    \log
    \ln

## Suma y Producto
La suma puede insertarse mediante el comando `\sum`. Para introducir los 
argumentos de una suma utilizamos la misma notación que para los subíndices y 
superíndices. Hay que encerrar todos los carácteres del subíndice o superíndice
entre llaves

    \sum_{n=1}^{10}n

Es equivalente con el producto

    \prod_{n=1}^{10}n

## Integrales
Las integrales en LaTeX siguen la msima notación que en el caso de la suma o el
producto. Tenemos que agregar el siguiente comando:

    \int
    
A continuación podemos especificar los límites de la integral mediante `_` para 
el límite inferior y `^` para el límite superior.

    \int_0^5 x \mathrm{d}x

Englobamos la `d` en el comando `\mathrm` para que no aparezca en cursiva. Este
detalle es a gusto propio de cómo considerar la notación.

Coo en el caso de la suma, es necesario escribir los límites de la integral 
entre llaves si tienen más de un carácter:

    \int_0^{\infty} x \mathrm{d}x
    
Existen distintas variaciones del símbolo de la integral. Podemos escribir 
integrales dobles y triples, cuádruples o múltiples mediante los comandos

    \iint
    \iiint
    \iiiint
    \idotsint

También tenemos disponible el símbolo de una integral a lo largo de una línea
cerrada mediante

    \oint

## Límites

La abreviatura correspondiente a límite se inserta en LaTeX mediante el comando
`\lim`. Para especificar los elementos del límite (e.g. cuando x tiende a cero)
utilizamos la misma notación para subíndices.

En estos casos es habitual insertar una fleca para indicar la tendencia de la 
variable. Esta flecha puede insertarse con el comando `\rightarrow` o 
alternativamente con el comando `\to`.

También es habitual en estos casos insertar el símbolo de infinito, que puede 
escribirse mediante el comando `\infty`

    \lim_{x\rightarrow\infty}\frac{3+x}{x^2}
    
## Matrices
Una opción para escribir matrices en LaTeX es utilizar el entorno `matrix`. En
este caso las distintas celdas se separan mediante el símbolo _ampersand_ (&) y
cada  final de fila se indica mediante doble barra inversa `(\\)`. Con esto
podemos ya crear una matriz.

    \begin{matrix}
        5 & 4 & 8 \\
        4 & 0 & 7 \\
        3 & 5 & 6
    \end{matrix}

En el caso anterior la matriz aparece sin ningún tipo de delimitador. Si 
queremos introducir algun tipo de delimitador podemos utilizar variaciones del
entorno matrix. Los más habituales son `pmatrix, bmatrix, Bmatrix, vmatrix y
Vmatrix`.
    
    \begin{pmatrix}
        5 & 4 & 8 \\
        4 & 0 & 7 \\
        3 & 5 & 6
    \end{pmatrix}
    
Alternativamente podemos utilizar siempre el entorno matrix e incluir el 
delimitador que necesitamos inmediatamente antes y después de abrir y cerrar
este entorno

    \left(
    \begin{matrix}
        5 & 4 & 8 \\
        4 & 0 & 7 \\
        3 & 5 & 6
    \end{matrix}
    \right)

## Alineación de ecuaciones
Es habitual encontrarse con ecuaciones o grupos de ecuaciones que ocupan más de 
una línea. Esto ocurre si la ecuación es demasiado larga para ser escrita en una
sola línea o si escribimos distintos pasos a partir de una ecuación inicial. 
Existen diferentes opciones para dar formato a este tipo de ecuaciones.

Para ecuaciones largas una opción es utolizar el entorno `multiline` en lugar 
del entorno `equation`. Este entorno parte la ecuación en el punto donde 
indiquemos y mantiene un solo número para numerar la ecuación entera.

    \begin{multiline}
        (x+y+z)^3+k = x^3+y^3+z^3 \\
        + 3x^2y + 3x^2z + 3xy^2 + 3xz^2 + 3yz^2 + 6xyz + k
    \end{multiline}

Si queremos tener más control sobre la alineación de cada línea es mejor usar el
entorno `split` dentro de un entorno `equation`. Dentro de este entorno podemos 
indicar saltos de línea con `\\` y también el punto donde todas las líneas de la 
ecuación deben alinearse con `&`.

    \begin{equation}
    \begin{split}
        (a+b)^2&=(a+b)(a+b)\\
        & = a^2 + ab + ab + b^2\\
        & = a^2 + 2ab `b^2`
    \end{equation}
    \end{split}

La expresión anterior está compuesta por tres líneas y todas ellas están 
alineadas en el punto del símbolo `=`. En el caso anterior se asigna un único
número para todo el conjunto dentro del entorno `split`. Si queremos crear una
alineación similiar pero con un número distinto para la ecuación de cada línea 
es mejor utiliza el entorno `align`.

    \begin{align}
        (a+b)^2&=a^2+2ab+b^2\\
        (a-b)^2&=a^2-2ab+b^2\\
        (a+b)(a-b)=a^2 - b^2
    \end{align}

Este entorno funciona exactamente igual que `split` pero se asigna un número a 
cada línea.

Otra posibilidad es utilizaar el entorno `gather`. Dentro de este entorno se 
asigna un número a cada línea y cada ecuación aparece centrada en la página. 
En este caso es necesario indicar con el ampersand el punto de alineación.

    \begin{gather}
        (a+b)^2=a^2+2ab+b^2\\
        (a-b)^2=a^2-2ab+b^2\\
        (a+b)(a-b)=a^2-b^2
    \end{gather}

## Vectores
Existen una serie de comandos que son indispensables cuando se trabaja con 
vectores. El primero es sin duda la representación de una pequeña flecha sobre 
una letra para indicar que se trata de un vector. Esto puede generarse con el
comando `\vec`

    \vec{v}

En algunos casos, cuando se trata de vectores unitarios de una base, se utiliza 
el acento circunflejo para indicar que se trata de un vector. Este puede 
insertarse con el comando `\hat{}`

    \hat{k}

En caso de escribir tres vectores, _i,j,k_ combijados con el acento es mejor 
utilizar los comandos `\imath` y `\jmath` para la i y la j, para sustituir el 
punto por el acento.

    \hat{\imath}
    \hat{\jmath}
    \hat{k}

Entre las operaciones más habituales entre los vectores tenemos el producto
escalar que podemos representar con `\cdot`, el producto vectorial, que podemos
representar con `\times`

    \vec{u} \cdot \vec{v}
    \vec{u} \times \vec{v}

## Acentos
A parte del acento mostrado anterioremnte para indicar un vector, existen otros
acentos que pueden ser útiles de vez en cuando.

    \acute{a}
    \grave{a}
    \tilde{a}
    \bar{a}
    \hat{a}
    \vec{a}
    \dot{a}
    \ddot{a}
    \dddot{a}

## Texto dentro de una ecuación
Existen varias alternativas para insertar texto dentro de una ecuación. Escribir
directamente un texto dentro del entorno ecuación no funciona correctamente 
porque LaTeX interpreta cada carácter como si fuera una variable.

Una primera opción es usar simplemente el comando `\text` dentro del entorno
ecuación para indicar el texto que queremos esscribir.

    \begin{equation}
        F = ma \quad\text{Segunda ley de Newton}
    \end{equation}

En el ejemplo anterior se ha incluido también el comando `\quad` para creaar un 
espacio entre la fórmula y el texto. Las variaciones de este comando incluyen 
`\textit` para texto en cursiva, `\textbf` para texto en negrita y `\textsf` 
para tipo de letra _sans serif_.

Si hay que escribir simplemente una letra o palabra puede utilizarse también el
comando `\mathrm`. De forma simular, existen comandos `\mathir` para letra 
cursiva, `\mathbf` para negrita, `\mathsf` para letra _sans serif_ y `\mathtt`
para letra de maquina de escribir. Estos comandos no están pensados para 
escribir texto sino solamente algunas letras en algún tipo de fuente específico.
En consecuencia, los espacios son ignorados. 

    f(x) = 2x \quad\mathrm{si}\quad x < 2
    f(x) = x^2 \quad\mathit{si}\quad 2 \leq x < 4
    f(x) = x + 12 \quad\mathsf{si}\quad x \geq 4
    
## Tipos de letra en el modo matemático
Dentro del modo matemático en LaTeX (por ejemplo, dentro de un entorno 
`equation`), a veces es necesario escribir letras con algún tipo de letra 
distinta. Tres posibilidades muy útiles son las letras caligráficas, las letras
negritas de pizarra y las letras Fraktur.

Las letras caligráficas pueden escribirse con el comando `\mathcal`. Estas son
normalmente utilizadas para escribir la transformada de Fourier, `\mathcal{F}`,
o la transformada de Laplace, `\mathcal{L}`.

Las letras negritas de pizzara (Blackboard Bold) son muy utilizadas para 
representar conjuntos de números. Por ejemplo, los números naturales, 
`\mathbb{N}`, o los números enteros, `\mathbb{Z}`.

Para otras aplicaciones tenemos también disponible el estilo de letra gótica 
Fraktur con `\mathfrak`.

## Referenciar una ecuación
Si queremos hacer referencia a una ecuación dentro del texto en primer lugar 
tenemos que incluir el comando `\label` dentro de la ecuación correspondiente. 
El texto dentro del comando label es la etiqueta de la ecuación.

    \begin{equation}
        \label{eq:esfera}
        V=\frac{4}{3}\pi r^2
    \end{equation}

A continuación podemos hacer referencia a esta ecuación en algún punto del texto
con el comando `\ref`. 

    El volumen de una esfera se calcula mediante la ecuación~\ref{eq:esfera}.

Hay dos detalles importantes. Uno es que hay que tener en cuenta que se pueden
crear etiquetas para otro tipo de elementos (figuras, tablas, etc). Para 
mantener un cierto orden dentro del documento es recomendable incluir en el 
nombre de la etiqueta una referencia al tipo de elemento (`fig` para figuras,
`eq` para ecuaciones, etc). En el caso presentado la etiqueta de la ecuación 
es: `eq:esfera`.

Otro detalle es que al hacer referencia a la ecuación se ha escrito el símbolo
`~` entre la palabra eciación y el comando `\ref`. Esto indica a LaTeX que debe 
haber un espacio entre los dos elementos pero también que los dos elementos 
deben estar en la misma línea. Con esto evitamos que haya un salto de línea 
entre la palabra ecuación y el número de la ecuación.

Referencias cruzadas en LaTeX: https://manualdelatex.com/tutoriales/referencias-cruzadas

# Crear tablas en LaTeX
Existen distintas alternativas para crear tablas en LaTeX. En este artículo 
presentamos uno de los métodos más utilizados basado en el entorno `tabular`.

Los comandos mínimos para crear una tabla con el entorno `tabular` incluyen:
crear el entorno, definir el número de columnas necesarias y añadir el contenido
en tantas filas como sea necesario.

    \begin{tabular}{r l}
        Fruta & Cantidad \\
        Manzana & 4 \\
        Naranja & 10 \\
        Plátano & 3
    \end{tabular}

Los comandos `\begin{tabular}` y `\end{tabular}` delimitan el entorno. Junto con
el comienzo del entorno `tabular` deben definirse el número de columnas. Esto se
lleva a cabo escribiendo entre llaves una letra para cada columna. Esta letra 
indica si el contenido debe estar alineado a la izquierda (l), a la derecha (r)
o centrado (c).

En este caso hemos definido dos columnasm la primera alineada a la derechay la 
segunda a la izquierda, (e.g. {r l}).

También es posible definir la anchura de las columnas en lugar de su alineación.
en este caso indicamos cada columna con la letra p seguida de la anchura entre 
llaves. Por ejemplo,, para crear dos columnas de 4 y 3cm, respectivamente sería:

    \begin{tabular{ p {4cm} p{3cm}} }

Para añadir el contenido de cada fila debemos separar mediante `&` el contenido
correspondiente a cada columna. Para indicar el final de una fila escribimos
`\\`.

    Manzana & 4 \\

Escribirá Manzana en la rpimera columna, 4 en la segunda columna y a 
continuación cerrará la fila. Siguiendo este esquema pueden añadirse tantas 
flas como sea necesario.

Si queremos mostrar una línea horizontal entre dos filas podemos escribir el 
comando `\hline` después de la doble barra invertida `\\`.

    \begin{tabular}{ p {4cm} p{3cm}}
        Manzana & 4 \\
        Naranja & 10 \\
        Plátano & 3 \\
    \end{tabular}

También es posible incluir líneas verticales entre las distintas columnas. Estas
deben crearse al abrir el entorno `tabular` y definir las columnas. Si queremos
crear tres columnas centradas con líneas verticales entre ellas escribimos:

    \begin{tabular}{ c | c | c }
    
Aplicando este sistema a la tabla anterior obtenemos:

    \begin{tabular}{| c | c |}
        \hline
        Fruta & Cantidad \\ \hline
        Manzana & 4 \\
        Naranja & 10 \\
        Plátano & 3 \\ \hlinet
    \end{tabular}

Con el mismo sistema también es posible crear líneas dobles:

    \begin{tabular}{ c || c || c }

Hay ocasiones en las que es necesario definir exactamente la posición en la que
aparece la tabla dentro de documento. Además, a veces será necesario añadir una
descripción o poder hacer referencia a ella en algún punto del texto. Para ello
es necesario incluir la tabla, declarada mediante el entorno `tabular`, dentro 
del entorno `table`.

    \begin{table}[t]
        \begin{center}
            \begin{tabular}{| r | l | c |}
                Fruta & Cantidad & Origen \\ \hline
                Manzana & 4 & Estados Unidos \\
                Naranja & 10 & España \\
                Plátano & 3 & Colombia \\ \hline
            \end{tabular}
            \caption{Fruta disponible}
            \label{tab:fruta}
        \end{center}
    \end{table}
    
El primer comando e este ejemplo abre un entorno `table`. La indicación entre 
llaves, en este caso `[t]`, indica que la tabal debe posicionarse en la parte 
superior de la página. Las cuatro posibilidades principales son: `t` (top: 
parte superior), `b` (bottom: parte inferior), `h`(here: aproximadamente en el 
punto donde se inserta la tabal) o `p` (page: mostrar la tabla en una página a 
parte).

A continuación se abre un entorno `center` para centrar la tabla horizontalmente
en la página.

Una vez cerrado el entorno `tabular` y antes de cerrar el entorno `table` puede
indicarse la descrpción y etiqueta de la tabla.

La descripción se incluye mediante el comando `\caption{Texto descriptivo}` y
aparecerá debajo de la tabla. Por defecto, el paquete babel en español llama 
cuadros a las tablas. Si no especificamos nada, la descripción de la tabla 
aparecerá con está etiqueta de `Cuadro 1`.

Si preferimos que se muestren con la palabra Tabla, debemos indicarlo al carga 
el paquete babel mediante lo siguiente:

    \usepackage[spanish, es-tabla]{babel}

La etiqueta se define mediante el comando `\label{etiqueta}`. Al tratarse de una
tabla es recomendable pero no obligatorio definir la etiqueta empezando con el 
texto `tab: _____` Esto permite evitar confusiones con otros elementos en el 
documento.

Una vez definida la etiqueta puede crearse una referencia en el texto mediante 
el comando `\ref{tab:etiqueta}`

    Como podemos ver en la Tabla \ref{tab:fruta}, hay tres tipos de fruta disponible.
    
## Combinar celdas en LaTeX
Existen ocasiones en las que es necesario combinar celdas de un tabla. El 
procedimiento a seguir es distinto dependiendo de si las celdas se combinan 
horizontalmente o verticalmente.

Para combinar celdas horizontalmente debemos indicar que la nueva celda abarca
distintas columnas. Esto se consigue mediante el comando `\multicolumn`. Al 
utilizar el comando hay que indicar tres parámetros:

    \multicolumn{columnas}{alineación}{contenido}
    
En primer lugar escribimos entre llaves el número de columnas que deben 
fusionarse. En segundo lugar indicamos la alineación de la nueva celda: 
`l, r, c`. Finalmente, incluimos el contenido en la nueva celda.

Con este comando podemos crear una fila inicial que abarque las cuatro columnas:

    \begin{table}[t]
        \begin{center}
            \begin{tabular}{| c | c | c | c | }
                \hline
                \multicolumn{4}{ |c| }{Música} \\ \hline
                Artista & Género & Álbum & Canción \\ \hline
                Twenty One Pilots & Alternativo & Trench & Bandito \\
                Twenty One Pilots & Alternativo & Trench & My Blood \\
                Dua Lipa & Pop & Future Nostalgia & Levitating \\
                Bach & Clásica/Orquesta & BWV 1041 & Concierto para violín en la menor \\
                Alt-J & Alternativo & An Awesome Wave & Taro \\ \hline
            \end{tabular}
            \caption{Mi música favorita}
            \label{tab:musica}
        \end{center}
    \end{table}

También podemos combinar solo algunas de las celdas de una fila concreta:

    \begin{table}[t]
        \begin{center}
            \begin{tabular}{| c | c | c | c | }
                \hline
                \multicolumn{4}{ |c| }{Música} \\ \hline
                Artista & Género & Álbum & Canción \\ \hline
                \multicolumn{2}{ |c|}{Twenty One Pilots Alternativa} & Trench & Bandito \\
                Twenty One Pilots & Alternativo & Trench & My Blood \\
                Dua Lipa & Pop & Future Nostalgia & Levitating \\
                Bach & Clásica/Orquesta & BWV 1041 & Concierto para violín en la menor \\
                Alt-J & Alternativo & An Awesome Wave & Taro \\ \hline
            \end{tabular}
            \caption{Mi música favorita}
            \label{tab:musica}
        \end{center}
    \end{table}

En este caso el contenido de "Twenty One Pilots Alternativa" se muestra en una 
sola celda, mientras que el resto de celdas en la fila siguen la división de la
tabla.

También puede ser necesario combinar celdas en dirección vertical. Para estos 
casos es necesario cargar el paquete `multirow`, que da acceso al comando 
`\multirow` con un funcionamiento muy similar a `\multicolumn`. En este caso 
debemos definir el número de filas a combinar, la anchura de la columna y el 
contenido de la nueva celda. 

    \multirow{2}{4cm}{Contenido}

Este código generá la combinación de dos celdas, dándoles una anchura de 4 cm 
con la palabra "Contenido". También es posible escribir un asterisco como 
parámetro de anchura para que la celda se adapte automáticamente a la anchura
necesaria. Aplicando este comando a la tabla anterior podemos obtener lo 
siguiente:
    
    \usepackage{multirow}
    \begin{table}[t]
        \begin{center}
            \begin{tabular}{| c | c | c | c | }
                \hline
                \multicolumn{4}{ |c| }{Música} \\ \hline
                Artista & Género & Álbum & Canción \\ \hline
                Twenty One Pilots & Alternativa & \multirow{2}{*}{Trench} & Bandito \\
                Twenty One Pilots & Alternativa &  & My Blood \\
                Dua Lipa & Pop & Future Nostalgia & Levitating \\
                Bach & Clásica/Orquesta & BWV 1041 & Concierto para violín en la menor \\
                Alt-J & Alternativo & An Awesome Wave & Taro \\ \hline
            \end{tabular}
            \caption{Mi música favorita}
            \label{tab:musica}
        \end{center}
    \end{table}
    
Donde ahora la palabra Trench aparece en una celda combinada. Es importante 
mantener un espacio en blanco entre dos signos & en las celdas que ahora están 
ocupadas por la nueva celda combinada. En este caso con la fila de My Blood.

## Centrar verticalmente el contenido de una celda
Para centrar verticalmente las celdas de una tabla es necesario incluir el 
paquete `array` en el preámbulo.

    \usepackage{array}

Este paquete permite definir la alineación de cada columna de un entorno 
`tabular` con el parámetro `m`. El parámetro `m` significa middle, lo cuál 
indicará que el texto debe aparecer verticalmente en el centro. 

    \begin{table}[t]
        \begin{center}
            \begin{tabular}{ | m{2cm} | m{5cm} | }
                \hline País & Ciudades \\ \hline
                España & Madrid, Barcelona, Sevilla, Valencia, Málaga, Zaragoza, Granada, Córdoba, Bilbao, etc. \\ \hline
                Francia & París, Lyon, Burdeos, Toulouse, Marsella, Estrasburgo, Niza, Nantes, Montpellier, etc. \\ \hline
                Alemania & Berlín, Múnich, Hamburgo, Colonia, Stuttgart, Leipzig, Núremberg, Bremen, Hannover, etc. \\ \hline
            \end{tabular}
        \end{center}
    \end{table}

El nombre de los países aparece centrado verticalmente.

## Tablas de más de una página
En algunos casos es necesario crear tablas tan largas que acaban ocupando más de
una página. Para gestionar este tipo de tabas existe el paquete `longtable`.

Podemos crear una tabla con este entorno siguiendo el mismo esquema que en el 
caso del entorno tabular. Es decir:

    \begin{longtable}{ c c }
        Fruta & Cantidad \\ Manzana & 4 \\
        Naranja & 10 \\
        % Muchas líneas
        Plátano & 3
    \end{longtable}

La ventaja de utilizar este entorno es que podemos definir un encabezado para la
tabla que se repetirá al principio de cada página.

En primer lugar podemos definir el encabezado que aparece justo en el comienzo 
de la tabla. Al finalizar la definición de este encabezado escribimos el comando
`\endfirsthed`.

A continuación definimos el encabezado que aparecerá en las páginas
subsiguientes y escribimos el comando `\endhead` al final.

Después de estás dos definiciones incluimos el contenido de la tabla.

        \subsection{Otra tabla}
    \begin{center}
    \begin{longtable}{|l|l|l|}
        \caption[Feasible triples for a highly variable Grid]{Feasible triples for 
        highly variable Grid, MLMMH.} \label{grid_mlmmh} \\

    \hline \multicolumn{1}{|c|}{\textbf{Time (s)}} & \multicolumn{1}{c|}{\textbf{Triple chosen}} & \multicolumn{1}{c|}{\textbf{Other feasible triples}} \\ \hline 
    \endfirsthead

    \multicolumn{3}{c}
    {{\bfseries \tablename\ \thetable{} -- continued from previous page}} \\
    \hline \multicolumn{1}{|c|}{\textbf{Time (s)}} &
    \multicolumn{1}{c|}{\textbf{Triple chosen}} &
    \multicolumn{1}{c|}{\textbf{Other feasible triples}} \\ \hline 
    \endhead

    \hline \multicolumn{3}{|r|}{{Continued on next page}} \\ \hline
    \endfoot

    \hline \hline
    \endlastfoot

    0 & (1, 11, 13725) & (1, 12, 10980), (1, 13, 8235), (2, 2, 0), (3, 1, 0) \\
    2745 & (1, 12, 10980) & (1, 13, 8235), (2, 2, 0), (2, 3, 0), (3, 1, 0) \\
    5490 & (1, 12, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    8235 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    10980 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    13725 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    16470 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    19215 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    21960 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    24705 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    27450 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    30195 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    32940 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    35685 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    38430 & (1, 13, 10980) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    41175 & (1, 12, 13725) & (1, 13, 10980), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    43920 & (1, 13, 10980) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    46665 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    49410 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    52155 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    54900 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    57645 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    60390 & (1, 12, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    63135 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    65880 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    68625 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    71370 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    74115 & (1, 12, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    76860 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    79605 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    82350 & (1, 12, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    85095 & (1, 12, 13725) & (1, 13, 10980), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    87840 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    90585 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    93330 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    96075 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    98820 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    101565 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    104310 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    107055 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    109800 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    112545 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    115290 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    118035 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    120780 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    123525 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    126270 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    129015 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    131760 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    134505 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    137250 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    139995 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    142740 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    145485 & (1, 12, 16470) & (1, 13, 13725), (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    148230 & (2, 2, 2745) & (2, 3, 0), (3, 1, 0) \\
    150975 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    153720 & (1, 12, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    156465 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    159210 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    161955 & (1, 13, 16470) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    164700 & (1, 13, 13725) & (2, 2, 2745), (2, 3, 0), (3, 1, 0) \\
    \end{longtable}
    \end{center}
    
# Crear matrices
Las matrices en LaTeX pueden escribirse dentro del entorno `equation`con el 
entorno `matrix`

    \begin{equation}
        \begin{matrix}
            a & b\\
            c & d
        \end{matrix}
    \end{equation}
    
Como podemos ver las distintas celdas de una fila se separan mediante el símbolo
ampersand (&) y las distintas filas se crean con la doble barra invertida (\\).

Si no se indica nada más la matriz aparece sin ningún tipo de delimitador. Si 
queremos añadir delimitadores (paréntesis, corchete, llaves, etc.) podemos 
utilizar alguna de las siguientes opciones

## Matriz entre paréntesis

    \begin{equation}
        \begin{pmatrix}
            2 & 5 & 0\\
            7 & 3 & 8\\
            3 & 0 & 1
        \end{pmatrix}
    \end{equation}
    
## Matriz entre corchetes
    
    \begin{equation}
        \begin{bmatrix}
            6 & 8 & 1\\
            2 & 9 & 3\\
            4 & 5 & 1
        \end{bmatrix}
    \end{equation}
    
## Matriz entre llaves

    \begin{equation}
        \begin{Bmatrix}
            1 & 7 & 8\\
            0 & 5 & 7\\
            9 & 3 & 4
        \end{Bmatrix}
    \end{equation}

## Matriz entre barras

    \begin{equation}
        \begin{vmatrix}
            2 & 5 & 8\\
            6 & 7 & 1\\
            5 & 0 & 3
        \end{vmatrix}
    \end{equation}
    
## Matriz entre barras dobles

    \begin{equation}
        \begin{Vmatrix}
            3 & 5 & 0\\
            2 & 8 & 6\\
            7 & 1 & 4
        \end{Vmatrix}
    \end{equation}

## Método alternativo para crear matrices con delimitadores
Un método alternativo para crear matrices con delimitadores es utilizar 
simplemente el entorno `matrix` y escribir el delimitador que queremos antes y
después de la matriz. Dado que es necesario que el delimitador se adapte a la 
altura de la matriz debemos escribirlo con los comandos `\left` y `\right`.
Por ejemplo, para crear una matriz entre paréntesis podemos utilizar:

    \begin{equation}
        \left(
        \begin{matrix}
            8 & 0 & 2\\
            2 & 7 & 1\\
            4 & 3 & 9
        \end{matrix}
        \right)
    \end{equation}
    
De modo equivalente podemos utilizar los delimitadores `\left[` y `\right]`,
`\left{` y `\right}`, `\left|` y `\right|` o `\left\|` y `\right\|`

    \begin{equation}
        \left\|
        \begin{matrix}
            8 & 0 & 2\\
            2 & 7 & 1\\
            4 & 3 & 9
        \end{matrix}
        \right\|
    \end{equation}

## Puntos
En matrices a partir de una cierta dimensión a veces es necesario escribir 
puntos suspensivos para indicar la repetición de alguna regla. Dependiendo de la
dirección de los puntos podemos utilizar `\cdots` para puntos horizontales, 
`\vdots` para verticales y `\ddots` para diagonales.

    \begin{equation}
        \begin{pmatrix}
            1 & 0 & \cdots & 0\\
            0 & 1 & \cdots & 0\\
            \vdots & \vdots & \ddots & \vdots\\
            0 & 0 & \cdots & 1
        \end{pmatrix}
    \end{equation}

## Alineación de las celdas
Cuando se utilizan los entornos de matriz, las celdas aparecen por defecto 
centradas. El paquete `mathtools` añade una serie de comandos que permiten 
alinear las celdas a la izquierda o a la derecha. 

Para ello sólo debemos cargar el paquete y aádir un asterisco junto con el 
nombre del entorno matriz que queramos utilizar. A continuación, indicamos la 
alineación entre corchetes: `[l]` para alinear a la izquierda y `[r]` para 
alinear a la derecha.

A continuación se muestra la misma matriz con las tres alineaciones posibles.

Centrada:
    
    \begin{equation}
        \begin{pmatrix}
            63 & 71 & 2\\
            6 & 829 & 12\\
            599 & 9 & 361
        \end{pmatrix}
    \end{equation}

Alineación a la izquierda (con el paquete `mathtools`)

    \begin{equation}
        \begin{pmatrix*}[l]
            63 & 71 & 2\\
            6 & 829 & 12\\
            599 & 9 & 361
        \end{pmatrix*}
    \end{equation}

Alineación a la derecha (con el paquete `mathtools`)

    \begin{equation}
        \begin{pmatrix*}[r]
            63 & 71 & 2\\
            6 & 829 & 12\\
            599 & 9 & 361
        \end{pmatrix*}
    \end{equation}

## Matrices en texto
Si queremos escribir una matriz integrada con el resto del texto la mejor opción
es utilizar el entorno `smallmatrix`.

    Este es un ejemplo de una matriz sin delimitadores $\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}$ integrada en el texto ...

En este primer caso la matriz aparece sin delimitadores. Si queremos añadirlos 
podemos utilizar las siguientes opciones:

    Matriz entre paréntesis $\bigl(\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}\bigr)$ \\

    Matriz entre corchetes $\bigl[\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}\bigr]$ \\
    
    Matriz entre llaves $\bigl\{\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}\bigr\}$ \\

    Matriz entre barras $\bigl|\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}\bigr|$ \\
    
    Matriz entre barras dobles $\bigl\|\begin{smallmatrix}2 & 3\\ 4 & 1\end{smallmatrix}\bigr\|$ \\

# Insertar figuras e imágenes
Para insertar una imagen el primer paso es incluir el paquete `graphicx` en el 
preámbulo del documento.

    \usepackage{graphicx}

Con este paquete es posible utilizar el comando `\includegraphics` que nos 
permite insertar una imagen.

    \includegraphics[opciones]{imagen}

Primero podemos incluir entre corchetes una serie de opciones que indican cómo 
debe mostrarse la imagen. Entre llaves indicamos la ruta donde está guardada la
imagen. Si se encuentra en una carpeta debemos escribir el nombre de la carpeta

    \includegraphics[opciones]{carpeta/imagen}

No es necesario incluir la extensión de la imagen ya que LaTeX es capaz de 
reconocer el archivo con sólo el nombre.

Dentro de las opciones que podemos incluir existen:

    width: Anchura de la imagen
    height: Altura de la imagen
    scale: Factor de escala para aumentar o disminuir el tamaño de la imagen
    angle: Ángulo de rotación con el que debe aparecer la imagen 

La anchura y altura pueden definirse en distintas unidades. Las más habituales 
son:

    in: pulgadas
    cm: centímetros
    mm: milímetros
    pt: puntos (1 punto = 0.3528mm)

Para insertar una imagen forzando una anchura de 8cm y una altura de 5cm sería:

    \includegraphics[width=8cm, height=5cm]{images/imagen01}

También es posible definir la anchura de la imagen en relación directa con la 
longitud de una línea en el documento. Esto se puede hacer mediante el comando
`\linewidth`.

    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor
    incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
    nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi 
    consequat. Quis aute iure reprehenderit in voluptate velit esse cillum 
    dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non 
    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    \includegraphics[width=0.5\linewidth]{images/imagen02}

La opción `scale` cambia el tamaño de la imagen respecto su tamaño original.

    \includegraphics[scale=0.5]{images/imagen03}

Podemos urlizar la opción angle para rotar la imagen en cierto ángulo

    \includegraphics[angle=60]{images/imagen04}

## Posición de la imagen
Si no se incluye ninguna indicación al respecto, LaTeX insertará la figura justo
en el punto donde hemos incluido el comando `\includegraphics`.

Si queremos controlar con más precisión la posición de la figura, así cómo 
añadir una descripción o hacer referencia a ella en el texto, debemos incluir la
figura adentro de un entorno `figure`.

    \begin{figure}
        \includegraphics[opciones]{carpeta/imagen}
    \end{figure}

Una vez declarado el entorno podemos indicar donde queremos que se muestre la 
figura. Las opciones principales son:

    h (here): Para mostrar la imagen aproximadamente en el mismo lugar donde se
    inserta.
    b (bottom): Para mostrar la imagen en la parte superior de la página.
    t (top): Para mostrar la imagen en la parte superior de la página.
    p (page): Para mostrar la imagen en una página a parte.
    !: Para forzar la posición indicada ignorando las reglas de LaTeX.

También es posible combinar distintas opciones. 

    \begin{figure}[htb]
        \includegraphics[opciones]{carpeta/imagen}
    \end{figure}

En este caso LaTeX intentará posicionar la imagen en el punto del documento 
donde hemos insertado estos comandos (siguiendo primero con la instrucción h). 
Si no es posible, intentará posicionar la imagen en la parte superior de la 
imagen (t) y si no, en la parte inferior (b). También podriamos utilizar:
    
    \begin{figure}[b!]
        \includegraphics[opciones]{carpeta/imagen}
    \end{figure}

En este caso LaTeX ingorará sus parámetros internos para forzar la imagen al 
final de la página.

Dentro del entorno `figure` también podemos definir una descripción para que
aparezca debajo de la figura. Para ello disponemos del comando `\caption`

    \begin{figure}[htb]
        \centering
        \includegraphics{images/imagen05}
        \caption{Meme de un michi}
    \end{figure}

Si queremos que la descripción aparezca en la parte superior de la imagen sólo 
tenemos que escribir el comando `\caption` antes de `\includegraphics`.

También podemos definir una etiqueta mediante el comando `\label` para poder
referenciar la figura en algún punto del texto.

    En la figura \ref{fig:michi} podemos ver un bonito michi
    \begin{figure}[htb]
        \centering
        \includegraphics[width=0.5\textwidth]{images/imagen06}
        \caption{Un michi todo tierno}
        \label{fig:michi}
    \end{figure}

Aunque no es imprescindible, se recomienda en el caso de las figuras utilizar 
nombres de etiqueta que empiecen con "fig:". Esto evita posibles confusiones con
otras etiquetas (por ejemplo, para secciones o tablas) que puedan utilizar el
mismo nombre.

Dentro del entorno figura también podemos indicar si la figura debe mostrarse 
centrada, alineada a la izquierda o alineada a la derecha. Para ello incluimos
el comando correspondiente antes del `\includegraphics`:

    \centering %Figura centrada.
    \raggedleft %Figura alineada a la derecha.
    \raggedright %Figura alineada a la izquierda.

Por ejemplo:

    \begin{figure}[htb]
        \centering
        \includegraphics{images/imagen07}
    \end{figure}

Muestra la figura centrada.

## Subfiguras con el paquete subcaption
En algunos casos es necesario incluir una serie de imágenes en un documento para
que aparezcan más o menos juntas. Para estás ocasiones existen el entorno 
`subfigure`, accesible a partir del paquete `subcaption`.

Mediante este paquete podemos crear distintos entorno `subfigure` dentro de un
entorno `figure` situado en un nivel superior. Dentro de cada entonrno 
`subfigure` podemos incluir una imagen junto con su descripción. Por ejemplo,
si quieremos crear una figura que a su vez incluya dos subfiguras, podemos
hacer lo siguiente:

    
    \begin{figure}[h!]
       \centering
        \begin{subfigure}[b]{0.45\textwidth}
            \centering
            \includegraphics[width=\linewidth]{images/imagen08}
            \caption{Michi llorando de lov}
            \label{fig:michiInLov}
        \end{subfigure}
        \hfill
        \begin{subfigure}[b]{0.45\textwidth}
            \includegraphics[width=\linewidth]{images/imagen09}
            \caption{Michi llorando}
            \label{fig:michiLlorando}
        \end{subfigure}
        \caption{Michis}
        \label{fig:michis}
    \end{figure}

En este caso, hemos definido tres etiquetas. Esto nos permite incluir en el 
texto tanto referencias a la figura completa como referencias a alguna de las
subfiguras. Esto podría hacerse mediante:

    En la Figura~\ref{fig:michis} podemos ver dos memes de michis.
    Uno esta llorando de amor~\ref{fig:michiInLov} y uno esta llorando de
    tristeza~\ref{fig:michiLlorando}.

Añadiendo más entornos `subfigure` podemos generar distribuciones de imágenes 
más complejas.

# Listas y enumeraciones
Para crear listas y enumeraciones en LaTeX tenemos los comandos `itemize` y 
`enumerate`.

El funcionamiento de estos dos comandos es simple. Sólo tenemos que crear el 
entorno e indicar cada elemento de una lista con el comando `\item`. El entorno
_itemize_ crea items indicados con un símbolo destintivo mientras que el entorno
_enumerate_ crea listas ordenadas con números

    \begin{itemize}
        \item Edad de Piedra
        \item Edad del Cobre
        \item Edad del Bronce
        \item Edad del Hierro
    \end{itemize}

La misma lista puede crearse exactamente de la misma forma pero dentro del 
entorno _enumerate_ para darle un orden numérico.

    \begin{enumerate}
        \item Edad de Piedra
        \item Edad del Cobre
        \item Edad del Bronce
        \item Edad del Hierro
    \end{enumerate}

También es posible combinar los dos tipos de listados. Por ejemplo, creando un
entorno _enumerate_ dentro de un entorno _itemize_.

    \begin{itemize}
        \item Edad de Piedra
        \begin{enumerate}
            \item Paleolítico
            \item Mesolítico
            \item Neolítico
        \end{enumerate}
        \item Edad del Cobre
        \item Edad del Bronce
        \item Edad del Hierro
    \end{itemize}

No hay ningún problema en incluir el mismo tipo de entorno dentro de otro 
entorno a un nivel superior. Por ejemplo, podemos crear un entorno 
_enumerate_ dentro de otro entorno _enumerate_
    
    \begin{itemize}
        \item Edad de Piedra
        \begin{enumerate}
            \item Paleolítico
            \begin{enumerate}
                \item Paleolítico inferior
                \item Paleolítico medio
                \item Paleolítico superior
            \end{enumerate}
            \item Mesolítico
            \item Neolítico
        \end{enumerate}
        \item Edad del Cobre
        \item Edad del Bronce
        \item Edad del Hierro
    \end{itemize}

Como podemos ver en el ejemplo anterior, LaTeX asigna automáticamente un tipo de
enumeración diferente en cada nivel. El primer nivel se indica con números 
seguidos de punto y el segundo nivel con letras seguidas de paréntesis. El 
tercer nivel aparecería con números seguidos de paréntesis y el cuarto nivel con
letras seguidas de coma volada.

De forma similar, el entorno _itemize_ también asigna símbolos distintos a cada 
nivel. Por ejemplo, si creamos la lista anterior solamente con entorno _itemize_

    \begin{itemize}
        \item Edad de Piedra
        \begin{itemize}
            \item Paleolítico
            \begin{itemize}
                \item Paleolítico inferior
                \item Paleolítico medio
                \item Paleolítico superior
            \end{itemize}
            \item Mesolítico
            \item Neolítico
        \end{itemize}
        \item Edad del Cobre
        \item Edad del Bronce
        \item Edad del Hierro
    \end{itemize}

El primer nivel indica un cuadrao, el segundo un círculo negro, el tercero un
círculo vacío. El cuarto no aparece en el código anterior, pero se representa 
con un rombo.

## Rescribir el tipo de símbolos y enumeración
Las etiquetas y símbolos que aparecen automáticamente en este tipo de lista 
pueden redefinirse mediante el comando `\renewcommand`.

## Entorno enumerate
En el caso del entorno _enumerate_, dependiendo del nivel que queramos cambiar 
tenemos que reescribir el comando `\theenumi, \theenumii, theenumiii` o 
`\theenumiv`, para el primer, segundo, tercer y cuarto nivel respectivamente.
Podemos escoger utilizar números arábugos, números romanos en minúscula, 
números romanos en mayúscula,, letras minúsculas o mayúsculas.
Para cambiar las etiquetas del primer nivel por una de estas cinco opciones 
debemos utilizar respectivamente: 

    \renewcommand{\theenumi}{\arabic{enumi}} %Números arábigos 

    \renewcommand{\theenumi}{\roman{enumi}} %Números romanos en minúscula 
    
    \renewcommand{\theenumi}{\Roman{enumi}} %Números romanos en mayúscula 
    
    \renewcommand{\theenumi}{\alph{enumi}} %Letras minúsculas 
    
    \renewcommand{\theenumi}{\Alph{enumi}} %Letras mayúsculas 

Podemos reescribir el segundo nivel simplemente sustituyendo `\theenumi` por 
`theenumii` y `enumi` por `enumii`. La misma lógica puede aplicarse para 
reescribir el tercer y cuarto nivel.

Aparte de cambiar el tipo de númeración también es posible modificar el tipo de
etiqueta que se aplica, Es decir, si queremos que se muestre un punto, un 
paréntesis, una coma volada, etc. Para estos casos y dependiendo el nivel 
debemos redefinir los comandos `\labelenumi, labelenumii, labelenumiii` y
`\labelenumiv`.

    \renewcommand{\labelenumi}{{\theenumi})} %Paréntesis
    
    \renewcommand{\labelenumi}{{\theenumi}:} %Mostrar dos puntos

Los níveles inferior pueden modificarse cambiando `labelenumi` y `theenumi` por
los comandos del nivel correspondiente.

## Entorno itemize

La redefiniciónde los símbolos que aparecen en las listas del entorno _itemize_
pueden modificarse reescribiendo los comandos `\laelitemi, \labelitemii, 
\labelitemiii` y `\labelitemiv`

    \renewcommand{\labelenumi}{$\bigstar$} %Muestra una estrella en vez de un 
                                            cuadrado.

En este caso, para poder utilizar el comando `\bigstar` es necesario cargar el 
paquete `amssymb` con

    \usepackage{amssymb}

## Crear un índice de contenidos
Crear un índice de contenidos es un proceso automático si hemos definido 
correctamente la estructura del documentos utilizando los comandos para ello.

Los comandos principales para estructurar el documento son `\section, 
\subsection` y `\subsubsection`. Si además trabajamos en un documento de clase 
_book_ o _report_, podemos añadir también capítulos con `\chapter`y partes con
`\part`.

Una vez creada la estructura podemos crear el índice de contenido simplemente 
con el comando `\tableofcontents`.

    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        \tableofcontents
        \chapter{Introducción}
            \section{Conocimientos previos}
            \section{Estado del arte}
        \chapter{Fundamentos teóricos}
            \section{Teoría clásica}
                \subsection{Definición de variables}
                \subsection{Pruebas y refutaciones}
            \section{Hipótesis}
        \chapter{Resultados}
            \section{Simulación de resultados}
                \subsection{Suposiciones}
                \subsection{Modelos}
            \section{Resultados preliminares}
            \section{Resultados postprocesados}
                \subsection{Valores atípicos}
                \subsection{Correlaciones}
        \chapter{Conclusiones}
    \end{document}

Dependiendo del programa que utilicemos para crear los archivos es posible que 
necesitemos compilar más de una vez para que el índice de contenidos aparezca
actualizado.

En la jerarquía introducida con los comandos `\chapter, \section, etc`, cada 
elemento ocupa un nivel distinto.

* Nivel 0: Capítulos.
* Nivel 1: Secciones
* Nivel 2: Subsecciones
* Nivel 3: Subsubsecciones
* Nivel 4: Párrafos
* Nivel 5: Subpárrafos

Por defecto, LaTeX muestra en el índice de contenidos sólo los títulos hasta un 
nivel 2, es decir, incluye las subsecciones pero no las subsubsecciones. Este 
parámetro puede ser modificado mediante el comando

    \setcounter{tocdepth}{0}

Lo cual sólo mostrará únicamente los nombres de los capítulos.

## Índice de figuras
De un modo muy similar podemos crear un índice de figuras en LaTeX con el 
comando `\listoffigures`

Para que esto funcione es necesario haber asignado primero una leyenda a las 
imágenes que queremos que aparezcan en el índice de figuras. Esto debe hacerse
con el comando `\caption`. El siguiente ejemplo muestra un índice de figuras en
un documento con cinco fuguras.

    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \usepackage{graphicx}
    \begin{document}
        \tableofcontents
        \listoffigures
        \chapter{África}
            \begin{figure}
                \includegraphics{mapa_africa}
                \caption{Mapa de África}
            \end{figure}
        \chapter{América}
            \begin{figure}
                \includegraphics{mapa_america}
                \caption{Mapa de América}
            \end{figure}
        \chapter{Asia}
            \begin{figure}
                \includegraphics{mapa_asia}
                \caption{Mapa de Asia}
            \end{figure}
        \chapter{Europa}
            \begin{figure}
                \includegraphics{europa}
                \caption{Mapa de Europa}
            \end{figure}
        \chapter{Oceania}
            \begin{figure}
                \includegraphics{mapa_Ooceania}
                \caption{Mapa de Oceanía}
            \end{figure}
    \end{document}

En algunos casos, especialmente si la leyenda de una figura es muy larga, 
podemos querer mostrar una descripción diferente en el índice de figuras. El 
comando `\caption` permite especificar dos descripciones, una entre corchetes 
para el índice de figuras y otras entre llaves que aparece debajo de la imagen.

    \caption[Descripción que aparece en el índice de figuras]{Descripción que aparece debajo la figura}
    
Por defecto la lista de figuras no aparece en el índice de contenidos. Si 
queremos que aparezca tenemos que introducir adicionalemnte el siguiente comando
en el preámbulo

    \addcontentsline{toc}{chapter}{\listfigurename}

Lo cuál quedaría así en el preámbulo:

    \tableofcontents
    \addcontentsline{toc}{chapter}{\listfigurename}
    \listoffigures

## Índice de tablas
El índice de tablas puede crearse con el comando `\listoftables`. Al igual que 
en el caso del índice de figuras es necesario haber definido una leyenda para 
todas las tablas que queremos que aparezcan en el índice mediante el comando 
`\caption`

    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
    \tableofcontents
    \listoftables
        \chapter{África}
            \begin{table}
                \centering
                \begin{tabular}{ c | c }
                    Dato & Valor \\ \hline
                    Superficie & 30370000 km$^2$\\
                    Población & 1225 millones\\
                    Países & 54
                \end{tabular}
                \caption{Datos de África}
            \end{table}
        \chapter{América}
            \begin{table}
                ...
                \caption{Datos de America}
                ...
            \end{table}
        \chapter{Asia}
            \begin{table}
                ...
                \caption{Datos de Asia}
                ...
            \end{table}
        \chapter{Europa}
            \begin{table}
                ...
                \caption{Datos de Europa}
                ...
            \end{table}
        \chapter{Oceania}
            \begin{table}
                ...
                \caption{Datos de Oceania}
                ...
            \end{table}
    \end{document}

Si queremos que las descripciones que aparecen en el índice de tabalas y debajo
de las tablas sea distinta debemos instroducir las dos descrpciones mediante el
comando `\caption`:

    caption[Descripción que aparece en el índice de tablas]{Descripción que aparece debajo la tabla}

Como recordatorio es importaten saber que por defecto el paquete `babel` en 
españo traduce table como cuadro. Para ajustar esto debemos de agregar la 
opción `es-tabla` junto al paquete:

    \usepackage[spanish,es-tabla]{babel}

Al igual que en el caso del índice de figuras, el índice de tablas no aparece 
por defecto en el índice de contenidos. Si queremos que aparezca tenemos que
indicarlo

    \tableofcontents
    \addcontentsline{toc}{chapter}{\listtablename}
    \listoffigures
    
# Crear una bibliografía
Los métodos recomendados para gestionar la bibliografia en un documento escrito 
en LaTeX han ido evolucionando a lo largo de los años. Actualmente, la forma más 
simple y eficiente de almacenar y citar de fuentes bibliográficas es mediante el
paquete `biblatex`.

Debido a la variedad de herramientas que existen para gestionar una bibliografía
en LaTeX, este tema puede resultar confuso para cualquier principiante. 
Para practicidad presentaremos una única opción para gestionar la bibliografía 
la cual se considera la más adecuada y simple en la actualidad.
Aún así, podemos encontrar referencias a otros paquetes y opciones que han sido 
y siguen siendo de gran uso para muchos usuarios de LaTeX.

Para gestionar las referencias de un documento hacen falta tres elementos:

* Base de datos de las referencias: La información sobre la bibliografía que vas
a usar (autores, títulos, etc.) debe estar guardada en algún sitio. La opción 
más recomendable es guardar todas las referencias bibliográficas en un documento 
separado en formato `.bib`. Si se trabaja con un sólo documento que requiere 
pocas referencias puede considerarse la opción de guardar estos detalles 
directamente al final del documento. 
* Programa para procesar la información bibliográfica: Es necesario un programa
que actúe como vínculo entre la base de datos y el documento escrito en LaTeX. 
Este programa interpreta las referencias guardadas en un documento `.bib` y las
pone a disposición de LaTeX para poder ser citadas en el documento. Las dos 
opciones principales son `biber` y `bibtex`. Probablemente tengas las dos 
opciones disponibles si has instalado una distribición de LaTeX con los 
parámetros que vienen por defecto.
* Paquete para dar formato a la bibliografía: Dentro del documento LaTeX debemos
cargar un paquete para escoger el estilo en que queremos que se muestre la 
bibliografía. La opción clásica ha sido usar el paquete `natbib`. La opción más
recomendable actualmente es usar `biblatex`, considerando el sucesor de algunas
partes de `bibtex` y desarrollado con la intención de solucionar algunas de sus
limitaciones.

El paquete `natbib` sólo funciona en combinación con `bibtex`. `Biblatex`, en 
cambio, puede trabajar tanto con `bibtex` como con `biber`. Usar `biblatex` en
combinación con `biber` aporta una serie de ventajas con respecto a la opción 
`bibtex+natbib`. Por ejemplo, `biber` puede interpretar archivos `.bib` escritos
en códificación utf-8. Esto implica que no debes preocuparte de escribir los 
carácteres especiales (á, é, etc) como `\'a. \'e, ...`

## Crear un archivo de referencias .bib
El primer paso para gestionar la bibliografía es crear un archivo donde guardar
toda la información sobre las referencias que se van a citar.

Este archivo cotneine una entrada distinta para cada fuente bibliográfica.
Por ejemplo, si queremos citar un artículo científico podemos guardar su 
información dentro de un archivo `.bib` como:

    @article{watson53,
        author = "Watson, James and Crick, Francis",
        title = "Molecular structure of nucleic acids",
        journal = "Nature",
        year = "1953",
        number = "4356",
        pages = "737-738",
        volume = "171",
        note = "Notas opcionales"
    }

El ejemplo anterior muestra la información de un artículo científico. Esto se
indica mediante el parámetro `@article`. Existen otro tripo de elementos 
bibliográficos. Por ejemplo `@book` para libros y `@inproceedings` para las 
actas de sesiones de una conferencia. El tipo de elemento que se escoja da 
acceso a distintos atributos que pueden o deben ser definidos.

En el caso del artículo científico es obligatorio definir el nombre del autor o
autores (`author`), el título del artículo (`title`), el nombre de la revista 
que lo ha publicado (`journal`) y el año de publicación. Opcionalmente pueden
definirse también el volumen (`volume`) el número (`number`), las páginas del
artículo (`pages`), el mes de publicación (`month`) y añadir alguna nota 
aclaratoria (`note`).

Todos estos atributos se escriben dentro de las llaves definidas con 
`@article{}`. La estructura siempre debe ser primero la palabra clave 
(e.g author) seguida del signo igual y a continuación el valor concreto. Es 
importante delimitar el valor entre algún símbolo especial, se puede utilizar
tanto llaves como comillas. También es necesario escribir una coma al final de 
cada fila. cuando hay dos o más autores es importante separar cada autor con la
palabra `and`. Esto permite a LaTeX determinar el número de autores y darles el 
formato correcto dependiendo del estilo bibliográfico que se escoja.

Otro elemento esencial es la etiqueta asociada con cada fuente. Esta se escribe
inmediatamente después de abrir las llaves después de indicar el tipo de fuente
bibliográfica. En el caso anterior la etiqueta es `watson53`. Se considera una 
práctica estándar definir la etiqueta con el nombre del autor principal seguida
del año de publicación. Si existe más de una fuente con el mismo autor en el 
mismo año estás pueden diferenciarse añadiendo letras al final de la etiqueta:
`watson53a, watson53b, etc`.

Puedes editar el archivo `.bib` con cualquier programa de edición de texto. 
En el caso de Windows puede ser suficiente con el bloc de notas. En caso de 
tener que gestionar un gran número de referencias bibliográficas, existen 
programas que facilitan esta tarea y ayudan a introducir y mantener ordenadas 
todas las fuentes. Hay una gran oferta de programas, algunos como: Mendeley,
JabRef o Zotero.

## Citar y mostrar la bibliografía en un documento
Una vez creado el documento .bib ya podemos cargarlo en nuestro documento para 
citar las fuentes que conteine.

El siguiente código muestra un ejemplo muy básico de documento con bibliografía.

    \documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \usepackage[backend=biber]{biblatex}
    \bibliography{referencias}
    \begin{document}
        En 1953 James Watson y Francis Crick descubrieron la doble hélice del ADN~\cite{watson53}.
        \printbibliography
    \end{document}

El primer comando importante es cargar el paquete `biblatex`. En este caso 
hemos especificado también que se utilice `biber` como programa de procesado de
la bibliografía (`backed=biber`).

Justo después de haber cargado el paquete `biblatex` podemos especificar la base
de datos de las fuentes bibliográficas con `\bibliography{referencias}`. En este
ejemplo, las referencias están guardadas en un archivo llamado 
`referencias.bib`. No es necesario escribir la extensión `.bib`.

Una vez dentro del entorno documento podemos citar cualquier fuente guardada 
dentro del archivo `.bib` con el comando `\cite` y la etiqueta de la fuente 
entre llaves. En este caso, citamos a la fuente `watson53` con 
`\cite{watson53}`. El símbolo `~` antes del comando `\cite` indica que debe 
haber un espacio entre la última palabra y la cita pero a la vez evitar un salto
de línea entre los dos elementos.

Finalmente utilizamos el comando `\printbibliography` para listar todas las 
fuentes que se han citado a lo largo del documento.

Estos cuatro comandos son todo lo que necesitas para crear y gestionar una 
bibliografía en tu documento.

El único punto en el que pueden surgir problemas es en el momento de compilar. 
Para crear el documento final es necesario primero ejecutar `pdflatex`, después
`biber` y después otra vez `pdflatex` para que se actualice a lista de la 
bibliografía. En algunos programas debemos seleccionar esto manualmente.

Si compilamos el archivo a través de una terminal podemos aplicar el proceso 
mediante:

    pdflatex archivo.tex
    biber archivo
    pdflatex archivo.tex

## Estilos de bibliografía
Hay tres parámetros importantes para definir el estilo de la bibliografía: el 
estilo en el que se citan las fuentes, el estilo de la lista de bibliografía y
el orden de la bibliografía.

Las opciones más habituales para las referencias que aparecen en el texto son 
utilizar números ([1], [2], ...), utilizar códigos alfanuméricos ([SCh43], 
[Sha48], ...) o citar directamente los autores principales y el año (e.g 
Watson y Crick 1953). También es posible mostrar sólo los autores o sólo el año.
Este parámero se especifica en las opciones del paquete _biblatex_ con el 
argumento `citestyle`

    \usepackage[backend=biber,citestyle=numeric]{biblatex}

    \usepackage[backend=biber,citestyle=alphabetic]{biblatex}

    \usepackage[backend=biber,citestyle=authoryear]{biblatex}

Existen otras variantes que puedes encontrar en la documetación del paquete 
`biblatex`. También es interesante saber que en casos concretos puedes forzar la
aparición de sólo el autor utilzando `\citeauthor{}` en lugar de `\cite` o
mostrar sólo el año utilizando `\citeyear`. Si quieres el autor y año entre 
paréntesis puedes usar `\parencite` en lugar de `\cite`.

El otro parámetro importnte es el estilo de las fuentes en la lista 
bibliográfica. De forma similar al estilo de citación existe la opción 
`numeric, alphabetic, authoryear`, para mostrar el estilo. Se especifica en el
argumento `style`

    \usepackage[backend=biber,style=numeric]{biblatex}
    
    \usepackage[backend=biber,style=alphabetic]{biblatex}

    \usepackage[backend=biber,style=authoryear]{biblatex}

También es posible utilizar estilos de bibliografía determinados por 
organizaciones como, por ejemplo, el formato APA (American Psychological 
Association) o el IEEE (Institute of Electrical and Electronics Engineers).

    \usepackage[backend=biber,style=apa]{biblatex}

    \usepackage[backend=biber,style=ieee]{biblatex}

Es importante especificar el orden en que queremos que se muestre la lista de 
bibliografía. Este parámetro se especifica mediante el argumento `sorting`.
Las opciones más habituales son mostrar la lista en el mismo orden en el que las
fuentes aparecen en el texto (`sorting=none`), mostrarlas según el orden 
cronológico de publicación (`sorting=ynt`) o alfabéticamente según el nombre de 
los autores (`sorting=nyt`). Aunque estas son las opciones más habituales también 
es posible ordenar la bibliografía según otros parámetros.

    \usepackage[backend=biber,sorting=none]{biblatex}

    \usepackage[backend=biber,sorting=ynt]{biblatex}

    \usepackage[backend=biber,sorting=nyt]{biblatex}

Todas estas opciones pueden combinarse para generar el estilo bibliográfico que
necesitas. En la mayoría de los casos puedes omitir la definición de 
`citestyle` ya que LaTeX lo escogerá en coherencia con el `style`. Así, una
posible configuración es:

    \usepackage[backend=biber,style=numeric,sorting=none]{biblatex}

O también:

    \usepackage[backend=biber,style=authoryear,sorting=none]{biblatex}

Utilizando `\parencite` para mostrar el autor y aó entre paréntesis.

## Incluir una página web en la bibliografía
Utilizando el paquete `biblatex`, la forma de añadir una página web como 
bibliografía es mediante el elemento `@online` en el archivo `.bib`.

    @online{
        buffett84,
        author = "Warren E. Buffett",
        title = "The Superinvestors of Graham-and-Doddsville",
        year = "1984",
        url = "https: //www8.gsb.columbia.edu/ sites/valueinvesting/files/files/Buffett1984.pdf",
        urldate = "2019-06-30"
    }

Si además añades el paquete `hyperref` en el preámbulo puede convertir la _url_
que aparece en la bibliografía en un link que se abre en el navegador.

## Mostrar la bibliografía en el índice de contendios
Por defecto, la bibliografía no aparece en el índice de contenidos. Si queremos 
que aparezca solamente tiene que especificarlo en el comando 
`\printbibliography` mediante:

    \printbibliography[heading=bibintoc]

## Cambiar el nombre de la bibliografía
Si utilizamos el paquete `babel` con la opción _spanish_, la sección de 
bibliografía se traduce automáticamente como Referencias. Si queremos cambiarle
el título por cualquier otra palabra podemos hacerlo.

    \printbibliography[title={Bibliografía}]

# Referencias cruzadas en LaTeX
Una de las grandes ventajas de trabajar con LaTeX es la facilidad con las que 
se pueden gestionar las referencias cruzadas a los elementos del documento:
imágenes, gráficas, tablas, ecuaciones, etc.

Los dos comandos necesarios para crear una referencia son `\label` para crear 
una etiqueta y `\ref` para referenciarla.

El comando `\label` debe ser llamado dentro del entorno correspondiente al 
elemento que queremos referenciar. Este puede ser el entorno `figure` para una
imágen, `table` para una tabla o `equation` para una ecuación.

Para mantener un cierto orden entre todas las etiquetas de un documento se 
recomienda nombrar las etiquetas correspondientes a imágenes empezando con 
`fig:` para figuras, `tab:` para tablas, `eq:` para ecuaciones, `ch:` para
capítulos y `sec:` para secciones.

    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        \chapter{Introducción} \label{ch:introduccion}
        A continuación presentamos la identidad de Euler: \begin{equation}
            \label{eq:euler}
            e^{i\pi}+1=0
        \end{equation}
        \chapter{Resultados}
        En el Capítulo~\ref{ch:introduccion} puede encontrarse la identidad de Euler expresada en la Ecuación~\ref{eq:euler}.
    \end{document}

En el ejemplo anterior se han creado dos etiquetas (`ch:introducción` y 
`eq:euler`) que se han referenciado posteriormente con el comando `\ref`.

En algunas ocasiones puede ser necesario citar además la página donde se 
encuentra un elemento en concreto. Esto puede hacerse con el mismo sistema pero
utilizando el comando `\pageref` en lugar de `\ref`. Por ejemplo:

    En el Capítulo~\ref{ch:introduccion} puede encontrarse la identidad de Euler expresada
    en la Ecuación~\ref{eq:euler} en la página~\pageref{eq:euler}.

El símbolo `~` entre el comando y la palabra anterior es simplemente para 
indicar que debe haber un espacio entre los dos elementos pero sin introducir un
salto de línea.

Si además de la referencia quieres añadir un link que permita al usuario clicar
para trasladarse al elemento referenciado debes simplemente cargar el paquete 
`hyperref`.
    
    \usepackage{hyperref}

Si quieres mostrar los links pero evitar que aparezca el borde de color 
característico de un link en el pdf puedes hacerlo con

    \usepackage[hidelinks]{hyperref}

# Notas al pie
Para notas al pie de página, LaTeX incorpora el comando `\footnote`. Solamente 
tenemos que escribir este comadno seguido del texto entre llaves que queramos
que aparezca al pie de página.

    \documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        Las notas al pie son muy útiles para añadir detalles interesantes.
        \footnote{Detalles interesantes} A veces estos detalles resultan ser irrelevantes.
        \footnote{Detalles irrelevantes}.
    \end{document}

Por defecto las notas al pie de página aparecen ordenadas en números arábigos.
Si en su lugar queremos utilizar números romanos o letras tenemos que añadir los
siguientes comandos en el preámbulo del documento:

* Números romanos:

    `\renewcommand{\thefootnote}{\Roman{footnote}}`

* Letras minúsculas:

    `\renewcommand{\thefootnote}{\alph{footnote}}`

* Letras mayúsculas:

    `\renewcommand{\thefootnote}{\Alph{footnote}}`

También existe la opción de insertar un símbolo distinto para cada nota:

    \renewcommand{\thefootnote}{\fnsymbol{footnote}}

Sin embargo, si utilizas el paquete `babel` con la configuración en español 
estos símbolos son sustituidos por asteriscos. Si quieres seguir utilizando los
símbolos del sistema anglosajón (que son 6), puedes cargar el paquete babel con 
la opción `es-nolayout`. Ten en cuenta, que esto puede afectar también la forma
en que se muestrsa tus listas de entorno `itemize` y `enumerate`.

    \usepackage[spanish, es-nolayout]{babel}

Si queremos que las notas aparezcan al final del documento y no al pie de cada
página podemos utilizar el paquete `endnotes`.

En este caso debemos definir las notas mediante el comando `\endnote` en lugar 
de `\footnote`. A continuación, debes insertar en el punto donde quieres que 
aparezca todas las notas el comando `\theendnotes`.

    \documentclass{article}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \begin{document}
        \chapter{Introduction}
        Las notas al pie son muy útiles para añadir detalles interesantes.\footnote{Detalles interesantes} A veces estos detalles resultan ser irrelevantes.\footnote{Detalles irrelevantes}.
        \chapter{Conclusiones}
        Aquí tenemos las conclusiones finales.
        \theendnotes
    \end{document}
    
Aunque hayamos cargado el paquete `babel` en español, el paquete `endnotes` 
muestra todas las notas bajo el título en inglés _Notes_. Para traducir este
título tienes que incluir el siguiente comando en el preámbulo del documento:

    \renewcommand{\notesnam}{Notas}

## Numeración manual de las notas al pie
Es interesante saber que también puedes reescribir manualmente el número de las
notas al pie de página. Para ello simplemente tienes que indicar el número que 
desees entre corchete junto con el comando `\footnote`. 

    Las notas al pie son muy útiles para añadir detalles interesantes.\footnote[39]{Detalles interesantes} A veces estos detalles resultan ser irrelevantes.\footnote[40]{Detalles irrelevantes}

En este caso las notas se mostrarán con los números 39 y 40.

# Encabezado y pie de página
Existen principalmente dos opciones para dar formato al encabezado y al pie de
página de un documento escrito en LaTeX.

La opción más sencilla no requiere ningún paquete adicional y consiste 
únicamente en definir un estilo de página en el preámbulo mediante el comando
`\pagestyle`. Con este comando podemos escoger entre cuatro estilos diferentes
para el encabezado y pie de página:

## Empty
    
    \pagestyle{empty}
En este caso tanto el encabezado como el pie de página aparecen totalmente 
vacíos.

## Plain
    
    \pagestyle{plain}
Este es el estilo que aparece por defecto si no se indica nada. Incluye 
únicamente el número de página centrado en el pie de página.

## Headings

    \pagestyle{headings}
Este estilo muestra el número de página y títulos de capítulos o secciones.
La distribución de estos elementos depende de la clase de documento. 
Normalmente incluye el número de página situado a la derecha del encabezado y el
título del capítulo o sección a la izquierda.

## MyHeadings

    \pagestyle{myheadings}
El estilo myheadings es equivalente al headings pero permite definir el texto
que aparece en el encabezado.

Para definir los encabezados en este estilo hay que tener en cuenta si el 
documento está configurado para impresión a una cara o a dos caras. El estilo 
_article_ asume impresión a una cara y el estilo _book_ a dos caras. Siempre es
posible reescribir esta instrucción indicándolo en la clase de documento:

    \documentclass[twoside]{article}
    \documentclass[oneside]{book}

La principal diferencia de un documento a dos caras es que los márgenes son
diferentes entre las páginas pares e impares para tener en cuenta el 
encuadernado del documento.

Si el documento es a una cara, el encabezado será el msimo en todas las páginas.
En este caso, podemos definir el texto del encabezado dentro del estilo 
_myheadings_ mediante:

    \markright{texto en el encabezado}

Cuando el documento es a dos caras podemos definir un encabezado para las 
páginas pares y otro para las impares:

    \markboth{texto página izquierda}{texto página derecha}

Si estas cuatro opciones básicas no son suficientes para generar el tipo de 
encabezado que queremos será necesario utilizar alguna de las opciones que nos 
ofrece el paquete `fancyhdr`. Las opciones de este paquete son mucho más 
amplias y esto hace que su uso pueda resultar inicialmente un poco confuso.

## Encabezados y pies de página personalizados con el paquete fancyhdr
El primer paso para personalizar los encabezados y pies de página con el 
paquete `fancyhdr` es cargar el paquete en el preámbulo del documento y 
seleccionar el estilo `fancy` como estilo de página:

    \usepackage{fancyhdr}
    \pagestyle{fancy}

Dependiendo de la configuración del documento que utilicemos es posible que 
recibas un error relacionado con este paquete duran el compilado del documento.
En este caso es probable que puedas solucionar el problema añadiendo también el
comando `\setlength{\headheight}{13.6pt}`. Puedes modificar el número de 
`\headheight` hasta encontrar la configuración que nosotros prefieramos.

Cuando utilizamos el paquete _fancyhdr_ un aspecto importante a entender es que 
existen en total dieciocho elementos o puntos que podemos modificar. En primer
lugar existen tres tipos de páginas distintas: las páginas pares, las páginas
impares y las páginas iniciales de capítulos. Cada página tiene un encabezado y
un pie de página. Finalmente, en cada encabezado y pie de página podemos editar
el elemento de la izquierda, el centro y la derecha. En total, esto da como 
resultado 18 elementos (3x2x3).

En primer lugar veremos cómo definir los encabezados de las páginas pares e 
impares. Para el elemento situado a la izquierda debemos utilizar el comando 
`\lhead` (l hace referencia aunque no lo esperemos a left y head a header).
    
    \lhead[encabezado a la izquierda en páginas pares]{encabezado a la izquierda en páginas impares}

De forma similar podemos definir el elemento en el centro del encabezado con
`\chead` y a la derecha con `\rhead`

    \chead[encabezado en el centro en páginas pares]{encabezado en el centro en páginas impares}
    \rhead[encabezado a la derecha en páginas pares]{encabezado a la derecha en páginas impares}
    
La definición de los pies de página sigue la misma lógica y se especifica con 
los comandos:

    \lfoot[pie de página a la izquierda en páginas pares]{pie de página a la izquierda en páginas impares}
    \cfoot[pie de página en el centro en páginas pares]{pie de página en el centro en páginas impares}
    \rfoot[pie de página a la derecha en páginas pares]{pie de página a la derecha en páginas impares}

Estos seis comandos son suficientes para definir los encabezados y pies de 
página de las páginas pares e impares. Lógicamente no es necesario definir los
12 puntos anteriores y podemos dejar espacios vacíos donde queramos.

Para evitar resultados inesperados es recomendable incluir primero todo el 
comando `\fancyhf{}` para borrar los valores que vienen por defecto.

A parte de un texto predefinido podemos utilizar los siguientes comandos:

* `\thepage`: Muestra el número de página.
* `\today`: Muestra la fecha actual.
* `\thechapter`: Muestra la palabra Capítulo (o Chapter si se escribe en inglés)
* `\leftmark`: Muestra el título del elemento situado en el nivel superior en la
  jerarquía del documento (en los documentos book esto es el nombre del 
  capítulo, en los articulos es la sección)
* `\rightmark`: Muestra el título del elemento situado en el segundo nivel en la
  jerarquía del documento (en los documentos book esto es el nombre de la 
  sección, en los artículos es la subsección)

Una posible configuración sería:
    
    \fancyhf{}
    \lhead[\leftmark]{Nombre Autor}
    \rhead[Nombre Autor]{\rightmark}
    \lfoot[\thepage]{}
    \rfoot[]{\thepage}

El primer comando de este ejemplo es `\fancyhf{}` para borrar la configuración 
que viene por defecto.

El segundo comando `\lhead[\leftmark]{Nombre Autor}` inserta el título del
capítulo a la izquierda del encabezado de las páginas pares y el nombre del 
autor a la izquierda de las páginas impares.

El tercer comando `\rhead [Nombre Autor]{\rightmark}` inserta el nombre del 
autor a la derecha del encabezado de las páginas pares y el título de la 
sección a la derecha de las páginas impares.

El cuarto comando `\lfoot[\thepage]{}` inserta el número de página a la 
izquierda del pie de página de las páginas pares.

El quinto comando `\rfoot[]{\thepage}` inserta el número de página a la
derecha del pie de página de las páginas impares.

El paquete `fancyhdr` también permite insertar líneas horizontales para separar 
estos elementos del resto de la página. Podemos definir el grueso de estás 
líneas mediante los comandos:

    \renewcommand{\headrulewidth}{0.5pt}
    \renewcommand{\footrulewidth}{0pt}

La configuración anterior mostraría una línea horizontal de 0.5pt en el 
encabezado y borraría la línea en el pie de pagina (0 pt).

Estos comandos no definen el estilo de los encabezados y pies de página en las
páginas inciiales de cada capítulo. Por defecto, estás utilizarán el estilo 
_plain_, es decir, mostrarán el número de página en el centro del pie de página.
Si queremos modificar esta configuración la opción más fácil es reescribir las 
instrucciones del estilo _plain_

    \fancypagestyle{plain}{
    %modificaciones
    }

Una opción es dejar estas páginas sin nigún tipo de encabezado o pie de página,
eliminando también las líneas que aparecen en las otras páginas

    \fancypagestyle{plain}{
    fancyhf{}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
    }

Para mostrar los elementos específicos en las páginas iniciales de los capítulos
debemos incluir también los comandos `\fancyhead` o `\fancyfoot`. En estos
comandos podemos definir la posición (L: izquierda, C: centro, R: derecha) y el 
texto

    \fancyhead[L]{Nombre Autor}

Mostrará _Nombre Autor_ a la izquierda del encabezado de las páginas iniciales 
de los capítulos. También podríamos incluir el número de página a la derecha del
pie de página con:

    \fancyfoot[R]{\thepage}

Para mostrar estos elementos tenemos que incluirlos en la redefinición del 
estilo plain

    \fancypagestyle{plain}{
        fancyhf{}
        \fancyhead[L]{Nombre 
        \fancyfoot[R]{\thepage}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
    }

El sigiente código muestra una plantilla básica con la que podrías empezar para
definir el encabezado y pie de página en tu documento
    
    \documentclass{book}
    \usepackage[spanish]{babel}
    \usepackage[utf8]{inputenc}
    \usepackage{fancyhdr}
    fancyhf{}
    \lhead[\leftmark]{Nombre Autor}
    \rhead[Nombre Autor]{\rightmark}
    \lfoot[\thepage]{}
    \rfoot[]{\thepage}
    \renewcommand{\headrulewidth}{0.5pt}
    \renewcommand{\footrulewidth}{0pt}
    \fancypagestyle{plain}{
        fancyhf{}
        \fancyhead[L]{Nombre Autor}
        \fancyfoot[R]{\thepage}
        \renewcommand{\headrulewidth}{0pt}
        \renewcommand{\footrulewidth}{0pt}
    }
    \pagestyle{fancy}
    \begin{document}
        \chapter{Animales}
            \section{Vertebrados}
                \subsection{Mamíferos}
                \subsection{Anfibios}
                \subsection{Reptiles}
                \subsection{...}
            \section{Invertebrados}
                \subsection{Gusanos}
                \subsection{Moluscos}
                \subsection{Medusas}
                \subsection{...}
    \end{document}
    
# Tipo de letra
En general, en un entorno LaTeX puede distinguirse entre tres familias de 
letras. Estas tres familias reciben el nombre _roman, sans serif y teletype_.
La familia roman es la familia utilizada por defecto. 

Podemos escribir texto en estás tres familias mediante los comandos:

    \textrm{Texto escrito en familia roman}
    \textsf{Texto escrito en familia sans serif}
    \texttt{Texto escrito en familia de mecanografiado}
    
Es importante escribir entre llaves el texto correspondiente. En caso de querer 
cambiar la familia de forma permanente pueden utilizarse los comandos

    \rmfamily
    \sffamily
    \ttfamily

El texto escrito después de estos comandos aparece en la familia indicada hasta 
el punto en que se indique una instrucción alternativa con otro de los comandos
presentados

    \rmfamily
    Aquí el texto aparece en el estilo de la familia roman
    \sffamily
    A partir de este punto el texto es sans serif
    \ttfamily
    Por último tenemos texto de tipo mecanografiado

También es posible utilizar distintas formas de letras. Por defecto las letras
aparecen rectas, pero podemos cambiarlas a distintas formas de cursiva, 
mayúscula, etc. El procedimiento es equivalente y los principales comandos
disponibles son los siguientes:

    \textit{Texto escrito en cursiva}
    \textup{Texto escrito en letras rectas}
    \textsl{Texto roman de estilo inclinado}
    \textsc{Texto escrito en mayúsculas pequeñas}

De forma similar podemos utlizar los cuatro siguientes comandos para cambiar la 
forma de la letra a partir de un punto determinado

    \itshape
    \upshape
    \slshape
    \scshape

Para forzar que las letras aparezcan en mayúscula tenemos el comando:

    \uppercase{Este texto aparecerá en mayúscula}

Para determinar el grosor del trazo de las letras tenemos tres opciones. Una 
opción utilizada habitualmente son las negritas, que pueden insertarse mediante
el comando:

    \textbf{Texto en negrita} 

También tenemos una opción intermedia, que es la opción utilizada por defecto

    \textmd{Texto de grosor intermedio} 

Para cambiar a un entorno con este tipo de letras podemos utilizar los comandos
    
    \bfseries
    \mdseries

También es interesante indicar que existe el comando `\emph{}` para crear texto
que aparece de alguna forma enfatizado.

    En esta línea tenemos una \emph{palabra} enfatizada. 

Normalmente el texto enfatizado aparece en cursiva. Sin embargo, también podemos
utilizar el comando `\emph{}` dentro de un texto que ya está escrito en cursiva.
En este caso, el texto aparecerá con otro estilo. Las características del 
enfatizado dependen en última intancia de la configuración y tipo de fuente 
utilizado en el documento.

    \itshape
    En esta línea tenemos una \emph{palabra} enfatizada. 

En caso de querer resaltar un texto mediante un subrayado la opción más 
recomendable es utilizar el comando `\ul{}` disponible mediante el paquete 
`soul`.

    \usepackage{soul}
    Estas definiciones son \ul{muy importantes}. 

## Familias de letras en modo matemático
Dentro del modo matemático a menudo es necesario escribir letras con algún tipo
de fuente distinto al habitual. Las opciones más utilizadas en estos casos son:

* Letras caligráficas con el comando `\mathcal`:
  
  `$\mathcal{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$`

* Letras negritas de pizarra con el comando `\mathbb` (requiere 
  `\usepackage{amssymb}`)
  
  `$\mathbb{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$`

* Letras Fraktur con el comando `\mathfrak` (requiere `\usepackage{amssymb}`)

    `$\mathfrak{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$`

* Letras formales Ralph SMith con el comando `\mathscr` (requiere 
  `\usepackage{mathrsfs}`)
  
  `$\mathscr{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$`

* Letras de tipo Euler con el comando `\mathscr` (requiere 
  `\usepackage[mathscr]{euscript}`)
  
  `$\mathscr{ABCDEFGHIJKLMNOPQRSTUVWXYZ}$`

# Tamaño de letra
Por defecto un documento está definido con un tamaño de letra de 10pt.

Podemos especificar otro tamaño inicial al declarar la clase de documento
mediante 

    \documentclass[12pt]{article}

En relación a este tamaño podemos definir otros tamaños de letra mediante los
siguientes comandos

    %Diferentes tamaños
    {\tiny Texto en tamaño tiny} 
    {\scriptsize Texto en tamaño scriptsize}
    {\footnotesize Texto en tamaño footnotesize}
    {\small Texto en tamaño small}
    {\normalsize Texto en tamaño normalsize}
    {\large Texto en tamaño large}
    {\Large Texto en tamaño Large}
    {\LARGE Texto en tamaño LARGE}
    {\huge Texto en tamaño huge}
    {\Huge Texto en tamaño Huge}

# Agradecimeintos

Con todo lo que hemos leído en este archivo podemos darnos una idea de cómo 
realizar documentos en LaTeX de manera óptima y hemos aprendido las bases para 
poder hacerlo nosotros mismos.

Por tu atención, gracias por leer este archivo.
