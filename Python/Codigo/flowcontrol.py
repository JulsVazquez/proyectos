#!/usr/bin/env python3

# Probaremos algunas estructuras de control en Python

# Estructura de if
x = int(input("Ingresa un entero: "))
if x < 0:
    x = 0
    print('Número negativo cambiado a cero.')
elif x == 0:
    print('Cero')
elif x == 1:
    print('Uno')
else:
    print('More')

# Estructura de for
words = ['cat', 'window', 'defenestrate']
for w in words:
    print(w, len(w))

# Función range()
for i in range(5):
    print(i)

# Función range() con parámetros para especificar el incremento o decremento
for i in range(5,10):
    print(i) # Imprime 5, 6, 7, 8, 9

for i in range(0,10,3): #Imprime multiplos de 3 antes del 10
    print(i)

# Imprime desde el -10 hasta el -100 decrementando -30
for i in range(-10, -100, -30):
    print(i)

# Combinamos funciones para obtener una secuencia con indices
a = ['Mary', 'tiene', 'un', 'pequeño', 'corderito']
for i in range(len(a)):
    print(i, a[i])

# Una función que toma un iterable es sum()
print(sum(range(4))) # Imprime un 6

# Función que toma un iterable y regresa un iterable list()
print(list(range(4))) # [0, 1, 2, 3]

# break y continue
print("Vemos el funcionamiento de break")
for n in range(2, 10):
    for x in range(2, n):
        if n % x == 0:
            print(n, 'es igual', x, '*', n//x)
            break
    else:
        print(n, 'es un número primo')

# Propuesta Chelo
for contador in range(21):
    dt = (contador)*0.5
    print(dt)
