# Esta es una lista en Python
squares = [1, 4, 9, 16, 26]
print(squares)

# Las listas se pueden recorrer por indices
print(squares[0])
print(squares[-1])
print(squares[-3:]) # Esto devuelve una nueva lista [9, 16, 25]

# Podemos regresar una copia igual de la lista que creamos
print(squares[:])

# Podemos concatenarlas
print(squares + [36, 49, 64, 81, 100])

# Las listas son mutables, podemos cambiar su contenido
cubes = [1, 8, 27, 65, 125]
cubes[3] = 64 # Cambiamos el valor de 65 a 64 que es el correcto.
print(cubes)

# Podemos agregar cosas al final de la lista con el método append()
cubes.append(216)
cubes.append(7 ** 3)
print(cubes)

# Podemos cambiar el tamaño de una lista
letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
print(letters)
letters[2:5] = ['C', 'D', 'E']
print(letters)
letters[2:5] = []
print(letters)
letters[:] = []
print(letters)

# La función len() sigue funcionando aquí
letters = ['a', 'b', 'c', 'd']
print(letters)
print(len(letters))

# Podemos contener listas en listas
a = ['a', 'b', 'c']
n = [1, 2, 3]
x = [a, n]
print(x)
print(x[0])
print(x[0][1])

# Lista de elementos
listaCarros = ["Ford", "BMW", "Volvo"]
listaCarros.sort()
print(listaCarros)
