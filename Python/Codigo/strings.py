#!/usr/bin/env python3

# Imprimimos sin el string en blanco del inicio
print("""\
Usage: thingy [OPTIONS]
     -h                     Displays this usage message
     -H                     Hostname to connect to
""")

# 3 veces 'un' seguido de un 'ium'
un = 3 * 'un' + 'ium'
print(un)

# Concatenando de manera literal

py = 'Py' 'thon'
print(py)

# Concatenando variables con literales
prefix = 'Py'
print(prefix+'thon3.9')

# Imprimiendo carácteres desde su posición
print(py[0])
print(py[1])
print(py[2])
print(py[-3])
print(py[-2])
print(py[-1])

# Imprimiendo carácteres por longitud explicita
print(py[-6:])

# Creando un nuevo string ya que son inmutables
jython = 'J' + py[1:]
print(jython)
pypy = py[:2] + 'py'
print(pypy)

# Función que nos devuelve el tamaño de un string
s = 'supercalifragilisticoespiralidoso'
print(len(s))

# Funciones de str para poder pasar strings como objetos

miNombre = 'Julio Vázquez Alvarez'
sub = 'a'
# Imprime mi nombre con la primera letra en mayúscula y el resto en minúscula
print(str.capitalize(miNombre))

# Método casefold(), devuelve todas las letras en minúsculas
print(str.casefold(miNombre))

# Método str.count(sub[,start[,end]]).
# Regresa el número de ocurrencias de un substring en un rango [inicio,fin].
print(miNombre.count(sub, 1, 21))
