# Notas del Curso de Git 2021

## ¿Qué es Git?
Git es un software de control de versiones diseñado por Linus Torvalds, pensando
en la eficicencia, la confiabilidad y compatibilidad del mantenimiento de 
versiones de aplicaciones cuando estas tienen un gran número de archvios de 
código fuente. Su propósito es llevar un registro de los cambios en archivos de 
computadora incluyendo coordinar el trabajo de varias personas que realizan 
sobre archivos compartidos en un repositorio de código.

El diseño de Git se basó en BitKeeper y en Monotone. Y originalmente fue 
diseñado como un motor de sistema de control de versiones de bajo nivel sobre el
cual otros podría codificar interfaces frontales, tales como Cogito o StGIT. 
Desde ese momento hasta ahora el núcleo del proyecto Git se volvió un sistema de
control de versiones completo, utilizable de manera directa.

Linus Torvalds buscaba un sistema distribuido que pudiera usar de forma similar 
a BitKeeper, pero ningún sistema bajo software libre cumplía con los 
requerimientos y en particular por el desempeño. El diseño de Git mantiene una 
enorme cantidad de código distribuida y gestionada por muha gente, que incide en
numerosos detalles de rendimiento y de la necesidad de rapidez en una primera
implementación.

Las características más relevantes que contiene Git son:
* Fuerte apoyo al desarrollo no lineal, por lo que da rapidez en la gestión de 
  ramas y mezclado de diferentes versiones. Incluye herramientas específicas 
  para navegar y visualizar un historial de desarrollo no lineal.
* Gestión distribuida. Git le da a cada programador una copia local del 
  historial del desarrollo entero, y los cambios se propagan entre los 
  repositorios locales. Los cambios se importan como ramas adicionales y pueden
  ser fusionados en la misma manera que se hace con la rama local.
* Los almacenes de información pueden publicarse por HTTP, FTP, rsync o mediante
  un protocolo nativo, ya sea a través de una conexión TCP/IP simple o a través 
  de cifrado SSG (el más recomendable).
* Gestión eficiente de proyectos grandes, dada la rapidez de gestion de 
  diferencias entre archivos, entre otras mejoras de optimización de velocidad
  de ejecución.
* Compatibilidad con Github y Microsoft Visual Studio Code



## Cómo instalar Git en Windows, Linux y macOS

* Windows:
  Existe algo llamado Git for Windows: https://gitforwindows.org/
  Podemos seguir los pasos que nos da la aplicación.
  En el caso de Windows tenemos que abrir la Git BASH.
  Igual tenemos Github Desktop: https://desktop.github.com/
* Linux:
  A traves de la Terminal de acuerdo a nuestro gestor de paquetes
  `# apt-get install git`
  `# pacman -S git`
  `# dnf -y install git`
* MacOS:
  A traves de la Terminal o podemos hacerlo con Github Desktop
  https://desktop.github.com/

¿Cómo saber si ya tenemos git instalado en nuestra máquina?

Podemos hacerlo con el comando `git --version`.

En este curso se usara siempre la bash de Git, trabajaremos bajo terminal, ya 
que aprenderlo de manera nativa en vez de un entorno visual ayuda muchisimo más
que desde un entorno.
## Creando el primer commit

Primero tenemos que hacer una carpeta de nuestro proyecto, en este caso 
simularemos que estamos aprendiendo un nuevo lenguaje de programación el cuál 
por simplicidad será Python, dentro de él haremos nuestros cambios:

    $ mkdir Proyectos
    $ cd Proyectos
    $ mkdir Python
    $ cd Python

Ahora tenemos que crear un repositorio de git con un subcomando.
Si nosotros escribimos en la terminal `git` podemos ver muchos comandos, 
entonces uno de ellos es `init`, con él creamos un repositorio

    `$ git init`

En este caso se crea un archivo oculto, pero nosotros podemos listarlo, de esta
forma dentro de nuestra carpeta Python se ha creado una carpeta `.git`.

    `ls -a`

Si queremos examinar el estado de nuestro repositorio con el comando `status`

    `$ git status`

De esta forma nosotros ya podemos crear una serie de archivos para poder 
trabajar con git. En este caso crearemos el archivo `numbers.py`.

    `$ touch numbers.py`
    `$ emacs numbers.py`

Y guardamos nuestro archivo.
Si nosotros ingresamos el comando `status`, podemos visualizar que esta pasando 
en Git hasta este momento:

    $ git status
    En la rama master

    No hay commits todavía

    Archivos sin seguimiento:
    (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)
    numbers.py

    no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)
 
Esto quiere decir que Git sabe que en la carpeta hay un cambio, pero que no lo 
hemos agregado.

Ahora tendremos que registrar nuestro cambio, para modificar ese cambio después.
Cuando hacemos un cambio le decimos a git que archivos o trozos hemos modificado
así que usaremos el comando `add`

    $ git add numbers.py

Ahora la salida sería la siguiente:

    $ git status
    En la rama master

    No hay commits todavía

    Cambios a ser confirmados:
    (usa "git rm --cached <archivo>..." para sacar del área de stage) 
    nuevo archivo:  numbers.py

Ahora esta el el stage, que son archivos que estarán listos para hacer un commit.

Tendremos que especificar este cambio con el comando `commit`

    $ git commit

De esta manera se nos abrirá nuestro editor predeterminado (en mi caso Emacs), 
para poder escribir el mensaje de nuestro commit. Para términos prácticos
nombraremos a este cambio como _Archivo numbers.py para los operadores básicos_.

En el caso de Emacs el guardado es con el comando `Ctrl C + Ctrl C`, hay que 
verificar cómo se guarda en nuestro editor para poder hacer el guardado del
commit.

## ¿Qué es el working directory?

Ya que ahora tenemos nuestro primer commit elaborado necesitaremos hablar un 
poco de las palabras técnicas que se usan en git. 

Para nosotros ver nuestro historial de commits que hemos hecho en git tenemos el
comando:
    
    $ git log

De esta forma hasta el momento tendremos la siguiente salida:

    commit b22faddfa98b48aba7f831b27d859ef3c10cc705 (HEAD -> master)
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 15:58:16 2021 -0500

    Archivo numbers.py para los operadores básicos

Nota: Es posible que el hash sh1 sea diferente.

Podemos ver el Autor, la fecha y el mensaje del commit.

Si nosotros hacemos un `git status`, obtendremos la siguiente salida:

    En la rama master
    nada para hacer commit, el árbol de trabajo está limpio

Esto quiere decir que tenemos todo correcto, pero veamos que sucede si nosotros
modifcamos el archivo `numbers.py`

Después de hacer la modificación podemos verificar como se encuentra la rama de
trabajo.

    En la rama master
    Cambios no rastreados para el commit:
    (usa "git add <archivo>..." para actualizar lo que será confirmado)
    (usa "git restore <archivo>..." para descartar los cambios en el directorio de trabajo)
    modificado:     numbers.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")
    
Además la parte `modificado:  numbers.py` aparece en nuestra terminal en color _rojo_.

Podemos ver en detalle que modificamos con el comando `git diff`, de esta manera
podemos ver la lista de cambios que aún no están en el staged. 

Las líneas que agregamos se encuentran en verde y con un `+` y las que 
eliminemos con rojo y con un `-`.

Nuestra salida se vería de la siguiente manera:

    $ git diff
    diff --git a/numbers.py b/numbers.py
    index 61a2415..2c29a3b 100644
    --- a/numbers.py
    +++ b/numbers.py
    @@ -7,6 +7,7 @@ print (b)
    c = 8 / 5
    print(c)
    
    +# Aqui tenemos una fórmula sencilla
    width=20
    height=5*9
    print(width*height)

Cuando hacemos una modificación y está no se encuentra en el staged para poder
agregarla necesitamos agregarla con el comando `git add`.

    $ git add numbers.py
    
O si tenemos mucha prisa y queremos meter todos los archivos que hemos 
modificado de golpe podemos hacerlo con

    $ git add .

Preferimos usar el comando `add` con archivos separados, ya que cuando usamos
ese comando se manda al stage. Esta área es temporal entre la modificación de un
archivo y un commit.

Pero si ya hemos agregado algo en el stage y hacemos un cambio de nuevo al mismo
archivo obtendremos lo siguiente.

Para esto ya hemos agregado anteriormente el archivo `numbers.py` al stage y lo
volveremos a modificar con una nueva fórmula.

Después de modificar el archivo `numbers.py` haremos un `git status` y 
obtendremos la siguiente salida:

    En la rama master
    Cambios a ser confirmados:
    (usa "git restore --staged <archivo>..." para sacar del área de stage)
	  modificado:     numbers.py

    Cambios no rastreados para el commit:
    (usa "git add <archivo>..." para actualizar lo que será confirmado)
    (usa "git restore <archivo>..." para descartar los cambios en el directorio de trabajo)
	  modificado:     numbers.py

Hasta que no hagamos un `git add` ese cambio que hemos hecho no será registrado.
Podemos ver eso haciendo un `git diff` para ver la diferencia que hemos agregado
y/o eliminado, en nuestro caso la salida es la siguiente:

    diff --git a/numbers.py b/numbers.py
    index 2c29a3b..e7afeda 100644
    --- a/numbers.py
    +++ b/numbers.py
    @@ -11,3 +11,6 @@ print(c)
    width=20
    height=5*9
    print(width*height)
    +
    +# Área de un triangulo
    +print((width*height)/2)

Si nosotros hacemos un commit en este punto no será agregado el cambio que se
muestra en el `git diff`

Entonces si hacemos un commit y vemos nuestros commits con `git log` tendríamos
lo siguiente:

    commit 7da997f262ff6299f49c1bc05eb7d434d6da3b51 (HEAD -> master)
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 18:15:02 2021 -0500

    Agregamos una fórmula: width*height

    commit b22faddfa98b48aba7f831b27d859ef3c10cc705
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 15:58:16 2021 -0500

    Archivo numbers.py para los operadores básicos

Recordemos que los hashes siempre serán diferentes.

Ahora si nosotros hacemos un `git status` tendríamos lo siguiente:

    En la rama master
    Cambios no rastreados para el commit:
    (usa "git add <archivo>..." para actualizar lo que será confirmado)
    (usa "git restore <archivo>..." para descartar los cambios en el directorio de trabajo)
	  modificado:     numbers.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")

Ya que no hemos agregado ese último cambio, pero que pasa si es un cambio que 
quisieramos agregar al último commit, para no tener que volver a hacer otro
commit porque es innecesario para nosotros ya que es un cambio mínimo.

Agreguemos ese cambio que habíamos tenido al stage con `git add`

Podemos hacer lo siguiente:

    $ git commit --amend

Con esto el mensaje que nos lanza nuestro editor es diferente ya que estamos
agregando el pequeño cambio que hicimos.

Podemos revisar esto con `git log`, así que hagamoslo:

    $ git log
    commit 50389f8327d24fee564b547d732ac8abd4fcaee1 (HEAD -> master)
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 18:15:02 2021 -0500

    Agregamos una fórmula: width*height

    commit b22faddfa98b48aba7f831b27d859ef3c10cc705
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 15:58:16 2021 -0500

    Archivo numbers.py para los operadores básicos

Pero veamos que ha sucedido, ahora el último commit ha cambiado su hash, ya que
arriba esta como `7da997` y ahora esta como `50389f`

Con esto podemos ver que es imposible cambiar algo y que Git no se de cuenta ya
que cambia su hash. 

Pero este paso sólo podemos hacerlo con el último commit. Ya que como cada 
commit esta vínculado con su anterior sería un efecto en cadena bastante malo.


## ¿Cómo deshago una modificación?
Veamos ahora como deshacer algunas modificaciones, imaginemos que hemos borrado 
las variables de nuestro programa y lo guardamos. E imaginemos que esas 
variables las ocupamos en otro archivo o parte del código. ¿Qué hacemos ahora?

Si nosotros hacemos un `git status` podemos ver lo siguiente:

    En la rama master
    Cambios no rastreados para el commit:
    (usa "git add <archivo>..." para actualizar lo que será confirmado)
    (usa "git restore <archivo>..." para descartar los cambios en el directorio de trabajo)
	  modificado:     numbers.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")

Si examinamos con `git diff` podemos ver lo que acabamos de hacer:

    diff --git a/numbers.py b/numbers.py
    index e7afeda..aeaf263 100644
    --- a/numbers.py
    +++ b/numbers.py
    @@ -1,11 +1,5 @@
    # Archivo numbers.py para ver los operadores básicos de Python.

    -a = 2+2
    -print(a)
    -b = (50 - 5*6) / 4
    -print (b)
    -c = 8 / 5
    -print(c)

    # Aqui tenemos una fórmula sencilla
    width=20

Y justo ahí están las variables que hemos borrado. Pero justo con el siguiente
comando podemos hacerlo llamado `checkout` el cual puede 
descartar cambios:

    $ git checkout -- numbers.py

Y si volvemos a abrir el archivo podemos ver que ha regresado con la información
anterior.

Pero, ¿qué pasa si hemos hecho una modificación que ya mandamos al staged area?

Volvamos a borrar nuestras variables, guardamos el archivo y hacemos un `add` 
del mismo.

Si hemos hecho el `git add numbers.py` obtendremos la siguiente salida:

    $ git status
    En la rama master
    Cambios a ser confirmados:
    (usa "git restore --staged <archivo>..." para sacar del área de stage)
	  modificado:     numbers.py

Pero si ahora queremos ocupar el comando `git checkout -- numbers.py` veremos 
que no es posible, no nos regresa el archivo como tal.

Esto sucede porque el archivo ya se encuentra en el stage area, por lo que 
tenemos que sacarlo de ahí y entonces ¿cómo hacemos eso?

Pues podemos hacerlo con el siguiente comando:

    $ git reset HEAD numbers.py
    Cambios fuera del área de stage tras el reset:
    M   numbers.py

Ahora sí podemos usar el comando `git checkout -- numbers.py` para recuperar la
información de nuestro archivo
  
    $ git checkout -- numbers.py

Y si volvemos a abrir el archivo veremos que está como lo dejamos anteriormente.

Entonces con esto realmente lo único que teníamos que hacer era borrar la 
primera variable en vez de todas, ya que es la única que no ocupamos y usa 
memoria.

Y con esto podemos ya hacer un nuevo commit donde borramos esa variable.

    $ git add numbers.py
    $ git commit
    Variable eliminada
    [master 2225b3e] Borramos variable que usa memoria.
    1 file changed, 2 deletions(-)

Y listo.

## ¿Cómo deshago un commit?
Ahora supongamos que por alguna extraña razón decidimos borrar las otras 
variables que ya habiamos rescatado porque por alguna razón encontramos una
forma más óptima de hacerlo en nuestro código y ya hicimos un commit a ese
cambio.

Podemos verificar nuestros commits hasta ahora con `git log`, por cuestiones de
lectura sólo pondré los últimos dos commits realizados.
    
    $ git log
    commit d910c7b0c542854491abb25a983c07011c39e4b7 (HEAD -> master)
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 18:58:41 2021 -0500

    Elimino las variables porque sí

    commit 2225b3ece6a3991f98a860e0d7b5e7ada3106cae
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Thu May 20 18:56:06 2021 -0500

    Borramos variable que usa memoria.
    ...

Si nosotros hacemos un `git status` para verificar nuestro estado del proyecto
obtendremos el siguiente mensaje:

    $ git status
    En la rama master
    nada para hacer commit, el árbol de trabajo está limpio

Ahora veamos cómo podemos deshacer ese último commit. Algo importante es que 
debemos saber hasta que commit queremos dejar nuestro proyecto o archivo, en 
este caso queremos dejarlo en el commit anterior que es el `2225b3e`.

Pero con `git log` el nombre del commit es muy largo, entonces podemos usar el
comando `git log --oneline` con esto obtenemos la siguiente salida:

    $ git log --oneline
    d910c7b (HEAD -> master) Elimino las variables porque sí
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Con esto tenemos el código del commit y su mensaje.

Usaremos el comando `reset` pero de una manera diferente.

    $ git reset 2225b3e

Se elimina ese commit y obtenedremos lo siguiente:

    $ git reset 2225b3e
    Cambios fuera del área de stage tras el reset:
    M   numbers.py

Podemos verificar esto haciendo un `git log` y después un `git status`

    $ git log --oneline
    2225b3e (HEAD -> master) Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos
    $ git status
    En la rama master
    Cambios no rastreados para el commit:
    (usa "git add <archivo>..." para actualizar lo que será confirmado)
    (usa "git restore <archivo>..." para descartar los cambios en el directorio de trabajo)
	  modificado:     numbers.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")
    
Podemos ver lo que se hizo con `git diff` y regresar al estado anterior del 
archivo con `git checkout`
    
    $ git diff
    diff --git a/numbers.py b/numbers.py
    index c46f79a..3d7bbfc 100644
    --- a/numbers.py
    +++ b/numbers.py
    @@ -1,9 +1,6 @@
    # Archivo numbers.py para ver los operadores básicos de Python.

    -b = (50 - 5*6) / 4
    -print (b)
    -c = 8 / 5
    -print(c)
    +

    # Aqui tenemos una fórmula sencilla
    width=20
    $ git checkout -- numbers.py
    
Hay que tener cuidado con este comando porque es un comando destructivo, si es 
de manera local a lo mejor no importa, pero si es un repositorio compartido, 
cuando sincronicemos nuestros repositorios vamos a tener un problema bastante 
feo.

Pero todo esto es bastante largo, entonces si queremos hacerlo de forma breve
sabiendo lo que estamos haciendo podemos hacerlo de la siguiente manera:

    $ git reset --hard 2225b3e
    HEAD está ahora en 2225b3e Borramos variable que usa memoria.

Si abrimos el archivo otra vez volvió a su estado normal.

Existe otro caso que es el `soft` y cuando lo eliminamos lo que hace es que deja
el cambio en el stage.
Entonces volvamos a borrar las variables y volvamos a commitearlas.

    $ git reset --soft 2225b3e

Si nos damos cuenta no nos devuelve ningún mensaje. Pero si nosotros hacemos un
`status` para ver cómo se encuentra nuestro árbol de trabajo podemos ver lo 
siguiente:

    En la rama master
    Cambios a ser confirmados:
    (usa "git restore --staged <archivo>..." para sacar del área de stage)
	  modificado:     numbers.py

Y listo con esto podemos hacer un `reset` de los archivos modificarlos como 
nosotros queramos.

## ¿Cómo revierto un commit?
Ahora vereos como usar el comando `revert` que es un comando no tan destructivo
como el comando `reset`.
Así que veamos cómo podemos revertir un commit cuando alguien tiene acceso a 
nuestro repositorio. ¿Por qué no usamos `reset`? Recordemos que es un comando 
destructivo, entonces por ese motivo cuando nosotros queramos sincronizar 
nuestros repositorios existirá un error debido a que las tablas de hashes son
distintas.

Así que hagamos un nuevo commit a nuestro código, pero hagamos algo que ya no 
compile para suponer que nos han enviado un código erróneo.

Si hacemos un `git log --oneline` tendremos la siguiente salida:

    3d12717 (HEAD -> master) Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Podemos ver los cambios entre commits con el comando `git diff` y el 
indentificador de cada commit

    $ git diff 2225b3e 3d12717
    diff --git a/numbers.py b/numbers.py
    index c46f79a..a8f64df 100644
    --- a/numbers.py
    +++ b/numbers.py
    @@ -12,3 +12,6 @@ print(width*height)

    # Área de un triangulo
    print((width*height)/2)
    +
    +# Esta parte del código ya nos mandará error
    +print(n)
    
Ahora, el comando `git log --oneline --decorate` lo que hace es mostrarnos los
punteros que tiene `git`, uno de los cuales es HEAD y el otro master o nombre de
la rama.

Con esto tenemos un comando para referirnos a los commits que están antes de 
HEAD el cual es `git diff HEAD~<númerodesdeHEAD>`

La tilde quiere decir el anterior, entonces `git diff HEAD~1` es un commit antes
de HEAD.

Entonces el comando `revert` y la sintaxis que tenemos con `HEAD~<>` podríamos
revertir el cambio de la siguiente manera:

    $ git revert HEAD

Entonces con esto tendremos la siguiente salida:

    $ git log --oneline
    a2a9285 (HEAD -> master) Revert "Agregamos una parte de código erronea"
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Que a primera vista parece como si no hubieramos hecho nada, pero tenemos un 
`revert` en el commit de HEAD.

Ahora veamos que sucede si la persona no hubiera agregado un sólo commit, sino 
hubiera hecho dos.

Regresaremos nuestro árbol de trabajo a la manera inicial con el comando 

    $ git reset --hard HEAD~1

Y ahora modificaremos nuestro archivo para hacer otro commit.

De esta forma nuestro árbol de trabajo se vería de la siguiente manera:
    
    $ git log --oneline
    b238fdd (HEAD -> master) Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Con lo que hemos visto ahorita pues podriamos hacer varios git reverts con los
identificadores de nuestros commits y listo, pero hay una forma más sencilla la
cual nos elimina varios commits de un golpe.

    $ git revert --no-commit HEAD~<numero> 

Esto lo que hace es revertir los cambios pero sin hacer un commit y los deja en
el stage

Después de eso hacemos un `git revert continue` para confirmar todos los commits
que hemos revertido o queremos revertir y dejar en el staged.

    $ git revert --no-commit HEAD
    $ git revert --no-commit HEAD~1
    $ git revert --no-commit HEAD~2
    $ git revert --no-commit HEAD~3
    $ git revert continue

Con estos comandos hemos revertido hasta el tercer commit anterior HEAD para así
no haber hecho ningún cambio del código.
Mejor usemos siempre `revert` porque de esa forma no hacemos cambios 
destructivos a no ser que sepamos que hacemos, en ese caso podemos usar el 
comando `reset`

## Introducción a las ramas (branches)
Una de las principales ventajas de los sistemas de control de versiones es poder
ramificar el trabajo. Pero, ¿qué significa ramificar el trabajo o qué ventajas
tiene hacerlo?

Una de ellas es que podemos trabajar en distintas ramas con los mismos archivos
como si fueran una copia de la rama principal, pero sin afectar la rama 
principal, esto sirve mucho cuando estamos trabajando en equipo o estamos 
haciendo pruebas de nuestro código o proyecto. También sirve para dejar limpia
la rama master y de esa forma trabajar en otras ramas que resuelven diferentes
problemas dentro de nuestro código.

Un comando para poder ver esto es `git log --oneline --graph` el cual nos dará
la siguiente salida:

    git log --oneline --graph
    * 154ead8 (HEAD -> master) Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

De esta forma podemos ver que commit está seguido del otro.

Las bifurcaciones de ramas nos ayuda a trabajar en multitareas, de esta forma
podemos ir trabajando en ramas para interrumpir algún trabajo en ese momento y
solucionar lo que se necesite al instante.

## Cómo crear, modificar y eliminar ramas




Uno de los primeros comandos que tenemos es `git branch`, con ello veremos lo
siguiente:

    $ git branch
    * master

Esto quiere decir que nos encontramos en la rama master y lo que hace el comando
es listar todas las ramas que tenemos.

Cuando queremos agregar una nueva caracteristica (feature) nos vamos a una rama 
nueva para tener a master como código limpio y feature como código sucio.

Ahora imaginemos que queremos crear un nuevo documento en Python, porque ya no
estoy aprendiendo los operadores, ahora estoy aprendiendo como son los _strings_
en ese lenguaje.

    $ git branch feature-strings
    $ git branch
      feature-strings
    * master

Veamos como podemos cambiarnos de rama, para esto podemos usar el comando 
`checkout` para cambiarnos.

    $ git checkout feature-strings
    $ git banch
    * feature-strings
      master

Pero si vamos a hacer esto de manera seguida, estaría mejor tener un atajo para
hacerlo de manera más rápida.

    $ git checkout master
    $ git checkout -b feature-lists
    Cambiando a la nueva rama 'feature-lists'

A veces queremos cambiar el nombre de una rama. Podemos hacer lo siguiente:

    $ git branch master
    $ git branch -m freature-lists feature-listas
    $ git branch
      feature-listas
      feature-strings
    * master

Para deshacernos de una rama es con lo siguiente:
    
    $ git branch -d feature-listas
    $ git branch
      feature-strings
    * master

Podemos hacer muchas cosas con `git branch` para saberlo podemos usar el comando
`-h` para saber qué otras cosas podemos hacer con ese comando. Por lectura 
pondré sólo algunas cosas que lanza la terminal

    $ git branch -h
    Acciones específicas de git-branch:
    -a, --all             listar ramas de remote-tracking y locales
    -d, --delete          borrar ramas totalmente fusionadas
    -D                    borrar rama (incluso si no está fusionada)
    -m, --move            mover/renombrar una rama y su reflog
    -M                    mover/renombrar una rama, incluso si el destino existe
    -c, --copy            copiar una rama y su reflog
    -C                    copiar una rama, incluso si el objetivo 

## Commits bajo el workflow feature branch
Entremos a la última rama que creamos que fue `feature-strings` y pongamos un 
ejemplo sencillo de Python con strings para poder trabajar en ello.

    $ git checkout feature-strings
    $ touch strings.py
    $ nano strings.py
    $ git add strings.py
    $ git commit 
    
De esta manera ya tenemos dos ramas, una que es la rama principal que sólo tiene
el archivo `numbers.py` y otra rama en donde hemos creado `strings.py` que ha 
pesar de que vivan en la misma carpeta no tienen relación directa ya que 
`strings.py` fue creado en la rama `feature-strings`.

Ahora supongamos que creamos otra rama porque vamos a crear una nueva clase llamada
listas, para poder estudiar las listas en Python.

    $ git branch feature-lists
    $ git checkout feature-lists
    $ touch lists.py
    $ nano.py
    $ git add lists.py
    $ git commit

Ahora lo que tenemos es lo siguiente:

    git log --oneline
    1470e53 (HEAD -> feature-lists) Agregamos la clase lists.py
    154ead8 (master) Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Para ver como esta la situación podemos ir a master para ver todos los commits
    
    $ git checkout master
    git log --oneline --all
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 (HEAD -> master) Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Y si usamos el parámetro `--graph` podemos ver la cosa más visual
    
    git log --oneline --all --graph
    * 1470e53 (feature-lists) Agregamos la clase lists.py
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    |/
    * 154ead8 (HEAD -> master) Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

Ahora ya tiene dos sucesores que es el `feature-strings` y `feature-list`.
Pero recordemos que todo lo que sale de master tiene que volver a master.

## ¿Cómo hacer un merge?
Veamos hasta donde hemos dejado nuestro código con la siguiente línea de comando

    $ git log --oneline --decorate --all --graph
    * 1470e53 (feature-lists) Agregamos la clase lists.py
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    |/
    * 154ead8 (HEAD -> master) Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

Ahora queremos que el commit de `feature-lists` vuelva a master, porque por 
alguna razón ya hemos aprendido todo lo que puede hacer Python con listas.

Para esto tenemos que hacer una operación de fusión. Lo que hacemos es fusionar
los cambios que hay en una rama con todas sus modificaciones.
De esa forma cuando fusionemos con nuestra rama master dejaremos todos los 
commits que hay en ella y también los commits que había en este caso en 
`feature-lists`.

Tenemos que colocarnos en la rama de destino, en este caso master

    $ git checkout master

Y hacemos un `merge` de la rama a la que queremos unir

    $ git merge feature-lists
    Actualizando 154ead8..1470e53
    Fast-forward
    lists.py | 4 ++++
    1 file changed, 4 insertions(+)
    create mode 100644 lists.py

De esta manera tenemos la siguiente salida

    $ git log --oneline --decorate --all --graph
    * 1470e53 (HEAD -> master, feature-lists) Agregamos la clase lists.py
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    |/
    * 154ead8 Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos
    
De esta forma lo que antes era master apunta a `feature-lists`, pero podemos
complicar esto un poquito más.

Vamos a fusionar los cambios en `feature-strings` y en master. Para esto 
necesitamos encontrar el ancestro común de `feature-lists` y de master.

En este caso es el commit `154ead8`, no podemos hacer un Fast Forward, porque
tenemos que juntar cambios de dos ramas. La estrategia de fusión que usa Git es 
de manera recursiva. Para que los cambios que se han hecho en una rama se 
aplicaran a la rama de destino.

Pero veremos que si hacemos cambios extraños como dos personas cambien la misma
línea del mismo archivo vamos a tener que arreglar ese conflicto.

En este caso no aplica porque son archivos distintos (programas distintos)

    $ git merge feature-strings
    -- Mensaje del commit --
    $ git log --oneline --decorate --all --graph
    *   89db92b (HEAD -> master) Merge branch 'feature-strings'
    |\
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    * | 1470e53 (feature-lists) Agregamos la clase lists.py
    |/
    * 154ead8 Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

De esta manera lo fusiona bastante bien de manera cómoda y recursiva. Pero
esto se puede llegar a complicar como se dijo anteriormente.


## ¿Cómo resolver un conflicto en el merge?
Ahora vamos a ver cuando los cambios son el mismo archivo para ver cómo Git 
resuelve esto.

En este momento nuestro árbol de trabajo esta así:

    $ git log --oneline --decorate --all --graph
    *   89db92b (HEAD -> master) Merge branch 'feature-strings'
    |\
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    * | 1470e53 (feature-lists) Agregamos la clase lists.py
    |/
    * 154ead8 Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

Así que ahora hagamos un comentario bastante largo en nuestro programa 
`numbers.py`. Cambiamos a una nueva rama llamada: Divisiónes enteras sin punto
decimal.

    $ git checkout -b feature-division-entera
    $ nano numbers.py
    $ git add numbers.py
    $ git commit
    [feature-division-entera 1b7af6c] Comentario bastante largo sobre divisiones enteras
    1 file changed, 5 insertions(+)

Pero ahora imaginemos que tenemos que modificar las variables iniciales, ya que 
nuestro programa usa nuevas variables y las que tenemos ya no funcionan.
De esta manera cambiamos a la rama master para hacer una nueva rama que se llame
`feature-nuevas-variables`

    $ git checkout master
    $ git checkout -b feature-nuevas-variables

Para terminos prácticos sólo cambiaremos la letra siguiente de la variable para 
ver la diferencia.

    $ git add numbers.py
    $ git commit
    [feature-nuevas-variables 6314e83] Cambiamos las nuevas variables del programa
    1 file changed, 4 insertions(+), 4 deletions(-)

Ahora cambiemos a la rama master, porque supongamos que ya hemos terminado y 
queremos integrar los cambios

    $ git checkout master
    $ git merge feature-nuevas-variables
    Actualizando 89db92b..6314e83
    Fast-forward
    numbers.py | 8 ++++----
    1 file changed, 4 insertions(+), 4 deletions(-)

Git lo resuelve por Fast-forward porque master no se ha tocado. Veamos en que 
situación está nuestro árbol.

    * 6314e83 (HEAD -> master, feature-nuevas-variables) Cambiamos las nuevas variables del programa
    | * 1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    |/
    *   89db92b Merge branch 'feature-strings'
    |\
    | * 89fcebb (feature-strings) Ahora estudiamos strings
    * | 1470e53 (feature-lists) Agregamos la clase lists.py
    |/
    * 154ead8 Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

Ahora vamos a integrar `feature-division-entera` y lo debe resolver por 
recursión porque se ha modificado y el ancestro es común, pero hemos modificado
en ambas ocasiones el archivo `numbers.py`.

Ahora si hacemos lo siguiente

    $ git merge feature-division-entera
    -- Mensaje de commit --
    Auto-fusionando numbers.py
    ayuda: Esperando que tu editor cierre el archivo ... Waiting for Emacs...
    Merge made by the 'recursive' strategy.
    numbers.py | 5 +++++
    1 file changed, 5 insertions(+)

Vamos a llevarlo más allá, hagamos una modificación en la misma línea. En este 
caso vamos a modificar la ecuación de ancho y altura porque nos equivocamos y 
debe ser otra.

    $ git checkout -b fix-width-height
    $ git add numbers.py
    $ git commit
    [fix-width-height 8952ff1] Arreglamos ancho y alto
    1 file changed, 2 insertions(+), 2 deletions(-)
    $ git checkout master

Ahora sucede que queremos sacar en nuestro archivo el ancho y alto multiplicado
por 3.

    $ git checkout -b fix-formula
    $ nano numbers.py
    $ git add numbers.py
    $ git commit 
    [fix-formula 6f4b793] Modificamos la formula del ancho y alto
    1 file changed, 1 insertion(+), 1 deletion(-)
    
Ahora vamos a intentar integrar los cambios de las dos ramas de `fix-width-height` y de
`fix-formula`

    $ git merge fix-width-height
    Actualizando adbd48b..8952ff1
    Fast-forward
    numbers.py | 4 ++--
    1 file changed, 2 insertions(+), 2 deletions(-)
    $ git merge fix-formula
    Auto-fusionando numbers.py
    CONFLICTO (contenido): Conflicto de fusión en numbers.py
    Fusión automática falló; arregle los conflictos y luego realice un commit con el resultado.
    
Ahora nos dice que hay un conflicto porque hay cambios que se han realizado en 
la misma región.

Si hacemos un `diff` entre el commit antes de master y las dos ramas siempre es 
la misma línea.

Si nosotros hacemos un `git status` obtenemos la siguiente salida

    En la rama master
    Tienes rutas no fusionadas.
    (arregla los conflictos y corre "git commit"
    (usa "git merge --abort" para abortar la fusion)

    Rutas no fusionadas:
    (usa "git add <archivo>..." para marcar una resolución)
    ambos modificados:     numbers.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")
    
Si abrimos nuestro archivo obtendremos algunos símbolos de este estilo:

    # Aqui tenemos una fórmula sencilla
    <<<<<<< HEAD
    width=30
    height=10*7
    print(width*height)
    =======
    width=20
    height=5*9
    print(3*(width*height))
    >>>>>>> fix-formula

En este caso dependerá coon que nos queremos quedar, digamos que después de 
tanto buscar nos dimos cuenta que sí tenemos que multiplicar la formula por 3,
pero que el ancho y el alto deben ser de 30 y 10*7 respectivamente.
Entonces escribiremos eso

    # Aquí tenemos una fórmula sencilla
    width=30
    height=10*7
    print(3*(width*height))

Y con eso queda resuelto nuestro conflicto. Lo que nos queda es escribir lo 
siguiente

    $ git add numbers.py
    $ git commit
    ayuda: Esperando que tu editor cierre el archivo ... Waiting for Emacs...
    [master 583539a] Merge branch 'fix-formula'
    
Los conflictos pueden llegar a ser más complicados, pero de esta forma podemos
resolverlos de una manera sencilla.

## Cómo construir un alias?
Todo este tiempo hemos estado trabajando con comandos bastante largos como por
ejemplo

    $ git log --oneline --decorate --graph --all

¿No habrá una manera más sencilla de poder resolver esto?

Sí.

Para esto existen los alias, que son atajos para un comando total de git. Nos
permite hacer cosas de manera mucho más rápido 

Por ejemplo podemos hacer un comando llamado `git lod` que sea la versión corta 
de `git log  --oneline --decorate`

De esta manera tendremos este que usar el comando `git config.`. Y cómo funciona
para construir alias.

    $ git config --global alias.lodag 'log --oneline --decorate --all --graph'

De esta manera podemos crear de muchisimas formas diferentes alias según nuestra
conveniencia en git y no necesariamente modificarlo desde algún otro lugar como
por ejemplo la `.bashrc` en el caso de Linux.

Para poder ver todos los alias que hemos creado tenemos el siguiente comando

    $ git config --global --get-regexp alias

De este modo se nos despliegan los alias que tengamos.

Para eliminar un alias podemos hacer lo siguiente:

    $ git config --global --unset alias.lodag

Podemos eliminar, enlistarlos o crearlos. Todo de una forma sencilla y de 
acuerdo a lo que nosotros queramos.

Para terminos prácticos de los siguientes temas configuraremos el siguiente 
comando

    $ git config --global alias.lod 'log --oneline --decorate'

## ¿Cómo resolver conflictos en Git?
Vamos a ver cómo podemos resolver ciertos conflictos especiales en Git.

Vamos a trabajar ahora en una nueva carpeta llamada `Conflictos` y en ella vamos
a seguir con la norma de crear un archivo Python para poder modificar ciertas 
líneas o incluso algunas que esten en la misma posición.

Así que haremos lo siguiente:

    -- Nos ubicamos en la carpeta Conflictos --
    $ git init
    $ touch main.py
    $ nano main.py
    $ git add main.py
    $ git commit
    -- Mensaje del commit --
    $ git checkout -b rama-A
    $ nano main.py

En la rama-A hacemos un `print` es la segunda linea y lo commiteamos.

    $ git add main.py
    $ git commit
    -- Mensaje del commit --

Creo una nueva rama llamada `rama-B` y hago en la segunda línea del archivo 
hacemos un `print`
    
    $ git checkout -b rama-B
    $ nano main.py
    $ git add main.py
    $ git commit
    -- Mensaje del commit --
    $ git checkout master

Veamos entonces en nuestra rama master como se encuentra nuestro árbol de 
trabajo:

    $ git lod --all --graph
    * 29982e3 (rama-B) Modificación en la rama B
    | * d606372 (rama-A) Modificación A
    |/
    * abaab67 (HEAD -> master) Iniciando repositorio
    
Vamos entonces a hacer un merge de la `rama-A` a `master` y después intentaremos
hacer un merge con la `rama-B`

    $ git merge rama-A
    Actualizando abaab67..d606372
    Fast-forward
    main.py | 1 +
    1 file changed, 1 insertion(+)

    $ git merge rama-B
    Auto-fusionando main.py
    CONFLICTO (contenido): Conflicto de fusión en main.py
    Fusión automática falló; arregle los conflictos y luego realice un commit con el resultado.
    
Cuando ocurre un conflicto es que nos avisa Git, nuestro objetivo será 
resolverlo.

Esto ya lo habíamos visto antes cuando hacemos `git status`.

    En la rama master
    Tienes rutas no fusionadas.
    (arregla los conflictos y corre "git commit"
    (usa "git merge --abort" para abortar la fusion)

    Rutas no fusionadas:
    (usa "git add <archivo>..." para marcar una resolución)
    ambos modificados:     main.py

    sin cambios agregados al commit (usa "git add" y/o "git commit -a")

Y se dice que se ha modificado en ambas ramas en el archivo `main.py`.

Por lo cuál nuestro programa se vería de este estilo en nuestro editor:

    #!/usr/bin/python

    print ("Hola mundo")
    <<<<<<< HEAD
    print ("Est es una modificación hecha en la rama A")
    =======
    print ("Esta modificación la he hecho en la rama B")
    >>>>>>> rama-B

¿Ahora qué hacemos?

Lo más sensato es pensar qué es lo mejor para el proyecto. Queremos fusionar dos
ramas que tienen modificaciones en las mismas líneas de código. 

Primera, esa es una situación rara, ya que usualmente cada programador trabaja 
en una sola cosa. Ya que nadie debería de editar la misma función que tú estás
editando.

Pero usualmente pasan muchos conflictos por ejemplo con algunos `imports` que se
hacen en algunos lenguajes de programación como Java o Python.

Si existe un conflicto tendríamos que hablar con nuestro equipo para poder estar
en la misma sintonía con el proyecto dentro del ordenador.

Entonces deberemos quedarnos con la zona o región que nos interese. El conflicto
inicia desde los simbolos `<<<<<<< HEAD` y termina en `>>>>>>> rama-B`

Podemos quedarnos con la versión de la rama-A o la versión de la rama-B. O 
podríamos hacer en una sóla línea la información que se toma en ambas ramas.

Este ejemplo es bastante sencillo.

## ¿Cómo usar los tags?

Volvamos de nuevo a nuestra carpeta llamada Python donde hemos trabajado antes
del pequeño break de `Conflictos`.

¿Para qué sirven los tags?

Sirven para identificar commits particulares, puede ser un commit especial como 
la versión 1.0 o de un despliegue, etc. Puede ser también un commit que podamos
marcar si tiene algún problema y queramos resolver después.

Así que hasta el momento nuestro árbol de trabajo se ve de la siguiente manera:

    583539a (HEAD -> master) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto
    8952ff1 (fix-width-height) Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 (feature-nuevas-variables) Cambiamos las nuevas variables del programa
    1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Quisieramos hacer una marca especial a por ejemplo al commit `fix-formula` ya 
que es una fórmula muy importante para nuestro proyecto.

Para esto existe el comando `tag`

    $ git tag v0.2.0

De esta forma ya nos ha creado un `tag`, entonces tendríamos lo siguiente:

    583539a (HEAD -> master, tag: v0.2.0) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto
    8952ff1 (fix-width-height) Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 (feature-nuevas-variables) Cambiamos las nuevas variables del programa
    1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

De esta manera se nos ha creado un tag en ese commit, pero si queremos crearlo 
en un hash en particular lo podríamos hacer de la siguiente manera

    $ git tag v0.1.0 154ead8
    $ git lod
    583539a (HEAD -> master, tag: v0.2.0) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto
    8952ff1 (fix-width-height) Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 (feature-nuevas-variables) Cambiamos las nuevas variables del programa
    1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 (tag: v0.1.0) Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

De hecho hasta podemos hacer un `checkout` a ese tipo de `tag`
    
    $ git checkout v0.1.0
    Nota: cambiando a 'v0.1.0'.

    Te encuentras en estado 'detached HEAD'. Puedes revisar por aquí, hacer
    cambios experimentales y hacer commits, y puedes descartar cualquier
    commit que hayas hecho en este estado sin impactar a tu rama realizando
    otro checkout.

    Si quieres crear una nueva rama para mantener los commits que has creado,
    puedes hacerlo (ahora o después) usando -c con el comando checkout. Ejemplo:

    git switch -c <nombre-de-nueva-rama>

    O deshacer la operación con:

    git switch -

    Desactiva este aviso poniendo la variable de config advice.detachedHead en false

    HEAD está ahora en 154ead8 Revert "Agregamos una parte de código erronea"

Y si le hacemos un vistazo con `status` tendríamos lo siguiente:
    
    $ git status
    HEAD desacoplada en v0.1.0
    nada para hacer commit, el árbol de trabajo está limpio

Y si hacemos un `git lod` podríamos ver cómo se encuentra nuestro árbol de 
trabajo

    $ git lod
    154ead8 (HEAD, tag: v0.1.0) Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Y podemos bifucarnos a otra rama por ejemplo:
    
    $ git checkout -b fix-0.1.1
    Cambiado a nueva rama 'fix-0.1.1'

Y estariamos en otra rama distinta.

    $ git checkout master
    $ git lod
    583539a (HEAD -> master, tag: v0.2.0) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto
    8952ff1 (fix-width-height) Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 (feature-nuevas-variables) Cambiamos las nuevas variables del programa
    1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 (tag: v0.1.0, fix-0.1.1) Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos
    
Tenemos una nueva bifurcación en el commit `154ead8`.

Si queremos listar los `tags` que hemos creado se hace con el siguiente comando:

    $ git tag
    v0.1.0
    v0.2.0

También podemos eliminar tags. Por ejemplo imaginemos que creamos un tag en otro
lado donde no debería ir

    $ git tag v0.1.1 a91e67d

Para esto tenemos un montón de comandos con los cuáles podemos verlos con 
`git tag -h`

Y el que nos interesa es la bandera `-d`

Con eso podemos eliminar el tag que tenemos

    $ git lod
    583539a (HEAD -> master, tag: v0.2.0) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto
    8952ff1 (fix-width-height) Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 (feature-nuevas-variables) Cambiamos las nuevas variables del programa
    1b7af6c (feature-division-entera) Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 (feature-lists) Agregamos la clase lists.py
    89fcebb (feature-strings) Ahora estudiamos strings
    154ead8 (tag: v0.1.0, fix-0.1.1) Revert "Agregamos una parte de código erronea"
    a91e67d (tag: v0.1.1) Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos
    $ git tag -d v0.1.1
    Etiqueta 'v0.1.1' eliminada (era a91e67d)
    
Y con ese último comando se nos elimina el tag.

## ¿Qué son los tags anotados?
Existen dos tipos de `tags` los ligeros y los anotados, los que vimos 
anteriormente son los ligeros.

En Git tenemos algo llamado las etiquetas anotadas, en las que no sólo tenemos
un puntero, sino tenemos más información. Usualmente esto se hace en tags un 
poco _más serios_, por medio de eso podemos decir que hay en la nueva versión,
que queda obsoleto, que cambios corrige, que bugs arregla, etc.

Y además de eso podemos firmar un tag, porque cualquiera puede crearlo, pero 
así le damos vericidad.

También podemos listar los tags de la siguiente manera para poder ver las 
versiones que quisieramos.

    $ git tag -l "v0.1.*"

De esta forma podemos listar todos los tags que empiecen por `v0.1...`. De esta
manera podemos filtrar tags sin tener que poder verlos todos.

Para esto eliminaremos nuestros tags actuales.

    $ git tag -d v0.1.0
    Etiqueta 'v0.1.0' eliminada (era 154ead8)
    $ git tag -d v0.2.0
    Etiqueta 'v0.2.0' eliminada (era 583539a)

Para anotar un tag será con la bandera `-a`

    $ git tag -a v0.1.0

De esto nos desplegará nuestro editor y escribiremos un mensaje del tag.

De esta manera después de escribir el mensaje de nuestro tag podemos visualizar 
cómo queda con el siguiente comando:

    $ git show v0.1.0
    tag v0.1.0
    Tagger: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Fri May 21 21:18:19 2021 -0500

    Primera versión de nuestro programa en Python

    Cambios que introduce esta versión son tal, tal, tal......
    Los bugs que arreglamos en esta versión son .........

    commit 583539a15f8a2bbe950672459117ecdacd941773 (HEAD -> master, tag: v0.1.0)
    Merge: 8952ff1 6f4b793
    Author: JulsVazquez <juliovaal@ciencias.unam.mx>
    Date:   Fri May 21 18:11:27 2021 -0500

    Merge branch 'fix-formula'

    diff --cc numbers.py
    index 49cff74,adf1c0f..857944d
    --- a/numbers.py
    +++ b/numbers.py
    @@@ -6,9 -6,9 +6,9 @@@ f = 8 /
    print(f)

    # Aqui tenemos una fórmula sencilla
    -width=20
    -height=5*9
    +width=30
    +height=10*7
        - print(width*height)
    + print(3*(width*height))

    # Área de un triangulo
    print((width*height)/2)

Podemos ver el autor, cuándo lo hicimos, la etiqueta, el commit e incluso el 
diff.

Y el comando `show` podemos hacerlo para revisar otros commits y saber la 
información.

Si queremos algo casual son los tags ligeros, si queremos algo formal será con
un tag anotado.

## Stash: ¿Cómo esconder cambios en Git?
Imaginemos que hemos terminado parte de nuestra aplicación o lo que sea que 
estemos trabajando en nuestro proyecto, y nos disponemos a hacer un `README.md`.
Entonces estamos trabajando en nuestro README, pero por alguna razón tenemos que
cambiarnos a la rama de trabajo porque hay algo que nos avisó un amigo al que le
pasamos la aplicación beta y la compromete.

Pero nosotros ya habiamos añadido el README al stage, entonces no podemos 
cambiarnos de rama tan fácil. ¿Cómo podemos guardar estos cambios y no se 
pierdan?

Para esto existe el `stash`.

    $ git stash
    Directorio de trabajo guardado y estado de índice WIP on feat-readme: 583539a Merge branch 'fix-formula'
    
Si nosotros revisamos con `status` podemos ver lo siguiente:

    $ git status
    En la rama feat-readme
    nada para hacer commit, el árbol de trabajo está limpio
    
Pero en realidad esto está guardado en el stash y para verlo usamos el siguiente comando

    $ git stash list
    stash@{0}: WIP on feat-readme: 583539a Merge branch 'fix-formula'
    $ git checkout master
    
Y ver lo que se ha guardado que es el nombre de la rama y su commit.

Supongamos que nos cambiamos a master y paso el tiempo y queremos ahora regresar
lo que teníamos en el stash.

    $ git checkout feat-readme
    $ git stash apply
    En la rama feat-readme
    Cambios a ser confirmados:
    (usa "git restore --staged <archivo>..." para sacar del área de stage)
    nuevo archivo:  README.md

De esta forma aunque este fuera del stage, aunque si nosotros revisamos el 
`stash list` nos daremos cuenta que aún está ahí

    $ git stash list
    stash@{0}: WIP on feat-readme: 583539a Merge branch 'fix-formula'

Para eliminarlo usamos el comando

    $ git stash drop
    Botado refs/stash@{0} (a465fb95a14f65641fbc3fb81e32f1919b025b0a)

De esta forma podemos hacer un `stash` con un mensaje descriptivo con el 
siguiente comando

    $ git stash save "He modificado el archivo readme"
    Directorio de trabajo guardado y estado de índice On feat-readme: He modificado el archivo readme

Ahora vamos a modificar otro archivo por ejemplo `strings.py`

    $ nano string.py
    $ git stash save "He modifcado el archivo strings.py"
    Directorio de trabajo guardado y estado de índice On feat-readme: He modificado el archivo strings.py

Al hacer un `git stash list` veremos esta salida:

    $ git stash list
    stash@{0}: On feat-readme: He modificado el archivo strings.py
    stash@{1}: On feat-readme: He modificado el archivo readme

Podemos obtener más información con `git stash show <nombreDelStash>`

    $ git stash show stash@\{0\}
    strings.py | 2 ++
    1 file changed, 2 insertions(+)
    $ git stash apply
    
Y ahora modificamos el archivo `strings.py`. Pero no nos sirve, entonces haremos
un `drop`

    $ git stash drop

Hacemos un checkout para eliminar las modificaciones en `strings.py`

    $ git checkout -- strings.py
    
Ahora veamos como podemos aplicar de golpe el apply y el drop con el comando 
`pop`

    $ git stash pop

El problema de los stash es ponerle un mensaje y puede tenderse a olvidar qué
tenemos en el stash.

## Introducción a remotos
A veces nosotros queremos enviar nuestras modificaciones por internet o por un
servicio de hosting. La ventaja que tenemos con git es que un sistema de control
de versiones distribuido, porque no necesitamos estar conectados siempre a 
internet para poder trabajar.

Con el comando `git remote` podremos hacer algunas cosas con sus respectivas 
banderas para poder enviar o recibir cosas.

Un remoto podría ser que estas en una oficina y el archivo de tu compañero es el
remoto, entonces podemos hacer un `push` para enviarle cosas o un `pull` para 
jalar cosas del archivo o el remoto en el que esten trabajando.

## Cómo hacer un push
Vamos ahora a apoyarnos de una herramienta online para poder crear remotos.

Los remotos podrían también estando en máquina de otra persona, esto sirve para
poder pushear código en un servidor en nuestra empresa o incluso nuestra casa.

En este caso ocuparemos GitLab. Un sitio web en los que podemos crear 
repositorios, pushear, jalar cambios e incluso compartir repositorios. Así que 
vamos a crear un nuevo repositorio en GitLab.

A nuestro repositorio lo llamaremos para términos prácticos `GitPython`, vamos a
conectarlo con nuestro repositorio local que hemos estado creando.

Con esto vamos a conectarlo con el comando `git remote add`

    $ git remote add origin <direcciónDelRepositorio>
    $ git remote add origin git remote add origin git@gitlab.com:JulsVazquez/gitpython.git

Si nosotros escribimos el comando `remote` para verificar nuestro repositorio 
tendremos la siguiente salida

    $ git remote
    origin

Podemos obtener más información con la bandera `-v`

    $ git remote -v
    origin	git@gitlab.com:JulsVazquez/gitpython.git (fetch)
    origin	git@gitlab.com:JulsVazquez/gitpython.git (push)
    
Vamos a ver cómo hacer un `push` para enviarlo a la nube. Para esto necesitamos
el siguiente comando

    $ git push origin <ramaASubir>
    $ git push origin master

En este caso nos podría pedir nuestro usuario y contraseña debido a que es un
repositorio privado, pero como yo estoy usando el protocolo SSH para encriptar
lo que envío no me lo pide

    $ git push origin master
    client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0
    Enumerando objetos: 44, listo.
    Contando objetos: 100% (44/44), listo.
    Compresión delta usando hasta 8 hilos
    Comprimiendo objetos: 100% (39/39), listo.
    Escribiendo objetos: 100% (44/44), 5.00 KiB | 1.67 MiB/s, listo.
    Total 44 (delta 15), reusado 0 (delta 0), pack-reusado 0
    To gitlab.com:JulsVazquez/gitpython.git
    * [new branch]      master -> master
 
## Cómo clonar y hacer un pull
Vamos ahora a ver cómo poder clonar un repositorio remoto.
Para esto pondre el repositorio GitPython cómo un repositorio público.

Para eso tenemos un comando que puede ser ejecutado en cualquier carpeta, no 
necesariamente tiene que ser una carpeta que tenga un subdirectorio `.git` el 
cual es `clone`

    $ git clone <ubicaciónDelRepositorio>
    $ git clone https://gitlab.com/JulsVazquez/gitpython.git

Salvo que nosotros lo cambiemos, Git lo que hará es que descargará todo el 
repositorio.

    $ cd gitpython/
    $ git lod
    583539a (HEAD -> master, origin/master, origin/HEAD) Merge branch 'fix-formula'
    6f4b793 Modificamos la formula del ancho y alto
    8952ff1 Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 Cambiamos las nuevas variables del programa
    1b7af6c Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 Agregamos la clase lists.py
    89fcebb Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Con esto podemos ver que hemos copiado todo lo del repositorio, incluyendo los 
commits que se han hecho desde el momento que se creo el repositorio.

De esta forma nosotros ya podemos trabajar en el repositorio ya que se nos
asocia el remoto que se configuró al momento de crear el repositorio
    
    $ git remote
    origin

Imaginemos ahora que este repositorio que clonamos es un repositorio de código
abierto que nosotros vamos a actualizar nuestra copia local.

De esta forma me moveré al directorio donde he creado el repositorio original y 
haré una modificación.

    $ cd ~/Documentos/Pruebas/Git/Python
    $ touch Modificacion.txt
    $ git add .
    $ git commit
    -- Mensaje del commit --
    git push origin master
    client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0
    Enumerando objetos: 4, listo.
    Contando objetos: 100% (4/4), listo.
    Compresión delta usando hasta 8 hilos
    Comprimiendo objetos: 100% (2/2), listo.
    Escribiendo objetos: 100% (3/3), 362 bytes | 362.00 KiB/s, listo.
    Total 3 (delta 0), reusado 0 (delta 0), pack-reusado 0
    To gitlab.com:JulsVazquez/gitpython.git
    583539a..fdc58ec  master -> master

Ahora nosotros podríamos ir al repositorio que clonamos y jalar esos cambios
    
    $ cd
    $ cd gitpython/
    $ ls
    drwxr-xr-x   - multivac 25 may 16:24 .git
    .rw-r--r-- 110 multivac 25 may 16:24 lists.py
    .rw-r--r-- 490 multivac 25 may 16:24 numbers.py
    .rw-r--r-- 232 multivac 25 may 16:24 strings.py

Hasta este momento podemos ver que no existe el archivo `Modificacion.txt`,
vamos a actualizar el repositorio para jalar ese cambio que se hizo.

De esta manera le diremos a Git si ha habido cambios en el repositorio que hemos
estado trabajando 
    
    $ git pull origin master
    remote: Enumerating objects: 4, done.
    remote: Counting objects: 100% (4/4), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
    Desempaquetando objetos: 100% (3/3), 342 bytes | 342.00 KiB/s, listo.
    Desde https://gitlab.com/JulsVazquez/gitpython
    * branch            master     -> FETCH_HEAD
    583539a..fdc58ec  master     -> origin/master
        Actualizando 583539a..fdc58ec
    Fast-forward
    Modificacion.txt | 0
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 Modificacion.txt

Si nosotros examinamos el repositorio veremos que ya tenemos el archivo y
también el commit que se hizo

    $ git lod
    fdc58ec (HEAD -> master, origin/master, origin/HEAD) Modificación
    583539a Merge branch 'fix-formula'
    6f4b793 Modificamos la formula del ancho y alto
    8952ff1 Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 Cambiamos las nuevas variables del programa
    1b7af6c Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 Agregamos la clase lists.py
    89fcebb Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos
    $ ls
    drwxr-xr-x   - multivac 25 may 16:34 .git
    .rw-r--r-- 110 multivac 25 may 16:24 lists.py
    .rw-r--r--   0 multivac 25 may 16:34 Modificacion.txt
    .rw-r--r-- 490 multivac 25 may 16:24 numbers.py
    .rw-r--r-- 232 multivac 25 may 16:24 strings.p

Vemos que sucede con el comando `branch`

    $ git branch
    * master
    $ git branch --all
    * master
      remotes/origin/HEAD -> origin/master
      remotes/origin/master

De esta forma se han creado las ramas de los remotos. 

## Fetch y pull rebase
Vamos a revertir el último cambio que hicimos en nuestro repositorio original de
versiones.

    $ git revert HEAD
    git lod
    5141b84 (HEAD -> master) Revert "Modificación"
    fdc58ec (origin/master) Modificación
    583539a (tag: v0.1.0) Merge branch 'fix-formula'
    6f4b793 (fix-formula) Modificamos la formula del ancho y alto

Y vamos empujarlo a `origin master`
    
    $ git push origin master

Ahora movamonos a nuestra carpeta dónde clonamos el repositorio. Ya que para 
descargar el commit podríamos hacer `git pull origin master`, pero `pull` es la
combinación de dos comandos.

Uno de ellos es `fetch` el cual sirve para preguntarle a un remoto si tiene 
novedades y descargarlos.

    $ git fetch origin

Si tiene modificaciones descarga la rama.

    $ git fetch origin 
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 2 (delta 1), reused 0 (delta 0), pack-reused 0
    Desempaquetando objetos: 100% (2/2), 242 bytes | 242.00 KiB/s, listo.
    Desde https://gitlab.com/JulsVazquez/gitpython
    fdc58ec..5141b84  master     -> origin/master

De esta forma recordemos que nuestras ramas que se han creado cuando hicimos 
los remotos.

    $ git branch --all
    * master
      remotes/origin/HEAD -> origin/master
      remotes/origin/master

Y nosotros podemos hacer un `checkout` a la rama `origin/master` para revisarla

    $ git checkout origin/master
    Nota: cambiando a 'origin/master'.

    Te encuentras en estado 'detached HEAD'. Puedes revisar por aquí, hacer
    cambios experimentales y hacer commits, y puedes descartar cualquier
    commit que hayas hecho en este estado sin impactar a tu rama realizando
    otro checkout.

    Si quieres crear una nueva rama para mantener los commits que has creado,
    puedes hacerlo (ahora o después) usando -c con el comando checkout. Ejemplo:

    git switch -c <nombre-de-nueva-rama>

    O deshacer la operación con:

    git switch -

    Desactiva este aviso poniendo la variable de config advice.detachedHead en false

    HEAD está ahora en 5141b84 Revert "Modificación"
    
Y lo que hace `pull` es que fusiona la rama con el comando que le hemos dicho 
con la rama master.

    $ git checkout master

Entonces lo único que queda hacer es un `merge` con la rama remota y `master`

    $ git merge origin/master
    Actualizando fdc58ec..5141b84
    Fast-forward
    Modificacion.txt | 0
    1 file changed, 0 insertions(+), 0 deletions(-)
    delete mode 100644 Modificacion.txt

Esto es lo mismo que obtendríamos si usamos el comando `git pull origin master`

Pero todo esto puede dar situaciones algo curiosa.

Vamos a ver cómo suce este tipo de situaciones.

Hagamos una modificación en nuestro repositorio que clonamos

    $ touch archivoPrueba.js
    $ git add archivoPrueba.js
    $ git commit
    -- Mensaje del commit --

Y de forma paralela en la rama de versiones (el repositorio original) hago un 
revert de HEAD para agregar la modificación.

Tengamos en cuenta que hacer un `revert` de un `revert` es una cosa bastante 
rara.

    $ git revert HEAD
    ayuda: Esperando que tu editor cierre el archivo ... Waiting for Emacs...
    [master e2e28e5] Revert "Revert "Modificación""
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 Modificacion.txt

Haremos un push al `revert`
    
    $ git push origin master 
    client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0
    Enumerando objetos: 4, listo.
    Contando objetos: 100% (4/4), listo.
    Compresión delta usando hasta 8 hilos
    Comprimiendo objetos: 100% (2/2), listo.
    Escribiendo objetos: 100% (3/3), 391 bytes | 391.00 KiB/s, listo.
    Total 3 (delta 0), reusado 0 (delta 0), pack-reusado 0
    To gitlab.com:JulsVazquez/gitpython.git
    5141b84..e2e28e5  master -> master

Y en nuestro repositorio clonado haremos un `pull`

    $ git pull origin master 

Pero lo que sucede aquí es que nos habré una nueva ventana avisandonos que se 
tiene que hacer un `merge` de la rama `master` que hay en un remoto. Entonces 
debemos hacer un commit para resolver esta estrategia recursiva entre la rama
local y la remota.

La única desventaja en todo esto es que se nos creará un commit temporal, esto
puede molestar bajo distintos puntos de vista.

Imaginemos que sí nos molesta y no queremos eso.

Para evitar estas cosas nosotros debemos de hacer un `Fetch` antes de empezar a
trabajar o hacer un commit.

Una segunda forma es utilizar un parámetro. Para esto en nuestro repositorio 
clonado hagamos lo siguiente.

    $ rm archivoPrueba.js
    $ git rm archivoPrueba.js
    rm 'archivoPrueba.js'
    $ git commit -m "Elimina archivo prueba"
    [master 1c57504] Elimina archivo prueba

Nos movemos a nuestra carpeta de versiones y hacemos un `revert` a HEAD, para 
que haga la reversión de la reversión de la reversión del commit Modificación.

    $ git revert HEAD
    Eliminando Modificacion.txt
    ayuda: Esperando que tu editor cierre el archivo ... Waiting for Emacs...
    [master 117600d] Revert "Revert "Revert "Modificación"""
    1 file changed, 0 insertions(+), 0 deletions(-)
    delete mode 100644 Modificacion.txt

Hacemos un `push` porque es el más reciente.

    $ git push origin master
    client_global_hostkeys_private_confirm: server gave bad signature for RSA key 0
    Enumerando objetos: 3, listo.
    Contando objetos: 100% (3/3), listo.
    Compresión delta usando hasta 8 hilos
    Comprimiendo objetos: 100% (2/2), listo.
    Escribiendo objetos: 100% (2/2), 268 bytes | 268.00 KiB/s, listo.
    Total 2 (delta 1), reusado 0 (delta 0), pack-reusado 0
    To gitlab.com:JulsVazquez/gitpython.git
    e2e28e5..117600d  master -> master
    
Y en el repositorio clonado haremos un `pull` perovantes de `origin` escribimos
el comando `--rebase`, lo que hace es que cuando intenta fusionar dos ramas que
tienen commits en distinto tiempo, entonces lo que hace es barajear los commits
y los ordena sin romper el historial

    $ git pull --rebase origin master
    remote: Enumerating objects: 3, done.
    remote: Counting objects: 100% (3/3), done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 2 (delta 1), reused 0 (delta 0), pack-reused 0
    Desempaquetando objetos: 100% (2/2), 248 bytes | 248.00 KiB/s, listo.
    Desde https://gitlab.com/JulsVazquez/gitpython
    * branch            master     -> FETCH_HEAD
    e2e28e5..117600d  master     -> origin/master
    Rebase aplicado satisfactoriamente y actualizado refs/heads/master.

    
Pongamonos en situación. 

Imaginemos que iniciamos el día empezamos a trabajar en nuestro repositorio que
hemos clonado y trabajaremos en un nueva caracteristica.

Cómo se suponque hemos creado un repositorio para estudiar un lenguaje de 
programación, cada cambio que hagamos será un tema terminado.

    $ cd gitpython/
    $ git checkout -b feature-temas
    $ touch TemaA.py
    $ git add . && git commit -m "Estudiamos Tema A"

Ahora imaginemos que encontramos otro tema, digamos un Tema B más interesante
y trabajaremos ahora en él.

    $ touch TemaB.py
    $ git add . && git commit -m "Estudiamos Tema B"

Pero supongamos ahora que nuestro profesor ve un tema nuevo que debemos poner en
nuestra rama master, ya que nosotros andamos adelantando nuestros estudios.

Así que nos cambiamos de rama

    $ git checkout master
    $ touch TemaC.py
    $ git add . && git commit -m "Estudiamos Tema C"

Y finalmente regresamos a estudiar nuestros temas adelantados y logramos 
terminar el capítulo de ese día y lo juntamos en un Tema D

    $ git checkout feature-temas
    $ touch TemaD.py
    $ git add . && git commit -m "Estudiamos Tema D"

Hemos hecho modificaciones tanto en una rama derivada y otras en la rama master.

Con lo que sabemos es que se van a juntar las dos ramas en un commit que no 
sirve para nada.

Una advertencia antes de lo que haremos es que `--rebase` reescribe el historial
y hay que tener cuidado con esto porque modificamos el hash de los commits. Y 
podemos destrozar código o incluso destruir trabajos de compañeros. Hagamos esto
con cosas que no han sido `pusheadas`

De esta forma veamos como esta nuestro árbol de trabajo.

    $ git lod --all --graph --since=2021-05-24
    * bc4802e (HEAD -> feature-temas) Estudiamos TemaD
    * 8d8732d Estudiamos Tema B
    * 371f3ac Estudiamos Tema A
    | * a8218ba (master) TemaC.py
    |/
    * 634087e (origin/master, origin/HEAD) Elimina archivo prueba
    * 80a19de Agrega un archivo de prueba
    * 117600d Revert "Revert "Revert "Modificación"""
    * e2e28e5 Revert "Revert "Modificación""
    * 5141b84 Revert "Modificación"
    * fdc58ec Modificación
    
Lo que haremos es lo siguiente:

    $ git rebase master
    Rebase aplicado satisfactoriamente y actualizado refs/heads/feature-temas.

Ahora comparemos el `lod`

    $ git lod --all --graph --since=2021-05-24
    * 0c9daed (HEAD -> feature-temas) Estudiamos TemaD
    * 674279c Estudiamos Tema B
    * 87d17c4 Estudiamos Tema A
    * a8218ba (master) TemaC.py
    * 634087e (origin/master, origin/HEAD) Elimina archivo prueba
    * 80a19de Agrega un archivo de prueba
    * 117600d Revert "Revert "Revert "Modificación"""
    * e2e28e5 Revert "Revert "Modificación""
    * 5141b84 Revert "Modificación"
    * fdc58ec Modificación 

Todos los cambios se han aplanado, pero esta es la parte destructiva, los hashes
ya han cambiado.

Ahora si nosotros nos vamos a `master`
    
    $ git checkout master

Y hago un `merge` a `feature-temas` lo resuelve por `fast-forward`

    $ git merge feature-temas
    Actualizando a8218ba..0c9daed
    Fast-forward
    TemaA.py | 0
    TemaB.py | 0
    TemaD.py | 0
    3 files changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 TemaA.py
    create mode 100644 TemaB.py
    create mode 100644 TemaD.py

Y la consecuencia de esto es que el historial es más limpio

    $ git lod --all --graph
    * 0c9daed (HEAD -> master, feature-temas) Estudiamos TemaD
    * 674279c Estudiamos Tema B
    * 87d17c4 Estudiamos Tema A
    * a8218ba TemaC.py
    * 634087e (origin/master, origin/HEAD) Elimina archivo prueba
    * 80a19de Agrega un archivo de prueba
    * 117600d Revert "Revert "Revert "Modificación"""
    * e2e28e5 Revert "Revert "Modificación""
    * 5141b84 Revert "Modificación"
    * fdc58ec Modificación
    *   583539a Merge branch 'fix-formula'
    |\
    | * 6f4b793 Modificamos la formula del ancho y alto
    * | 8952ff1 Arreglamos ancho y alto
    |/
    *   adbd48b Merge branch 'feature-division-entera'
    |\
    | * 1b7af6c Comentario bastante largo sobre divisiones enteras
    * | 6314e83 Cambiamos las nuevas variables del programa
    |/
    *   89db92b Merge branch 'feature-strings'
    |\
    | * 89fcebb Ahora estudiamos strings
    * | 1470e53 Agregamos la clase lists.py
    |/
    * 154ead8 Revert "Agregamos una parte de código erronea"
    * a91e67d Revert "Revert "Agregamos una parte de código erronea""
    * 201a696 Revert "Agregamos una parte de código erronea"
    * b238fdd Agregamos un segundo commit erroneo
    * 3d12717 Agregamos una parte de código erronea
    * 2225b3e Borramos variable que usa memoria.
    * 50389f8 Agregamos una fórmula: width*height
    * b22fadd Archivo numbers.py para los operadores básicos

Los `merge` nos dejan ver de donde vienen los cambios y a dónde se fueron,
pero con `rebase` no podemos hacerlo, ya que todo se aplana directamente.

¿Pero qué pasa si quermeos aplastar los commits? Eso se hará con un rebase
interactivo.
## Rebase interactivo: reescribir el historial de Git
¿Qué podemos hacer con rebase interactivo?

Podemos cambiar el orden de los commits, modificar el orden de los commits, 
aplastar commits en un único commits, pero recordemos la norma que si cambiamos
el historial cambiamos los hashes y eso significa alterar la gráfica. 

NO HACER ESTO EN CÓDIGO PUSHEADO. Siempre hacerlo en cosas locales.

Veamos de nuevo como se encuentra nuestro árbol de trabajo hasta el momento

    $ git lod
    0c9daed (HEAD -> master, feature-temas) Estudiamos TemaD
    674279c Estudiamos Tema B
    87d17c4 Estudiamos Tema A
    a8218ba TemaC.py
    634087e (origin/master, origin/HEAD) Elimina archivo prueba
    80a19de Agrega un archivo de prueba
    117600d Revert "Revert "Revert "Modificación"""
    e2e28e5 Revert "Revert "Modificación""
    5141b84 Revert "Modificación"
    fdc58ec Modificación
    583539a Merge branch 'fix-formula'
    6f4b793 Modificamos la formula del ancho y alto
    8952ff1 Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 Cambiamos las nuevas variables del programa
    1b7af6c Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 Agregamos la clase lists.py
    89fcebb Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos

Quedemonos con los últimos 4 cambios, para ver si podemos aplastar los útlimos
4 commits en uno sólo

    $ git lod
    0c9daed (HEAD -> master, feature-temas) Estudiamos TemaD
    674279c Estudiamos Tema B
    87d17c4 Estudiamos Tema A
    a8218ba TemaC.py

Para ello necesitamos identificar los últimos cuatro commits.

    $ git rebase -i HEAD~4
    -- Aparece interfaz --
    -- Moveremos el commit para que queden en el orden A, B, C, D --
    -- En Doom Emacs se hace con Alt+Flechas --
    $ git lod
    ac30ba7 (HEAD -> master) Estudiamos TemaD
    483d059 TemaC.py
    c92e229 Estudiamos Tema B
    1ee9224 Estudiamos Tema A

Comparemos los hashes y vemos que han cambiado.

Pero, ¿qué pasa si queremos aplastar los commits en uno solo?

Lo podemos hacer con `squash`, entonces debemos seleccionar esa opción cuando 
nosotros entremos en el entorno del `rebase` interactivo.

Debemos dejar el primer commit como `pick` ya que cuando salgamos tendremos una
interfaz para poder realizar el nombre del commit en el que se aplastarán los 
demás

    $ git rebase -i HEAD~4
    -- Interfaz rebase --
    -- Ponemos el squash --
    $ git lod
    2a180ab (HEAD -> master) Terminamos de estudiar un tema muy importante
    634087e (origin/master, origin/HEAD) Elimina archivo prueba
    80a19de Agrega un archivo de prueba
    117600d Revert "Revert "Revert "Modificación"""
    e2e28e5 Revert "Revert "Modificación""
    5141b84 Revert "Modificación"
    fdc58ec Modificación
    583539a Merge branch 'fix-formula'
    6f4b793 Modificamos la formula del ancho y alto
    8952ff1 Arreglamos ancho y alto
    adbd48b Merge branch 'feature-division-entera'
    6314e83 Cambiamos las nuevas variables del programa
    1b7af6c Comentario bastante largo sobre divisiones enteras
    89db92b Merge branch 'feature-strings'
    1470e53 Agregamos la clase lists.py
    89fcebb Ahora estudiamos strings
    154ead8 Revert "Agregamos una parte de código erronea"
    a91e67d Revert "Revert "Agregamos una parte de código erronea""
    201a696 Revert "Agregamos una parte de código erronea"
    b238fdd Agregamos un segundo commit erroneo
    3d12717 Agregamos una parte de código erronea
    2225b3e Borramos variable que usa memoria.
    50389f8 Agregamos una fórmula: width*height
    b22fadd Archivo numbers.py para los operadores básicos
