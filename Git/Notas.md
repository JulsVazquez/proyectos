# Notas de Git

## ¿Qué es un control de versiones?
Un control de versiones es un sistema que graba los cambios de un archivo o 
conjunto de archivos a través del tiempo en donde puedes llamar versiones
especificas después. 

Usar un control de versiones nos ayuda a que si cometemos algún error con un
o unos archivos podamos recuperarlos de manera sencilla.

## Sistemas de control de versiones locales
Si estamos trabajando en un archivo en nuestra computadora y hemos modificado
algunas cosas y queremos tener una copia de cada cambio lo más sensato sería 
tener diferentes archivos en una carpeta, pero esto es un gran error. Porque
imaginemos que borramos esa carpeta por accidente o borramos un archivo que en 
un futuro necesitaremos. Para poder lidear con este problema los desarrolladores
crearon algo llamado sistema de control de versiones locales, para mantener 
todos los cambios de nuestro archivo en uno sólo sin necesidad de tener 
múltiples archivos.

## Sistemas de control de versiones centralizados
El otro problema que nos enfrentamos es que a veces necesitamos colaborar con
desarrolladores en otros sistemas, para esto existen los sistemas de control de
versiones centralizados. Estos sistemas tienen un único servidor que contiene
todas las versiones de los archivos y un numero de clientes puede revisar estos
archivos desde un lugar central. Por muchos años este fue el estándar del 
control de versiones.

Esta configuración ofrece varias ventajas, como las que todo mundo tiene la 
certeza de qué están haciendo en el proyecto y los administradores tienen un 
control de quién está haciendo qué.

De todas formas también tiene grandes desventajas y la más obvia es que si en
algún punto el servidor falla, digamos por una hora, esto significa que nadie 
va a poder colaborar en cambios durante esa hora hasta restablecer el servidor.
También si por alguna razón el HDD se corrompe y no tenemos un respaldo del 
proyecto, entonces nosotros perderemos absolutamente todo.

## Sistemas de control de versiones distribuidos
Es así cuándo llegan los sistemas de control de versiones distribuidos como Git,
Mercurial, Bazaar or Darcs los cuales no revisan el último archivo instántaneo,
sino que hace una copia exacta del repositorio incluyendo su historial completo.
De esta forma si el servidor muere, cada sistema que estaba colaborando por vía
de ese servidor tiene una copia y un respaldo para poder mandarlo de nuevo al
servidor. Cada copia es en realidad un respaldo completo de toda la información.

DE esta forma estos sistemas lidian bastante bien con varios repositorios 
remotos, así que uno puede colaborar con diferentes grupos de diferentes maneras
y todo simultaneamente en el mismo proyecto. 

## Una breve historia de Git
El proyecto del Kernel de Linux era _open source_. Por el tiempo en el que el 
Kernel estaba en matenimiento (1991-2002), los cambios en el software eran 
pasados como parches y archivados. En 2002, el proyecto empezó a usar un 
sistema de control de versión distribuido de un propietario llamado BitKeeper.

En 2005, la relación entre la comunidad que desarrollaba el kernel de Linux y
la empresa comercial que financiaba BitKeeper se rompió y todas las herramientas
libres de cargo fueron removidas. Este incidente hizo que la comunidad de 
desarrolladores de Linux y en especial Linus Torvalds (creador del kernel) 
desarrollaran su propia herramienta basada en las lecciones que habían aprendido
de trabajar con BitKeeper. Algunas de las metas de este nuevo sistema era:
* La velocidad
* Un diseño simple
* Un soporte fuerte para desarrollo no lineal (miles de ramas en paralelo)
* Totalmente distribuido
* Capaz de mantener grandes proyectos de manera eficiente (velocidad y espacio)

Fue así como en 2005, Git surgió y maduro de una manera facil de usar y todavía
tiene añgnas de sus cualidades iniciales. Es realmente rápido y muy eficiente 
con proyectos grandes.

## ¿Qué es Git?
La mayor diferencia entre Git y cualquier otro control de versiones es la manera
en la que Git piensa acerca de la información. Conceptualemte muchos otros 
sistemas guardan la información como una lista de cambios en un archivo. 
Estos otros sistemas (CVS, Subversion, Perforce, Bazaar, etc) piensan en 
la información como conjuntos de archivos y cambios hechos en cada archivo 
alrededor del tiempo. (Esto es llamado _delta-based version control_).

Git no piensa de esta manera el guardado de la información. En cambio piensa en
está información como una serie de fotos en miniatura de todo el sistema de 
archivos. Con Git, cada vez que hacemos un _commit_ o guardamos el estado
de nuestro proyecto lo que hace es tomar un foto de cómo se ven los archivos en 
ese momento y guarda una referencia de esa imagen. Para ser eficiente, si el
archivo no ha sido cambiado, Git no guarda la información del archivo otra vez,
sólo vincula el anterior archivo al que ya esta guardado. 

# Casi cada operación es local
La mayoría de las operaciones en Git necesitan sólo archivos y recursos locales 
para operar, generalmente no es necesaria información de otro computadora en 
nuestro sistema. De esta forma Git trabaja más rápido que otras CVCS ya que todo
el historia del proyecto se guarda en nuestro disco local, varias operaciones 
parecen casí instántaneas.
Por ejemplo, cuando nosotros revisamos nuestro historial de los cambios que hemos
hecho a un proyecto, Git no necesita ir al servidor y obtener el historial para
que lo podamos ver nosotros, sino simplemente lo lee directamente de nuestra 
base de datos local.

Esto nos ayuda a que si nos encotramos sin internet o fuera de una conexión VPN
y necesitamos trabajar podemos hacerlo de manera tranquila, realizar un _commit_
en nuestra copia local y después subirlo cuando tengamos conexión a internet.

## Git como integridad
Todo en Git es una suma de comprobaciones, antes de guardarse y después de ser
referenciado es una suma de comprobación. Esto quiere decir que es imposible 
cambiar el contenido de un archivo o directorio sin que Git lo sepa. Esta 
funcionalidad está contruida dentro de Git en los niveles más bajos y es una
filosofía de integración. Tú no puedes perder información en el tránsito o 
obtener un archivo corrupto sin que Git sea capaz de detectarlo.

Para este mecánismo Git usa algo llamado _SHA-1 hash_. Esto es una cadena de 
40 carácteres construido hexadecimalmente y calculados en base al contenido de 
un archivo o la estructura de un archivo. Un SHA-1 hash se ve algo así:

    24b9da6552252987aa493b52f8696cd6d3b00373

Entonces si nos damos cuenta, Git guarda todo en una base de datos de valores 
hash y no en una base de datos de nombre de archivos.

## Git generalmente añade sólo información 
Cuando nosotros hacemos alguna acción en Git, casi todo en ello es añadir 
información a la base de datos de git. Es dificil para el sistema hacer algo que
deshaga o hacerlo que borre información. Como con cualquier VCS, podemos perder
o arruinar cambios que no hemos hecho un _commit_. Pero después de hacerlo una
instántanea dentro de Git, es muy dificil perderla, especialmente si uno 
regularmente empuja la base de datos a otro repositorio.

Esto hace que usar Git sea un disfruto porque nosotros sabemos que podemos
experimentar sin peligro o equivocarnos en cosas.

## Los tres estados
Git tiene tres estados principales en donde los archivos pueden residir:
_modified, staged y committed_:
* _Modiffied:_ Quiere decir que hemos hecho cambios en el archivo pero no hemos
  hecho ningún _commited_ a la base de datos.
* _Staged:_ Quiere decir que hemos marcado el archivo modificado en una versión
  reciente para hacer un futuro _commited_
* _Commited:_ Quiere decir que la información ha sido guardada en nuestra 
  base de datos local.

Esto nos deja con tres principales secciones en un proyecto de Git: el árbol de
trabajo, el staging area y el directorio de git.

El árbol de trabajo es un simple chequeo de una versión del proyecto. Estos 
archivos son empujados afuera de la base de datos en el directorio git y puestos
en el disco para usarlos o modificarlos.

El staging area es un archivo, generalmente contenido en el directorio git, 
almacena información acerca de que irá al siguiente commit.

El directorio git es donde Git guarda toda la metadata y la base de datos de 
nuestro proyecto. Este es la parte más importante de Git y es la que se copia 
cuando nosotros clonamos un repositorio de otra computadora.

El flujo de trabajo básico en Git es:

1. Modificas archivos en tu árbol de trabajo.
2. Seleccionas sólo esos cambios que queremos que sean parte del siguiente 
   commit, que sólo añade esos cambios al staging area.
3. Hacemos un commit, el cuál toma los archivos como están en el staging area y
   almacena la imagen permanentemente en nuestro directorio Git.

## La linea de comandos
Existen muchas formas de usar Git. Esta la herramienta original que es con la 
línea de comandos y otras varias que son con interfaz gráfica. Nosotros 
aprenderemos a usarla por medio de la línea de comandos. Primero porque en la
línea de comandos podemos correr todos los comandos de Git, la mayoría de GUI's
implementan sólo parcialmente un conjunto de las funcionalidades de Git por 
prácticidad. Además, si sabemos usar la línea de comandos probablemente podremos
intuir cómo usar las versiones con interfaz, pero lo contrario no necesariamente
es cierto.

# Instalando Git
## Instalando en Linux
Si quieremos instalar las herramientas básicas de git en Linux, podemos hacerlo
por vía de un instalador binario, generalmente se hace con el manejador de 
paquetes que viene en nuestra respectiva distribución. Si estamos en Fedora (o
alguna distribución basada en RPM como RHEL o CentOS) podemos usar `dnf`:

    $ sudo dnf install git-all
    
Si estas es una distribución basada en Debian como Ubuntu podemos usar `apt`:

    $ sudo apt install git-all

Para más opciones en distintas distribuciones podemos ir a la página de Git:
https://git-scm.com/download/linux .

## Instalando en macOS
Hay muchas maneras de instalar Git en Mac. La más sencilla es instalando el 
Xcode Command Line Tools. En Mavericks (10.9) or superior podemos hacerlo
corriendo `git` desde la tervial.

    $ git --version

Si no la tenemos instalada, nuestro prompt lo instalará.

De todos modos si no ha sido instalada hay que verificar la version que tenemos
e ir a la página web de Git: https://git-scm.com/download/mac .

## Instalación en Windows
La manera oficial de instalarlo es directamente desde la página web. Sólo 
tenemos que ir a: https://git-scm.com/download/win y la descargará se hará
automáticamente. Aunque existe un proyecto llamado _Git para Windows_, el cual
esta separado de Git, para más información podemos visitar su página:
https://gitforwindows.org .

## Instalación desde la fuente
Algunas personas les parece útil instalar linus desde la fuente, de esta forma 
obtienen la versión más reciente. Los instaladores binarios tienden a estar un 
poco atrás en esto, pero Git ha madurado bastante entonces se ha hecho menor 
esta diferencia en los instaladores.

Para hacer esto necesitamos tener las dependencias de Git instaladas las cuales
son: ***autotools, curl, zlib, openssl, expat y libiconv**. Si nosotros estamos
en un sistema como Fedora o Debian podemos usar uno de estos comandos para 
instalar las dependeicas minimas para compilar e instalar los binarios de Git.

    $ sudo dnf install dh-autoreconf curl-devel expat-devel gettext-devel \
    openssl-devel perl-devel zlib-devel
    $ sudo apt-get install dh-autoreconf libcurl4-gnutls-dev libexpat1-dev \
    gettext libz-dev libssl-dev
    
Para tener habilitada la documentación en varios formatos (doc, html, info)
necesitaremos unas dependencias adicionales:

    $ sudo dnf install asciidoc xmlto docbook2X
    $ sudo apt-get install asciidoc xmlto docbook2x

En el caso de distribuciones basadas en Debian necesitaremos el paquete 
_install-info:_

    $ sudo apt-get install install-info

Si estamos usando una distribución basada en RPM necesitamos el paquete `getopt`
que esta ya instalado en algunas distribuciones de Debian:

    sudo dnf install getopt

Adicional a este si usamos Fedora/RHEL/derivados de RHEL necesitamos hacer esto:

    $ sudo ln -s /usr/bin/db2x_docbook2texi /usr/bin/docbook2x-texi

Debido a la diferencia en los nombres de los binarios.

Cuando tengamos todas las dependencias necesarias podemos agarrar la última 
versión tageada de git. Podemos hacerlo desde el sitio de kernel.org en
https://www.kernel.org/pub/software/scm/git o descargarlo desde el sitio de
Github: https://github.com/git/git/releases . Es más fácil saber la última 
versión desde el Github.

Después, compilamos e instalamos:

    $ tar -zxf git-2.8.0.tar.gz
    $ cd git-2.8.0
    $ make configure
    $ ./configure --prefix=/usr
    $ make all doc info
    $ sudo make install install-doc install-html install-info

Después de esto tenemos Git instalado.

Podemos obtener lo mismo clonando el repositorio:

    $ git clone git://git.kernel.org/pub/scm/git/git.git

# Primera configuración de Git
Ahora que tenemos Git en nuestro sistema, podemos hacer algunas cosas para
customizar el ambiente.

Git viene con una herramienta llamada `git config` que nos permiten configurar 
variables que controlan todos los aspectos de cómo funciona git. Estas variables
pueden ser guardadas en 3 diferentes lugares:

1. `[path]/etc/gitconfig`file: 

Git viene con una herramienta llamada `git config` que nos permiten configurar 
variables que controlan todos los aspectos de cómo funciona git. Estas variables
pueden ser guardadas en 3 diferentes lugares:

1. `[path]/etc/gitconfig`file: Contiene valores que se aplican para cada usuario
   en el sistema y todos los repositorios. Si nosotros pasamos la opción 
   `--system` a `git config`, leera y escribirá desde este archivo 
   especificamente. Ya que este es un archivo de configuración de sistema, 
   necesitaremos permisos de súper-usuario para modificarlo.
2. `~/.gitconfig` o `~/.config/git/config` file: Contiene valores personales del
   usaurio. Podemos hacer que Git lea y escriba en este archivo especificamente
   pasando la opción `--global` y esto afectará a todos los repositorios que 
   trabajemos en nuestro sistema.
3. `config` file en el directorio de Git (esto es, `git/config`) o cualquier 
   repositiorio que estemos usando: Podemos forzar a Git a leer y escribir desde
   este archivo con `--local`, pero de hecho eso lo hace por default. De hecho
   necesitas estar localizado en un repositorio Git para que esta opción 
   funcione correctamente.

Cada nivel sobrecarga valores del anterior nivel, los valores en `.git/config` 
fallan sobre los de `[path]/etc/gitconfig`

Podemos ver todas nuestras configuraciones con el siguiente comando:
    
    $ git config --list --show-origin

## Nuestra identidad
Lo primero que deberíamos hacer al instalar Git es configurar nuestro nombre de
usuario y correo. Ya que cada commit que hagamos en Git ocupa está información.

    $ git config --global user.name "John Doe"
    $ git config --globar user.email johndoe@example.com

Necesitamos hacer esto una sola vez pasando la opción `--global` porque git 
siempre usará esta información para lo que sea que hagamos en nuestro sistema.
Si queremos sobrecargar esto con un diferente nombre o email para proyectos
especificos, podemos correr el comando sin el `--global` cuando estemos en un
proyecto.

## Nuestro editor
Si nosotros no hemos configurado un editor Git lo hace automáticamente y utiliza
el editor que viene en el sistema por default.

Si queremos usar un editor de texto diferente como por ejemplo Emacs podemos 
hacer lo siguiente:

    $ git config --global core.editor emacs

## El nombre por default de tu branch
Por default Git crea una rama llamada _master_ cuando nosotros creamos un 
repositorio con `git init` todas las configuraciones de git las podemos
encontrar en este punto:
    
    $ git config --list
    user.name=John Doe
    user.email=johndoe@example.com
    color.status=auto
    color.branch=auto
    color.interactive=auto
    color.diff=auto
    ...

Podemos ver las llaves más de una vez porque Git lee la misma llave desde
diferentes archivos (`[path]/etc/gitconfig` y `~/.gitconfig` por ejemplo). En 
ese caso, Git usa el último valor para cada llave única que vemos.

Podemos ver lo que Git piensa del valor de una llave especifica escribiendo
`git config <key>`:

    $ git config user.name
    John Doe

# Obteniendo ayuda
Si en alguna ocasiones necesitamos ayuda mientras usamos Git, hay tres opciones
equivalentes para obtener el manual de ayuda con cualquiera de los siguientes 
comandos:

    $ git help <verb>
    $ git <verb> --help
    $ man git-<verb>

Por ejemplo si queremos obtener la página del manual para el comando 
`git config` podemos hacerlo con lo siguiente:

    $ git help config

Podemos acceder a estos comandos incluso de manera offline. 

En caso de que no necesitemos tanta información de parte del manual y sólo 
queremos refrescarnos las opciones de un comando en Git podemos hacerlo con la 
opción `-h`:

    $ git add -h

# Básicos de Git
## Obteniendo un repositorio git
Obtenemos un repositorio Git de una de las dos maneras:
1. Podemos tomar un directorio local que no está bajo un control de versiones
   y convertirlo a un repositorio git.
2. Podemos clonar un repositorio existente de algún lado.

De cualquier forma, nosotros terminaremos con un repositorio en nuestra máquina
local para trabajar.

## Inicializando un repositorio en un directorio ya existente
Si tenemos un directorio con un proyecto y no esta bajo un control de versiones 
y queremos iniciar controlandolo por Git, lo primero que debemos de hacer es ir
al directorio del proyecto.

En Linux y macOS lo hacemos:

    $ cd ~/ruta-de-mi-proyecto/carpeta-proyecto

en Windows: 

    $ cd C:/Users/user/carpeta-proyecto
    
y escribimos:

    $ git init

Con esto creamos un subdirectorio llamado `.git` que contiene todos los archivos
necesarios del repositorio.

Si nosotros queremos iniciar controlando archivos ua existentes (lo opuesto a un
directorio vacío) deberíamos empezar rastreando aquellos archivos con un commit 
inicial. Podemos hacer con el comando `git add` que especifica que archivos 
queremos rastrear seguido por un `git commit`:

    $ git init
    $ git add *.c //Agregará todos los archivos con extensión .c
    $ git add LICENSE //Agregará un archivo llamado LICENSE
    $ git commit -m 'Versión inicial del proyecto'

En este punto ya tendremos un repositorio con archivos en seguimiento y con un
commit inicial.

## Clonando un repositorio existente
Clonamos un repositorio con `git clone <url>`. Por ejemplo si queremos clonar la
colección llamada `libgit2` podemos hacerlo con:

    $ git clone https://github.com/libgit2/libgit2
    
Eso creara un directorio llamado `libgit2` que contiene un directorio `.git` 
dentro de él que verifica toda la información del repositorio y nos entrega la
última versión. Al entrar a la carpete podremos ver todos los archivos del
proyecto para trabajar en él.

Si queremos clonar el repositorio dentro de un directorio llamado de otra manera
que `libgit2` podemos hacerlo especificando el nombre del nuevo directorio:

    $ git clone https://github.com/libgit2/libgit2 mylibgit

De esta forma guardará toda la información previa en el directorio llamada 
`mylibgit`.

Git usa diferentes protocolos. El ejemplo anterior es usando el protocolo 
`https://`, pero podemos usar `user@server:path/to/repo.git`, que usa la
transferencia por SSH.

# Grabando cambios en el repositorio
Recordemos que cada archivo en el directorio en el que trabajamos puede tener 
dos estados: _tracked_ o _untracked_. Los archivos _tracked_ son los que tienen
una snapshot, pueden estar _unmodified, modified o staged_. Básicamente son 
archivos que Git ya sabe qué son.

Archivos _untracked_ son todos los demás archivos en nuestro directorio que no 
se les ha tomado una _snapshot_ y que no etan en nuestra _staging area_. Cuando
clonamos un repositorio todos los archivos son _tracked_ y _unmodified_ porque
Git acaba de revisarlos y nosotros no hemos editado nada.

Si editamos archivos Git verá esto como _modified_ porque hemos cambiado algo 
desde el último _commit_. Mientras trabajemos, iremos seleccionando estos 
archivos y modificandolos y después haremos un _commit_ de todos los cambios
que se encuentren en el _staging area_ y el ciclo se repetirá.

## Verificando el estado de nuestros archivos
El comando `git status` nos ayuda a saber en qué estado se encuentran los 
archivos.
Si corremos este comando en un repositorio que acabamos de clonar nuestra salida
será algo como:

    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    nothing to commit, working tree clean
    
Esto quiere decir que tenemos un directorio limpio, ya que ningún archivo está 
modificado. 

Ahora digamos que queremos agregar un archivo a nuestro directorio, digamos un 
archivo README. Si el archivo no existe antes y corremos el comando `git status`
veremos que se encuentra en como un archivo _untracked_.

    $ echo 'My Prject' > README
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Untracked files:
        (use "git add <file>..." to include in what will be commited)
        
        README
    nothing added to commit but untracked files present (use "git add" to track)

Esto quiere decir que Git esta viendo un archivo que antes no existía en el 
previo _commit_, por lo que nos comenta cómo podemos rastrearlo. 

## Rastreando nuevos archivos
Para agregar un archivo nuevo podemos usar el comando `git add`. Para empezar a
rastrear archivos

    4 git add README

Si corremos el comando `status` podemos ver que nuestro archivo README está 
ahora rastreado y listo para hacer un commited.

    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
        (use "git restore --staged <file>..." to unstage)
        
            new file:   README

## Archivos modificados en el staging
Cambiemos ahora un archivo que ya ha sido rastreado. Por ejemplo modifiquemos el
archivo CONTRIBUTING.md y corremos el comando `git status` otra vez, obtendremos
algo de este estilo:

    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)
        
        new file: README
    
    Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git checkout -- <file>..." to discard changes in working directory)
        
        modified: CONTRIBUTING.md
    
El archivo CONTRIBUTING.md aparece bajo el nombre de "Cambios no rastreados para
el commit", lo cual quiere decir que ha sido modificado en el directorio de 
trabajo pero no ha sido staged. Para esto tenemos que añadir el archivo al 
_staging area_ con el comando `git add <file>`, de esta forma al verificar con
`git status` tendríamos algo del estilo:

    $ git add CONTRIBUTING.md
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'
    Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)
        
        new file: README
        modified: CONTRIBUTING.md
    
Ahora imaginemos que recordaste en hacer una modificación al archivo 
CONTRIBUTING.md antes de hacer un commit, entonces lo editas:

    $ emacs CONTRIBUTING.md
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'
    Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)
        
        new file: README
        modified: CONTRIBUTING.md
    
    Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git checkout -- <file>..." to discard changes in working directory)
        
        modified: CONTRIBUTING.md
    
Pero ambos archivos estan en _staged_ y _unstaged_ a pesar de ser el mismo. Lo 
que sucede es que Git tiene rastreado el último cambio agregado con el comando
`git add` del archivo, entonces si nosotros hacemos un commit se guardará el 
archivo al que le habíamos hecho por última vez el comando `add`, entonces
si hemos modificado un mismo archivo que previamente había sido añadido tenemos
que volverlo a añadir con el comando `git add`, de esta forma tendremos el 
último cambio rastreado.

    $ git add CONTRIBUTING.md
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'
    Changes to be committed:
        (use "git reset HEAD <file>..." to unstage)
        
        new file: README
        modified: CONTRIBUTING.md

## Status corto
A pesar de que el comando `git satus` es bastante comprensible, tenemos otro 
comando más corto para poder ver los cambios de una manera más compacta. Si 
corremos el comando `git status -s` o `git status --short` obtendremos una 
salida más compacta:

    $ git status -s
    M README
    MM Rakefile
    A lib/git.rb
    M lib/simplegit.rb
    ?? LICENSE.txt

Los archivos que no han sido rastreados tendrán un `??` a un lado de ellos. Los
nuevos archivos que han sido agregados en el _staging area_ tendrán una `A`, los
archivos modificados tendrán una `M` y así consecutivamente. Hay dos columnas en
la salida, la izquierda indica el status del _staging area_ y la columna 
derecha indica el status en el árbol de trabajo. El archivo README ha sido 
modifcado en el directorio de trabajo, pero no ha sido _staged_, mientras que 
`lib/simplegit.rb` ha sido modificado y está _staged_. `Rakefile` fue modificado
agregado en el _staged_ y modificado otra vez, así que hay cambios tanto en el
_staged_ como en el _unstaged_.

## Ignorando archivos
En ocasiones tendremos clases o archivos que no queremos que Git añada 
automáticamente o incluso que los muestre como archivos _untracked_. 
En estos casos podemos añadir un archivo llamado `.gitignore`. Aquí tenemos un
ejemplo de un archivo `.gitignore`:

    $ cat .gitignore
    *.[oa]
    *~

La primera linea dice que ignoremos los archivos con extensión `.o` o `.a`. La 
segunda línea dice que ignoremos todos los archivos que terminen con una tilde 
(`~`), que son usados por algunos editores cómo Emacs para marcar archivos 
temporales.

Las reglas de un archivo `.gitignore` son las siguientes:

* Lineas en blanco o lineas que empiezan con un `#` son ignoradas.
* Los patrones estándar glob pueden ser aplicados recursivamente en todo el 
  árbol de trabajo.
* Podemos empezar patrones con un `/` para anular recursivamente.
* Podemos terminar patrones con un `/` para especificar un directorio.
* Podemos negar un patron empezando con un signo de exclamación (`!`).

Los patrones glob son expresiones regulares usadas por la `shell`. Un asterisco
(`*`) emparejan cero o más carácteres; `[abc]` emparejan cualquier carácter 
dentro de los corchetes, etc.

Aquí tenemos otro ejemplo un archivo `.gitignore`:

    # Ignora todos los archivos con extensión .a 
    *.a
    # Pero deja que rastree los archivos lib.a, incluso aunque ignoramos las extensiones .a 
    !lib.a
    # Sólo ignora el archivo TODO en el directorio actual, pero no el subdir/TODO
    /TODO
    # Ignora todos los archivos en cualquier directorio llamado build
    build/
    # Ignora doc/notes.txt, pero no doc/server/arch.txt
    doc/*.txt
    # Ignora todos los archivos .pdf  en el directorio /doc y en cualquiera de sus subdirectorios
    doc/**/*.pdf

## Viendo nuestros cambios staged y unstaged
Existe un comando que es `git diff` el cual compara que es lo que hemos agregado
en nuestro archivo antes de hagamos un `git add`.
Si queremos ver que ha estado _staged_ y que se irá en el siguiente _commit_, 
podemos usar `git diff --staged`. Este comando compara lo que ha estado en 
nuestro _staged area_ desde nuestro último commit.
Podriamos tener algo del estilo:

    $ git diff --staged
    diff --git a/README b/README
    new file mode 100644
    index 0000000..03902a1
    --- /dev/null
    +++ b/README
    @@ -0,0 +1 @@
    +My Project

Es importante notar que `git diff` por si mismo no muestra todos los cambios que
se ha hecho desde el último _commit_, sino sólo los cambios que siguen en 
_unstaged_. Si nosotros hemos mandado todo a la _staged area_ `git diff` no nos
mostrará nada de salida.

Otro ejemplo es que si nosotros tenemos _stage_ el archivo CONTRIBUTING.md y 
después lo editamos, podemos usar `git diff` para ver los cambios en el archivo
que esta en el _staged_ y los cambios que están _unstaged_. En nuestro ambiente
podría verse de la siguiente manera:

    $ git add CONTRIBUTING.md
    $ echo '# test line' >> CONTRIBUTING.md
    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes to be committed:
    (use "git reset HEAD <file>..." to unstage)
    modified:
    CONTRIBUTING.md
    Changes not staged for commit:
    (use "git add <file>..." to update what will be committed)
    (use "git checkout -- <file>..." to discard changes in working directory)
    modified:
    CONTRIBUTING.md
    
Ahora podemos usar `git diff` para ver lo que sigue _unstaged_:

    $ git diff
    diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
    index 643e24f..87f08c8 100644
    --- a/CONTRIBUTING.md
    +++ b/CONTRIBUTING.md
    @@ -119,3 +119,4 @@ at the
    ## Starter Projects
    See our [projects
    list](https://github.com/libgit2/libgit2/blob/development/PROJECTS.md).
    +# test line

y `git diff -chached` para ver lo que tenemos en el _staged_ (`--staged` y 
`cached` son sinónimos):

    $ git diff --cached
    diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
    index 8ebb991..643e24f 100644
    --- a/CONTRIBUTING.md
    +++ b/CONTRIBUTING.md
    @@ -65,7 +65,8 @@ branch directly, things can get messy.
    Please include a nice description of your changes when you submit your PR;
    if we have to read the whole diff to figure out why you're contributing
    in the first place, you're less likely to get feedback and have your change
    -merged in.
    +merged in. Also, split your changes into comprehensive chunks if your patch is
    +longer than a dozen lines.
    If you are starting to work on a particular area, feel free to submit a PR
    that highlights your work in progress (and note in the PR title that it's

## Haciendo commit a nuestros cambios
Ahora que nuestra _staging area_ esta completa, podemos hacer un _commit_ a 
nuestros cambios. Podemos solamente escribir el comando `git commit`:

    $ git commit
    
De esta forma nuestro editor predeterminado se abrirá:

    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.
    # On branch master
    # Your branch is up-to-date with 'origin/master'.
    #
    # Changes to be committed:
    #
    new file:
    README
    #
    modified:
    CONTRIBUTING.md
    #
    ~
    ~
    ~
    ".git/COMMIT_EDITMSG" 9L, 283C

Podemos ver que el mensaje por default del commit contiene la últma entrada del 
comando `git status` comentado.

Pero nosotros podemos escribir un mensaje de _commit_ sin la necesidad de 
desplegar el editor de texto. Esto se hace especificandolo con la bandera `-m`:

    $ git commit -m "Story 182: fix benchmarks for speed"
    [master 463dc4f] Story 182: fix benchmarks for speed
    2 files changed, 2 insertions(+)
    create mode 100644 README

Recordemos que todo a lo que se le haga _commit_ es aquello que está en el
_staging area_, si nosotros no hemos agregado todo a ella puede ser que no
tengamos un commit completo.

## Esquivando el staging area
El _staging area_ es más complicado de lo que necesitamos en nuestro flujo de
trabajo. Si queremos saltarnosla, Git provee un shortcut. Añadiendo la opción
`-a` en el comando `git commit` mandamos cada archivo como un _stage_ que es
rastreado antes de hacer el commit, de esta forma podemos saltarnos el comando
`git add`

    $ git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
    (use "git add <file>..." to update what will be committed)
    (use "git checkout -- <file>..." to discard changes in working directory)
    modified:
    CONTRIBUTING.md
    no changes added to commit (use "git add" and/or "git commit -a")
    $ git commit -a -m 'Add new benchmarks'
    [master 83e38c7] Add new benchmarks
    1 file changed, 5 insertions(+), 0 deletions(-)

## Removiendo archivos
